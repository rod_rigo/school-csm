<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Http\Exception\ForbiddenException;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginRedirect' => [
                'prefix' => 'Admin',
                'controller' => 'Users',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authError' => 'Did you really think you are allowed to see that?',
            'authenticate' => [
                'RememberMe.Cookie' => [
                    'userModel' => 'Users',
                    'fields' => ['username' => 'email', 'password' => 'password'],
                    'inputKey' => 'remember_me',
                    'always' => true,
                    'dropExpiredToken' => true,
                    'cookie' => [
                        'name' => 'rememberMe',
                        'expires' => '+30 days',
                        'secure' => true,
                        'httpOnly' => true,
                    ],
                ],
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password'],
                    'userModel' => 'Users',
                    'finder' => 'auth',
                ]
            ],
            'queryParam' => 'redirect',
            'storage' => 'Session',
            'unauthorizedRedirect' => Router::url(['prefix' => false, 'controller' => 'Users', 'action' => 'login'])
        ]);
        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');

        $this->Auth->deny();
        $auth = $this->Auth->user();
        $prefix = $this->request->getParam('prefix');
        $controller = $this->request->getParam('controller');
        $action = $this->request->getParam('action');

        $this->set(compact('prefix', 'controller', 'action', 'auth'));
    }

    public function isAuthorized($user) {

        if(strtolower($this->request->getParam('prefix')) == strtolower('Admin')){
            if(empty($user) && !boolval($user['is_admin'])){
                throw new ForbiddenException(__('Forbidden Action!'));
            }

            return true;
        }

        if(strtolower($this->request->getParam('prefix')) == strtolower('Division')){
            if(empty($user) && boolval($user['is_admin'])){
                throw new ForbiddenException(__('Forbidden Action!'));
            }

            return true;
        }

        throw new ForbiddenException(__('Forbidden Action!'));
    }

}
