<?php
declare(strict_types=1);

namespace App\Controller\Division;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * Choices Controller
 *
 * @property \App\Model\Table\ChoicesTable $Choices
 * @method \App\Model\Entity\Choice[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChoicesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Choices->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getChoices(){
        $data = $this->Choices->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $choice = $this->Choices->newEmptyEntity();
        if ($this->request->is('post')) {
            $choice = $this->Choices->patchEntity($choice, $this->request->getData());
            $choice->user_id = $this->Auth->user('id');
            if ($this->Choices->save($choice)) {
                $result = ['message' => ucwords('The choice has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($choice->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Choice id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $choice = $this->Choices->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $choice = $this->Choices->patchEntity($choice, $this->request->getData());
            $choice->user_id = $this->Auth->user('id');
            if ($this->Choices->save($choice)) {
                $result = ['message' => ucwords('The choice has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($choice->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($choice));
    }

}
