<?php
declare(strict_types=1);

namespace App\Controller\Division;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Moment\Moment;

/**
 * Surveys Controller
 *
 * @property \App\Model\Table\SurveysTable $Surveys
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SurveysController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('division');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

    }

    public function getSurveys(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $records = ($this->request->getQuery('start_date'))? $this->request->getQuery('records'): intval(10000);
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'OR'=>[
                    [
                        'Surveys.created >=' => $startDate,
                        'Surveys.created <=' => $endDate,
                    ],
                    [
                        'Surveys.created >=' => $endDate,
                        'Surveys.created <=' => $startDate,
                    ]
                ]
            ])
            ->order(['ca_no' => 'DESC'],true)
            ->limit($records);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function today()
    {

    }

    public function getSurveysToday(){
        $startDate = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->addDays(1)->format('Y-m-d');
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->order(['ca_no' => 'DESC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function week()
    {

    }

    public function getSurveysWeek(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('week')->addDays(1)->format('Y-m-d');
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->order(['ca_no' => 'DESC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function month()
    {

    }

    public function getSurveysMonth(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('month')->addDays(1)->format('Y-m-d');
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->order(['ca_no' => 'DESC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function year()
    {

    }

    public function getSurveysYear(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->order(['ca_no' => 'DESC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function reports(){
        $customerTypes = TableRegistry::getTableLocator()->get('CustomerTypes')
            ->find('list',[
                'keyField' => function($query){
                    return intval($query->id);
                },
                'valueField' => function($query){
                    return ucwords($query->customer_type);
                }
            ]);
        $this->set(compact('customerTypes'));
    }

    public function getReports(){
        $month = $this->request->getQuery('month') ?['month like' => '%'.ucwords($this->request->getQuery('month')).'%']: null;
        $customerType = $this->request->getQuery('customer_type') ?['Surveys.customer_type_id =' => $this->request->getQuery('customer_type')]: null;
        $services = $this->request->getQuery('services') ?['Surveys.service_id =' => $this->request->getQuery('services')]: null;
        $where = [
            $customerType,
            $services,
        ];
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'year' => 'YEAR(Surveys.created)',
                'month' => 'MONTHNAME(Surveys.created)'
            ])
            ->where($where)
            ->having([
                'year =' => intval($this->request->getQuery('year'))
            ])
            ->having($month)
            ->limit($this->request->getQuery('records'))
            ->enableAutoFields();
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function excel(){
        $month = $this->request->getQuery('month') ?['month like' => '%'.ucwords($this->request->getQuery('month')).'%']: null;

        $service = TableRegistry::getTableLocator()->get('Services')->find()
            ->where([
                'id =' => intval($this->request->getQuery('services'))
            ])->first();

        $year = $this->request->getQuery('year');
        $customerType = $this->request->getQuery('customer_type') ?['Surveys.customer_type_id =' => $this->request->getQuery('customer_type')]: null;
        $services = $this->request->getQuery('services') ?['Surveys.service_id =' => $this->request->getQuery('services')]: null;
        $where = [
            $services,
            $customerType
        ];

        $table = $this->Surveys;

        $surveys = $table->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Answers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where($where)
            ->select([
                'year' => 'YEAR(Surveys.created)',
                'month' => 'MONTHNAME(Surveys.created)'
            ])
            ->having([
                'year =' => intval($year)
            ])
            ->having($month)
            ->limit(intval($this->request->getQuery('records')))
            ->enableAutoFields(true);

        $subject = TableRegistry::getTableLocator()->get('Subjects')->find()
            ->where([
                'is_active =' => intval(1)
            ])
            ->first();

        $questions = TableRegistry::getTableLocator()->get('Questions')
            ->find()
            ->contain([
                'Options' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])->order(['Options.position' => 'ASC'], true);
                    }
                ],
                'Options.Choices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Questions.subject_id =' => $subject->id
            ])
            ->order(['Questions.position' => 'ASC'],true);

        $filename = !empty($service)? ucwords($service->service):uniqid();

        $this->set(compact('surveys', 'year', 'questions', 'filename'));
    }

    public function services(){
        $customerTypes = TableRegistry::getTableLocator()->get('CustomerTypes')
            ->find('list',[
                'keyField' => function($query){
                    return intval($query->id);
                },
                'valueField' => function($query){
                    return ucwords($query->customer_type);
                }
            ]);
        $this->set(compact('customerTypes'));
    }

    public function getSurveyServices($serviceId = null){
        $month = $this->request->getQuery('month') ?['month like' => '%'.ucwords($this->request->getQuery('month')).'%']: null;
        $data = $this->Surveys->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'year' => 'YEAR(Surveys.created)',
                'month' => 'MONTHNAME(Surveys.created)'
            ])
            ->where([
                'Surveys.service_id =' => intval($serviceId)
            ])
            ->having([
                'year =' => intval($this->request->getQuery('year'))
            ])
            ->having($month)
            ->limit($this->request->getQuery('records'))
            ->enableAutoFields();
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function servicesReport($serviceId = null){
        $year = $this->request->getQuery('year')? ['year =' => (intval($this->request->getQuery('year')))]: null;
        $month = $this->request->getQuery('month')? ['month like' => '%'.(ucwords($this->request->getQuery('month'))).'%']: null;

        $service = TableRegistry::getTableLocator()->get('Services')->get($serviceId,[
            'contain' => [
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]
        ]);

        $query = $this->Surveys;

        $total = $query->find()
            ->where([
                'service_id =' => intval($service->id)
            ])
            ->count();

        $customerTypes = $query->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->where([
                'service_id =' => intval($service->id)
            ])
            ->select([
                'customer_type' => 'CustomerTypes.customer_type',
                'total' => 'COUNT(customer_type_id)'
            ])
            ->group([
                'customer_type_id'
            ]);

        $spans = $query->find()
            ->where([
                'service_id =' => intval($service->id)
            ])
            ->join([
                'visitors' => [
                    'table' => 'visitors',
                    'type' => 'LEFT',
                    'conditions' => [
                        'visitors.id = Surveys.visitor_id'
                    ]
                ],
                'spans' => [
                    'table' => 'spans',
                    'type' => 'LEFT',
                    'conditions' => [
                        'spans.id = visitors.span_id'
                    ]
                ]
            ])
            ->select([
                'span' => 'spans.span',
                'total' => 'COUNT(Surveys.id)'
            ])
            ->group(['span']);

        $genders = $query->find()
            ->where([
                'service_id =' => intval($service->id)
            ])
            ->join([
                'visitors' => [
                    'table' => 'visitors',
                    'type' => 'LEFT',
                    'conditions' => [
                        'visitors.id = Surveys.visitor_id'
                    ]
                ],
                'genders' => [
                    'table' => 'genders',
                    'type' => 'LEFT',
                    'conditions' => [
                        'genders.id = visitors.gender_id'
                    ]
                ]
            ])
            ->select([
                'gender' => 'genders.gender',
                'total' => 'COUNT(Surveys.id)'
            ])
            ->group(['gender']);

        $answers = TableRegistry::getTableLocator()->get('Answers')
            ->find()
            ->join([
                'surveys' => [
                    'table' => 'surveys',
                    'type' => 'LEFT',
                    'conditions' => [
                        'surveys.id = Answers.survey_id'
                    ]
                ],
                'questions' => [
                    'table' => 'questions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'questions.id = Answers.question_id'
                    ]
                ],
            ])
            ->select([
                'question_id' => 'questions.id',
                'is_charter' => 'questions.is_charter',
                'service_id' => 'surveys.service_id',
                'total' => 'COUNT(Answers.id)',
                'choice_id',
                'year' => 'YEAR(Answers.created)',
                'month' => 'MONTHNAME(Answers.created)'
            ])
            ->where([
                'service_id =' => intval($service->id)
            ])
            ->having($year)
            ->having($month)
            ->group([
                'question_id'
            ])
            ->order(['position' => 'ASC'],true);

        $questions = TableRegistry::getTableLocator()->get('Questions')
            ->find()
            ->contain([
                'Options' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->select([
                                'choice_id',
                                'question_id',
                                'Options.position'
                            ])
                            ->order([
                                'Options.position' => 'ASC'
                            ], true);
                    }
                ],
                'Options.Choices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'id',
                'is_charter'
            ])
            ->order([
                'Questions.position' => 'ASC'
            ],true)
            ->order([
                'Questions.is_charter' => 'DESC'
            ],true);

        $collections = (new Collection($questions->toArray()))->groupBy('is_charter');

        $this->set(compact('service', 'total', 'customerTypes', 'genders', 'spans', 'collections', 'answers'));
    }

    /**
     * View method
     *
     * @param string|null $id Survey id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $survey = $this->Surveys->get($id, [
            'contain' => [
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Visitors.Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Services' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Answers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Answers.Questions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Answers.Questions.Options' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['position' => 'ASC'],true);
                    }
                ],
                'Answers.Questions.Options.Choices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ],
        ]);

        $sex = [ucwords('Male'), ucwords('Female')];
        $this->set(compact('survey', 'sex'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Survey id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $survey = $this->Surveys->get($id);
        if ($this->Surveys->delete($survey)) {
            $result = ['message' => ucwords('The survey has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The survey has not been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }
}
