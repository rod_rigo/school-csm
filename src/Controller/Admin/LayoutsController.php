<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Http\Exception\ForbiddenException;
use Cake\Validation\Validator;

/**
 * Layouts Controller
 *
 * @property \App\Model\Table\LayoutsTable $Layouts
 * @method \App\Model\Entity\Layout[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LayoutsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Layouts->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getLayouts(){
        $data = $this->Layouts->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getLayoutsDeleted(){
        $data = $this->Layouts->find('all',['withDeleted'])
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->whereNotNull('Layouts.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $layout = $this->Layouts->newEmptyEntity();
        if ($this->request->is('post')) {
            $layout = $this->Layouts->patchEntity($layout, $this->request->getData());

            $validator = new Validator();

            $validator
                ->requirePresence('file',true)
                ->notEmptyFile('file', ucwords('please upload a image'),false)
                ->add('file', 'file',[
                    'rule' => function($value){
                        $mimes = ['image/png', 'image/jpg', 'image/jpeg', 'image/jfif'];

                        if(!in_array($value->getClientMediaType(), $mimes)){
                            return ucwords('only JPEG, JPG, PNG, & JFIF are allowed');
                        }

                        return true;

                    }
                ]);

            $errors = $validator->validate($this->request->getData());
            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }


            if($layout->hasErrors() == false && strlen($this->request->getData('file')->getClientFileName()) > 0){

                $file = $this->request->getData('file');

                $filename = uniqid().$file->getClientFileName();

                try{
                    $path = WWW_ROOT. 'img'. DS. 'signature-img';
                    $folder = new Folder();
                    if(!$folder->cd($path)){
                        $folder->create($path);
                    }

                    $filepath = WWW_ROOT. 'img'. DS. 'signature-img'. DS. $filename;

                    if($file){
                        $file->moveTo($filepath);
                    }

                    $layout->signature = 'signature-img/'. ($filename);
                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }

            }

            if ($this->Layouts->save($layout)) {
                $result = ['message' => ucwords('The layout has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($layout->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $layout->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Layout id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $layout = $this->Layouts->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $layout = $this->Layouts->patchEntity($layout, $this->request->getData());

            $validator = new Validator();

            $validator
                ->requirePresence('file',true)
                ->allowEmptyFile('file', ucwords('please upload a image'),true)
                ->add('file', 'file',[
                    'rule' => function($value){
                        $mimes = ['image/png', 'image/jpg', 'image/jpeg', 'image/jfif'];

                        if(!in_array($value->getClientMediaType(), $mimes)){
                            return ucwords('only JPEG, JPG, PNG, & JFIF are allowed');
                        }

                        return true;

                    }
                ]);

            $errors = $validator->validate($this->request->getData());
            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            if($layout->hasErrors() == false && strlen($this->request->getData('file')->getClientFileName()) > 0){

                $file = $this->request->getData('file');

                $filename = uniqid().$file->getClientFileName();

                try{
                    $path = WWW_ROOT. 'img'. DS. $layout->signature;
                    $folder = new File($path);
                    if($folder->exists()){
                        $folder->delete();
                    }

                    $filepath = WWW_ROOT. 'img'. DS. 'signature-img'. DS. $filename;

                    if($file){
                        $file->moveTo($filepath);
                    }

                    $layout->signature = 'signature-img/'. ($filename);
                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }

            }

            if ($this->Layouts->save($layout)) {
                $result = ['message' => ucwords('The layout has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($layout->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $layout->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($layout));
    }

    /**
     * Delete method
     *
     * @param string|null $id Layout id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $layout = $this->Layouts->get($id);

        try{
            $path = WWW_ROOT. 'img'. DS. $layout->signature;
            $folder = new File($path);
            if($folder->exists()){
                $folder->delete();
            }
        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Layouts->delete($layout)) {
            $result = ['message' => ucwords('The layout has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The layout has not been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $layout = $this->Layouts->get($id,[
            'withDeleted'
        ]);

        if ($this->Layouts->restore($layout)) {
            $result = ['message' => ucwords('The layout has been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The layout has not been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $layout = $this->Layouts->get($id,[
            'withDeleted'
        ]);

        try{
            $path = WWW_ROOT. 'img'. DS. $layout->signature;
            $folder = new File($path);
            if($folder->exists()){
                $folder->delete();
            }
        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Layouts->hardDelete($layout)) {
            $result = ['message' => ucwords('The layout has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The layout has not been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

}
