<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Survey Entity
 *
 * @property int $id
 * @property string $ca_no
 * @property string $transaction_code
 * @property int $visitor_id
 * @property int $subject_id
 * @property int $customer_type_id
 * @property int $service_id
 * @property float $score
 * @property string $purpose
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\CustomerType $customer_type
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\Service $service
 * @property \App\Model\Entity\Answer[] $answers
 * @property \App\Model\Entity\Visitor $visitor
 */
class Survey extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ca_no' => true,
        'transaction_code' => true,
        'visitor_id' => true,
        'subject_id' => true,
        'customer_type_id' => true,
        'service_id' => true,
        'score' => true,
        'purpose' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'customer_type' => true,
        'subject' => true,
        'service' => true,
        'answers' => true,
        'visitor' => true,
    ];

    protected function _setPurpose($value){
        return ucwords($value);
    }

}
