<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Option Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $question_id
 * @property int $subject_id
 * @property int $choice_id
 * @property int $trigger_charter
 * @property int $position
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Question $question
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\Choice $choice
 */
class Option extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'question_id' => true,
        'subject_id' => true,
        'choice_id' => true,
        'trigger_charter' => true,
        'position' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'question' => true,
        'subject' => true,
        'choice' => true,
    ];
}
