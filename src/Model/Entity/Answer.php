<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Answer Entity
 *
 * @property int $id
 * @property int $survey_id
 * @property int $question_id
 * @property int $choice_id
 * @property float $points
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Survey $survey
 * @property \App\Model\Entity\Question $question
 * @property \App\Model\Entity\Choice $choice
 */
class Answer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'survey_id' => true,
        'question_id' => true,
        'choice_id' => true,
        'points' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'survey' => true,
        'question' => true,
        'choice' => true,
    ];
}
