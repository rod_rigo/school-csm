<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Span Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $span
 * @property int $start_age
 * @property int $end_age
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Visitor[] $visitors
 */
class Span extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'span' => true,
        'start_age' => true,
        'end_age' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'visitors' => true,
    ];

    protected function _setSpan($value){
        return ucwords($value);
    }

}
