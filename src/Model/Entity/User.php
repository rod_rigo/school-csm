<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $is_admin
 * @property string $token
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Choice[] $choices
 * @property \App\Model\Entity\CustomerType[] $customer_types
 * @property \App\Model\Entity\Question[] $questions
 * @property \App\Model\Entity\Service[] $services
 * @property \App\Model\Entity\Subject[] $subjects
 * @property \App\Model\Entity\Activity[] $activities
 * @property \App\Model\Entity\Database[] $databases
 * @property \App\Model\Entity\Export[] $exports
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'password' => true,
        'is_admin' => true,
        'token' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'choices' => true,
        'customer_types' => true,
        'questions' => true,
        'services' => true,
        'subjects' => true,
        'activities' => true,
        'databases' => true,
        'exports' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword($value){
        return (new DefaultPasswordHasher())->hash($value);
    }

    protected function _setName($value){
        return ucwords($value);
    }

}
