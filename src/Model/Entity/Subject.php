<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subject Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $subject
 * @property string $title
 * @property string $description
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Question[] $questions
 * @property \App\Model\Entity\Survey[] $surveys
 * @property \App\Model\Entity\Option[] $options
 */
class Subject extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'subject' => true,
        'title' => true,
        'description' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'questions' => true,
        'surveys' => true,
        'options' => true
    ];

    protected function _setSubject($value){
        return ucwords($value);
    }

    protected function _setTitle($value){
        return ucwords($value);
    }

}
