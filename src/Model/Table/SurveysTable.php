<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Surveys Model
 *
 * @property \App\Model\Table\CustomerTypesTable&\Cake\ORM\Association\BelongsTo $CustomerTypes
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\ServicesTable&\Cake\ORM\Association\BelongsTo $Services
 * @property \App\Model\Table\AnswersTable&\Cake\ORM\Association\HasMany $Answers
 *
 * @method \App\Model\Entity\Survey newEmptyEntity()
 * @method \App\Model\Entity\Survey newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Survey[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Survey get($primaryKey, $options = [])
 * @method \App\Model\Entity\Survey findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Survey patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Survey[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Survey|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Survey saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SurveysTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('surveys');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CustomerTypes', [
            'foreignKey' => 'customer_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Subjects', [
            'foreignKey' => 'subject_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Services', [
            'foreignKey' => 'service_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Answers', [
            'foreignKey' => 'survey_id',
        ]);
        $this->belongsTo('Visitors', [
            'foreignKey' => 'visitor_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){
            $connection->begin();
            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        if($entity->isDirty() && intval($i) == intval(0)){
            $connection->begin();
            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');
        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('ca_no')
            ->maxLength('ca_no', 255)
            ->requirePresence('ca_no', true)
            ->notEmptyString('ca_no', ucwords('please fill out this field'), false);

        $validator
            ->scalar('transaction_code')
            ->maxLength('transaction_code', 255)
            ->requirePresence('transaction_code', true)
            ->notEmptyString('transaction_code', ucwords('please fill out this field'), false);

        $validator
            ->scalar('subject_id')
            ->numeric('subject_id')
            ->requirePresence('subject_id', true)
            ->notEmptyString('subject_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('service_id')
            ->numeric('service_id')
            ->requirePresence('service_id', true)
            ->notEmptyString('service_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('customer_type_id')
            ->numeric('customer_type_id')
            ->requirePresence('customer_type_id', true)
            ->notEmptyString('customer_type_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('ca_no')
            ->maxLength('ca_no', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('ca_no', true)
            ->notEmptyString('ca_no', ucwords('please fill out this field'), false);

        $validator
            ->numeric('score')
            ->requirePresence('score', 'create')
            ->notEmptyString('score', ucwords('please fill out this field'), false);

        $validator
            ->scalar('purpose')
            ->maxLength('purpose', 4294967295)
            ->requirePresence('purpose', true)
            ->notEmptyString('purpose', ucwords('please fill out this field'), false);

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 4294967295)
            ->requirePresence('remarks', true)
            ->notEmptyString('remarks', ucwords('please fill out this field'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function cano(){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $survey = $this->find()
            ->where([
                'created >=' => $startMonth,
                'created <=' => $endMonth,
            ])->last();
        $year = date('Y');
        $month = date('m');
        $ca_no = (!empty($survey))? ($year).'-'.($month).'-'.(str_pad(strval(intval(intval(explode('-', $survey->ca_no)[2]) + 1)), 5, '0', STR_PAD_LEFT)): ($year).'-'.($month).'-'.(str_pad(strval('1'), 5, '0', STR_PAD_LEFT));
        return $ca_no;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['subject_id'], 'Subjects'), ['errorField' => 'subject_id']);
        $rules->add($rules->existsIn(['customer_type_id'], 'CustomerTypes'), ['errorField' => 'customer_type_id']);
        $rules->add($rules->existsIn(['service_id'], 'Services'), ['errorField' => 'service_id']);
        $rules->add($rules->existsIn(['visitor_id'], 'Visitors'), ['errorField' => 'visitor_id']);
        $rules->add($rules->isUnique(['transaction_code', 'customer_type_id'],ucwords('You already submitted this survey in this customer type')), ['errorField' => 'customer_type_id']);

        return $rules;
    }
}
