<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Visitors Model
 *
 * @property \App\Model\Table\SurveysTable&\Cake\ORM\Association\HasMany $Surveys
 *
 * @method \App\Model\Entity\Visitor newEmptyEntity()
 * @method \App\Model\Entity\Visitor newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Visitor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Visitor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Visitor findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Visitor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Visitor[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Visitor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Visitor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VisitorsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('visitors');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Spans', [
            'foreignKey' => 'span_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Surveys', [
            'foreignKey' => 'visitor_id',
            'joinType'   => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('gender_id')
            ->numeric('gender_id')
            ->requirePresence('gender_id', true)
            ->notEmptyString('gender_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name', ucwords('please fill out this field'),false);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('agency')
            ->maxLength('agency', 255)
            ->requirePresence('agency', 'create')
            ->notEmptyString('agency', ucwords('please fill out this field'),false);

        $validator
            ->scalar('transaction_code')
            ->maxLength('transaction_code', 255)
            ->requirePresence('transaction_code', 'create')
            ->notEmptyString('transaction_code', ucwords('please fill out this field'),false);

        $validator
            ->numeric('is_answered')
            ->requirePresence('is_answered', 'create')
            ->notEmptyString('is_answered', ucwords('please fill out this field'),false)
            ->add('is_answered','is_answered',[
                'rule' => function($value){
                    if(!in_array(intval($value),[0, 1])){
                        return ucwords('Visitor have not answered any surveys');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['gender_id'], 'Genders'), ['errorField' => 'gender_id']);
        $rules->add($rules->existsIn(['span_id'], 'Spans'), ['errorField' => 'span_id']);

        return $rules;
    }

    public function transactioncode(){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $transaction_code = $this->find()
            ->where([
                'created >=' => $startMonth,
                'created <=' => $endMonth,
            ])->count();
        $date = date('Y').date('m').date('d-');
        $transaction_code = $date.str_pad(strval($transaction_code+1), 1, '0', STR_PAD_LEFT);
        return $transaction_code;
    }

}
