<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \App\Model\Table\ChartersTable&\Cake\ORM\Association\HasMany $Charters
 * @property \App\Model\Table\ChoicesTable&\Cake\ORM\Association\HasMany $Choices
 * @property \App\Model\Table\CustomerTypesTable&\Cake\ORM\Association\HasMany $CustomerTypes
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\HasMany $Offices
 * @property \App\Model\Table\QuestionsTable&\Cake\ORM\Association\HasMany $Questions
 * @property \App\Model\Table\ServicesTable&\Cake\ORM\Association\HasMany $Services
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Charters', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Choices', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('CustomerTypes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Offices', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Questions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Services', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Subjects', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Activities', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Exports', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Databases', [
            'foreignKey' => 'user_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){
            $connection->begin();
            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        if($entity->isDirty() && intval($i) == intval(0)){
            $connection->begin();
            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');
        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('Please fill out this field'),false);

        $validator
            ->email('email')
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('Please fill out this field'),false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('password', 'create')
            ->notEmptyString('password', ucwords('Please fill out this field'),false);

        $validator
            ->notEmptyString('is_admin')
            ->requirePresence('is_admin', true)
            ->notEmptyString('is_admin', ucwords('Please fill out this field'), false)
            ->add('is_admin', 'is_admin',[
                'rule' => function($value){
                    $isAdmin = [0, 1];
                    if(!in_array($value, $isAdmin)){
                        return ucwords('please select admin value');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('token')
            ->maxLength('token', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('token', false)
            ->notEmptyString('token');

        $validator
            ->notEmptyString('is_active')
            ->requirePresence('is_active', true)
            ->notEmptyString('is_active', ucwords('Please fill out this field'), false)
            ->add('is_active', 'is_active',[
                'rule' => function($value){
                    $isActive = [0, 1];
                    if(!in_array($value, $isActive)){
                        return ucwords('please select active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'Users.id' => $userId]);
    }

    public function findAuth(Query $query, array $options)
    {
        $query->where([
            'email' => $options['username'],
            'is_active =' => intval(1)], [], true); // <-- true here means overwrite original query !IMPORTANT.
        return $query;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email'],ucwords('this email is already exists')), ['errorField' => 'email']);

        return $rules;
    }
}
