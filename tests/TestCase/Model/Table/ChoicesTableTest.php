<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChoicesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChoicesTable Test Case
 */
class ChoicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChoicesTable
     */
    protected $Choices;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Choices',
        'app.Users',
        'app.Answers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Choices') ? [] : ['className' => ChoicesTable::class];
        $this->Choices = $this->getTableLocator()->get('Choices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Choices);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ChoicesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ChoicesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
