<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpansTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpansTable Test Case
 */
class SpansTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SpansTable
     */
    protected $Spans;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Spans',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Spans') ? [] : ['className' => SpansTable::class];
        $this->Spans = $this->getTableLocator()->get('Spans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Spans);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SpansTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SpansTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
