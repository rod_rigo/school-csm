<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AlternativesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AlternativesTable Test Case
 */
class AlternativesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AlternativesTable
     */
    protected $Alternatives;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Alternatives',
        'app.Users',
        'app.Charters',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Alternatives') ? [] : ['className' => AlternativesTable::class];
        $this->Alternatives = $this->getTableLocator()->get('Alternatives', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Alternatives);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\AlternativesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\AlternativesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
