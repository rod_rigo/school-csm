<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$count = 0;
$column = 'A';

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue($column.(strval($count)), strtoupper('NO.'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue($column.(strval($count)), strtoupper('Reference code'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue($column.(strval($count)), strtoupper('DATE'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue($column.(strval($count)), strtoupper('Client type'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue($column.(strval($count)), strtoupper('sex'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue($column.(strval($count)), strtoupper('age'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$charter = 1;
$sdq = 1;
foreach ($questions as $question){
    $column++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($column.(strval($count)), strtoupper( boolval($question->is_charter)? 'CC'.$charter++: 'SQD'.$sdq++ ))
        ->getStyle($column.(strval($count)))
        ->getFont()
        ->setBold(true);
    $spreadsheet->getActiveSheet()
        ->getStyle($column.(strval($count)))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()
        ->getStyle($column.(strval($count)))
        ->getBorders()
        ->getBottom()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()
        ->getStyle($column.(strval($count)))
        ->getBorders()
        ->getLeft()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    foreach ($question->options as $key => $option){
        $column++;
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue($column.(strval($count)), strtoupper( intval($key + 1)))
            ->getStyle($column.(strval($count)))
            ->getFont()
            ->setBold(true);
        $spreadsheet->getActiveSheet()
            ->getStyle($column.(strval($count)))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()
            ->getStyle($column.(strval($count)))
            ->getBorders()
            ->getBottom()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()
            ->getStyle($column.(strval($count)))
            ->getBorders()
            ->getLeft()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    }

}


$spreadsheet->getActiveSheet()->getStyle('A'.(1).':'.($column).''.($count).'')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('BEDDBA');

$header = 'A';
foreach ($surveys as $key => $survey){
    $count++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($header.(strval($count)), strval($survey->transaction_code));
    $header++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($header.(strval($count)), $survey->ca_no);
    $header++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($header.(strval($count)), date('Y-m-d', strtotime($survey->created)));
    $header++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($header.(strval($count)), strtoupper($survey->customer_type->customer_type));
    $header++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($header.(strval($count)), strtoupper($survey->visitor->gender->gender));
    $header++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue($header.(strval($count)), strtoupper($survey->visitor->age));

    foreach ($questions as $k => $question){
        $header++;
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue($header.(strval($count)), strip_tags($question->question))
            ->getStyle($header.(strval($count)))
            ->getAlignment()->setWrapText(false);

        foreach ($survey->answers as $answer){
            if($answer->question_id == $question->id){
                foreach ($question->options as $option){
                    $header++;
                    if(intval($answer->choice_id) == intval($option->choice_id)){
                        $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue($header.(strval($count)), 1);
                        $spreadsheet->setActiveSheetIndex(0)
                            ->getStyle($header.(strval($count)))
                            ->getAlignment()
                            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                    }else{
                        $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue($header.(strval($count)), null);
                    }

                }
            }
        }

    }

    $header = 'A';
}

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')
    ->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->filename = ucwords($filename);
$export->mime = strtolower('excel');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)) {
// Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
    $writer->save('php://output');
    exit(0);
}