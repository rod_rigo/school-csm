<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question[]|\Cake\Collection\CollectionInterface $questions
 */

$months = [
    ['text' => 'January', 'value' => 'January'],
    ['text' => 'February', 'value' => 'February'],
    ['text' => 'March', 'value' => 'March'],
    ['text' => 'April', 'value' => 'April'],
    ['text' => 'May', 'value' => 'May'],
    ['text' => 'June', 'value' => 'June'],
    ['text' => 'July', 'value' => 'July'],
    ['text' => 'August', 'value' => 'August'],
    ['text' => 'September', 'value' => 'September'],
    ['text' => 'October', 'value' => 'October'],
    ['text' => 'November', 'value' => 'November'],
    ['text' => 'December', 'value' => 'December']
];

?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <?=$this->Form->create(null,['class' => 'row mb-3', 'id' => 'form', 'type' => 'file'])?>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->label('customer_type', ucwords('Customer Type'))?>
                    <?=$this->Form->select('customer_type', $customerTypes,[
                        'id' => 'customer-type',
                        'class' => 'form-control form-control-border',
                        'empty' => ucwords('Customer Type'),
                        'required' => true
                    ])?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->label('Services', ucwords('Services'))?>
                    <?=$this->Form->select('services', [],[
                        'id' => 'services',
                        'class' => 'form-control form-control-border',
                        'empty' => ucwords('Services')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                    <?=$this->Form->label('year', ucwords('Year'))?>
                    <?=$this->Form->year('year',[
                        'id' => 'year',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y'),
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('Year'),
                        'required' => true,
                        'min' => 2000,
                    ])?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 mt-4">
                    <?=$this->Form->label('month', ucwords('Month'))?>
                    <?=$this->Form->select('month', $months,[
                        'id' => 'month',
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('Month'),
                        'empty' => ucwords('Month')
                    ])?>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-end mt-4">
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-primary rounded-0',
                        'type' => 'submit'
                    ])?>
                </div>
                <?=$this->Form->end()?>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>CA No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>Agency/Office</th>
                                    <th>Purpose</th>
                                    <th>Customer Type</th>
                                    <th>Service</th>
                                    <th>Modified</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/division/services')?>
