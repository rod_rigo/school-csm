<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Span[]|\Cake\Collection\CollectionInterface $spans
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('span', ucwords('span'))?>
                        <?=$this->Form->text('span',[
                            'class' => 'form-control',
                            'id' => 'span',
                            'required' => true,
                            'placeholder' => ucwords('span'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->label('start_age', ucwords('start age'))?>
                        <?=$this->Form->number('start_age',[
                            'class' => 'form-control',
                            'id' => 'start-age',
                            'required' => true,
                            'placeholder' => ucwords('start age'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->label('end_age', ucwords('end age'))?>
                        <?=$this->Form->number('end_age',[
                            'class' => 'form-control',
                            'id' => 'end-age',
                            'required' => true,
                            'placeholder' => ucwords('end age'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>

                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Span">
            New Span
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Span</th>
                        <th>Ages</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<?=$this->Html->script('division/spans/index')?>

