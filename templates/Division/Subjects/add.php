<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subject $subject
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>
<style>
    #description {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
</style>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Subject Form</h3>
            </div>
            <?= $this->Form->create($subject,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('subject', ucwords('subject'))?>
                        <?= $this->Form->text('subject',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('subject'),
                            'id' => 'subject',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('title', ucwords('title'))?>
                        <?= $this->Form->text('title',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('title'),
                            'id' => 'title',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('description', ucwords('description'))?>
                        <?= $this->Form->textarea('description',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('description'),
                            'id' => 'description',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => @$auth['id']
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => 1
                ]);?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Subjects', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit'),
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('division/subjects/add')?>


