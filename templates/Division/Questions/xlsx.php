<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$count = 0;

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
    ->setCreator('')
    ->setTitle('')
    ->setSubject('');

foreach ($questions as $question){
    $count++;
    $spreadsheet->setActiveSheetIndex(0)
        ->mergeCells('A'.(intval($count)).':F'.(intval($count)))
        ->setCellValue('A'.(strval($count)), ucwords(strip_tags($question->question)))
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    $spreadsheet->setActiveSheetIndex(0)
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setBold(true);

    foreach ($question->answers as $answer){
        $count++;
        $spreadsheet->setActiveSheetIndex(0)
            ->mergeCells('A'.(intval($count)).':C'.(intval($count)))
            ->setCellValue('A'.(strval($count)), ucwords($answer->choice))
            ->getStyle('A'.(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->setActiveSheetIndex(0)
            ->mergeCells('D'.(intval($count)).':F'.(intval($count)))
            ->setCellValue('D'.(strval($count)), intval($answer->total))
            ->getStyle('D'.(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    }

    $count++;
}

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

// Save the spreadsheet
$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode(''.(strtoupper($subject->subject)).'-'.($startDate).'-'.($endDate).'.xlsx').'"');
$writer->save('php://output');
exit(0);