<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question $question
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<style>
    #question {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
</style>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Question Form</h3>
            </div>
            <?= $this->Form->create($question,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('subject_id', ucwords('subject'))?>
                        <?=$this->Form->select('subject_id', $subjects,[
                            'class' => 'form-control',
                            'id' => 'subject-id',
                            'required' => true,
                            'empty' => ucwords('Please Select subject')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('charter',[
                                'id' => 'charter',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('charter', ucwords('Charter'))?>
                        </div>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('question', ucwords('question'))?>
                        <?= $this->Form->textarea('question',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('question'),
                            'id' => 'question',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-column justify-content-start align-items-start">
                        <?php foreach ($choices as $key => $value):?>
                            <div class="icheck-primary m-1">
                                <?=$this->Form->checkbox('options[]',[
                                    'id' => 'choice-'.($key),
                                    'hiddenField' => false,
                                    'value' => $key
                                ])?>
                                <?=$this->Form->label('choice_'.($key),ucwords($value))?>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => @$auth['id']
                ]);?>
                <?= $this->Form->hidden('position',[
                    'id' => 'position',
                    'value' => $position,
                    'required' => true
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => 1,
                    'required' => true
                ]);?>
                <?= $this->Form->hidden('is_charter',[
                    'id' => 'is-charter',
                    'value' => 0,
                    'required' => true
                ]);?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Questions', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('division/questions/add')?>

