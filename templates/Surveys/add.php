<?php
/**
 * @var \App\View\AppView $this
 */

$background = \Cake\ORM\TableRegistry::getTableLocator()->get('Backgrounds')
    ->find()
    ->where([
        'is_active =' => intval(1)
    ])
    ->first();

?>

<style>
    #remarks {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
    select.form-control{
        height: 60px !important;
    }
</style>

<!-- page title -->
<section class="page-title-section overlay" style="object-fit: contain; background-repeat: no-repeat; background-size: 100% 100%;" data-background="<?=$this->Url->assetUrl('/img/'.(@$background->background))?>">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-inline custom-breadcrumb">
                    <li class="list-inline-item">
                        <a class="h2 text-primary font-secondary" href="javascript:void(0);">
                            <?=ucwords($subject->subject)?>
                        </a>
                    </li>
                    <li class="list-inline-item text-white h3 font-secondary @@nasted"></li>
                </ul>
                <p class="text-lighten">
                    <?=$subject->description?>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- /page title -->

<!-- contact -->
<section class="section bg-gray">
    <div class="container-fluid p-5">
        <?=$this->Form->create($survey,['type' => 'file', 'id' => 'form', 'class' => 'w-100'])?>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title"><?=ucwords($subject->title)?></h2>
                </div>
            </div>
        <div class="row d-flex justify-content-center align-items-start">
                <div class="col-md-12 col-md-9 col-lg-10">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label('transaction_code', ucwords('transaction code'))?>
                            </strong>
                            <?=$this->Form->text('transaction_code',[
                                'class' => 'form-control',
                                'placeholder' => ucwords('enter your transaction code'),
                                'required' => true,
                                'id' => 'transaction-code',
                                'autocomplete' => 'off'
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label('purpose', ucwords('purpose'))?>
                            </strong>
                            <?=$this->Form->textarea('purpose',[
                                'class' => 'form-control',
                                'placeholder' => ucwords('enter Your purpose of visit (specify)'),
                                'required' => true,
                                'id' => 'purpose',
                                'autocomplete' => 'off'
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label('customer_type_id', ucwords('customer type'))?>
                            </strong>
                            <?=$this->Form->select('customer_type_id', $customerTypes,[
                                'class' => 'form-control form-control-lg',
                                'placeholder' => ucwords('age'),
                                'required' => true,
                                'id' => 'customer-type-id',
                                'empty' => ucwords('select customer type')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label('service_id', ucwords('service'))?>
                            </strong>
                            <?=$this->Form->select('service_id', [],[
                                'class' => 'form-control form-control-lg',
                                'required' => true,
                                'id' => 'service-id',
                                'empty' => ucwords('choose customer type')
                            ])?>
                            <small></small>
                        </div>

                        <?php foreach ($questions as $key => $question):?>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-4 border-bottom my-2 pb-3">
                                <p class="mb-2">
                                    <?=$question->question?>
                                </p>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 d-sm-flex flex-sm-column align-items-sm-start d-md-flex flex-md-row justify-content-md-start align-items-md-center d-lg-flex flex-lg-row justify-content-lg-start align-items-lg-center">
                                        <?php foreach ($question->options as $option):?>
                                            <div class="icheck-primary mx-2">
                                                <div class="icheck-primary d-inline">
                                                    <input type="radio" id="choice-<?=$option->id?>-<?=$key?>" class="choices <?=boolval($question->is_charter)? 'charters': '';?>" points="<?=$option->choice->points?>" value="<?=$option->choice_id?>" data-id="<?=$key?>" trigger-charter="<?=$option->trigger_charter?>" required>
                                                    <label class="text-dark" for="choice-<?=$option->id?>-<?=$key?>">
                                                        <strong> <?=ucwords($option->choice->choice)?></strong>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                    <?=$this->Form->hidden('answers.'.($key).'.question_id',[
                                        'id' => 'answers-'.($key).'-question-id',
                                        'value' => $question->id,
                                        'required' => true,
                                        'class' => 'answers-question-id',
                                    ])?>
                                    <small></small>
                                    <?=$this->Form->hidden('answers.'.($key).'.choice_id',[
                                        'id' => 'answers-'.($key).'-choice-id',
                                        'required' => true,
                                        'class' => 'answers-choice-id',
                                        'charter' => $question->is_charter
                                    ])?>
                                    <small></small>
                                    <?=$this->Form->hidden('answers.'.($key).'.points',[
                                        'id' => 'answers-'.($key).'-points',
                                        'required' => true,
                                        'value' => 0,
                                        'class' => 'answers-points',
                                    ])?>
                                    <small></small>
                                </div>
                            </div>
                        <?php endforeach;?>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label('remarks', ucwords('Suggestions on how we can further improve our services'))?>
                            </strong>
                            <?= $this->Form->textarea('remarks',[
                                'class' => 'form-control',
                                'placeholder' => ucwords('remarks'),
                                'id' => 'remarks',
                                'pattern' => '(.){1,}',
                                'title' => ucwords('please fill out this field'),
                                'required' => false,
                                'value' => '-'
                            ]);?>
                            <small></small>
                        </div>

                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <?=$this->Form->hidden('score',[
                                'id' => 'score',
                                'required' => true,
                                'value' => intval(0)
                            ])?>
                            <?=$this->Form->hidden('ca_no',[
                                'id' => 'ca-no',
                                'required' => true,
                                'value' => intval($ca_no)
                            ])?>
                            <?=$this->Form->hidden('subject_id',[
                                'id' => 'subject-id',
                                'required' => true,
                                'value' => intval($subject->id)
                            ])?>
                            <?=$this->Form->button('Submit',[
                                'type' => 'submit',
                                'class' => 'btn btn-success'
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2" id="pending-transactions">

                </div>
            </div>
        <?=$this->Form->end()?>
    </div>
</section>
<!-- /contact -->

<?=$this->Html->script('surveys/add')?>
