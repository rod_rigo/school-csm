<?php

use Dompdf\Dompdf;
use Dompdf\Options;
use CodeItNow\BarcodeBundle\Utils\QrCode;

// Fetch data
$name = strtoupper($survey->visitor->name);
$office = ucwords($survey->visitor->agency);
$dateOfVisit = date('m/d/Y h:i A', strtotime($survey->visitor->created));
$purpose = ucwords($survey->purpose);

$path = WWW_ROOT.'img'.DS.'dpd.png';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$logo = 'data:'.($mime).';base64,'.($image);

$signature_path = WWW_ROOT.'img'.DS.$layout->signature;
$signature_image = base64_encode(file_get_contents($signature_path));
$signature_mime = mime_content_type($signature_path);
$signature = 'data:'.($signature_mime).';base64,'.($signature_image);

$officeLayout = ($layout->office)? '<p style="text-align: center;text-transform: uppercase;font-family: Trajan Pro;font-size: 16px;">'.(ucwords($layout->office)).'</p>': '';
$address = ($layout->address)? '<p style="text-align: center;text-transform: uppercase;font-family: Trajan Pro;font-size: 16px;">'.(ucwords($layout->address)).'</p>': '';

$qrCode = new QrCode();
$qrCode
    ->setText(strval($survey->ca_no))
    ->setSize(300)
    ->setPadding(10)
    ->setErrorCorrection('high')
    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
    ->setLabel(null)
    ->setLabelFontSize(16)
    ->setImageType(QrCode::IMAGE_TYPE_PNG)
;

// Setup Dompdf
$options = new Options();
$options->set('isPhpEnabled', true);
$options->set('isRemoteEnabled', true);
$options->set('isHtml5ParserEnabled', true);

// HTML content for the certificate
$html = '
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Certificate of Visit</title>
<style>
*{
    margin: 0;
    padding: 0;
}
@font-face {
    font-family: Old English Text MT;
    src: url("OLD.ttf")format("truetype");
    src: url("OLD.ttf")format("embedded-opentype"),
    url("OLD.ttf")format("woff2"),
    url("OLD.ttf")format("woff"),
    url("OLD.ttf")format("truetype"),
    url("OLD.ttf")format("svg");
}
@font-face {
    font-family: Trajan Pro;
    src: url("./Trajan Pro.ttf")format("truetype");
}
body {
    padding-top: 20px;
    padding-bottom: 0px;
    padding-left: 60px;
    padding-right: 60px;
}
table {
    width: 100%;
    border-collapse: collapse;
}

th, td {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 4px;
}

th {
    text-align: left;
    border: none;
}
img{
    height: 80px;
    width: 80px;
}
</style>
</head>
<body>

<div style="text-align: center;width: 100%;">
<img src="' . ($logo) . '" alt="Certificate Image">
</div>
<span style="font-size: 6x; position: absolute;top: 20px; right: 60px; color: red;font-weight: 400;"><b>CA No. : '.(strval($survey->ca_no)).'</b></span>
<img src="data:'.$qrCode->getContentType().';base64,'.$qrCode->generate().'" style="position: absolute;top: 60px;right: 60px;"/>

<p style="text-align: center; font-family: Old English Text MT; font-size: 18px; margin-bottom:-5px;">Republic of the Philippines</p>
<p style="text-align: center; font-family: Old English Text MT; font-size: 25px; margin-top: 0;">Department of Education</p>
<p style="text-align: center;text-transform: uppercase;font-family: Trajan Pro;font-size: 16px;">'.(ucwords($layout->region)).'</p>
<p style="text-align: center;text-transform: uppercase;font-family: Trajan Pro;font-size: 16px;">'.(ucwords($layout->division)).'</p>
'.($officeLayout).'
'.($address).'

<div style="border-bottom: 2px solid black; width: 100%; margin-top: 2px; margin-bottom: 10px;"></div>

<h3 style="margin-bottom: 2px;margin-top: 2px;text-align: center;">CERTIFICATE OF APPEARANCE</h3>

<p style="text-align: center;margin-top: 2px; margin-bottom: 5px;">This is to certify that this person has appeared in this office with the following details:</p>

<table>
    <tr>
        <th width="20%">Name:</th>
        <td width="80%">' . $name . '</td>
    </tr>
    <tr>
        <th width="20%">Agency/Office:</th>
        <td width="80%">' . ($office) . '</td>
    </tr>
    <tr>
        <th width="20%">Date of Visit:</th>
        <td width="80%">' . ($dateOfVisit) . '</td>
    </tr>
    <tr>
        <th width="20%">Purpose:</th>
        <td width="80%">' . ($purpose) . '</td>
    </tr>
</table>

<p style="text-align: center;margin-top: 5px;">Issued on the date indicated below at the Schools Division of Santiago City, Isabela.</p>

<img src="'.($signature).'" style="position: absolute; left: 48%; height: 40px" height="" alt="">
<p style="text-align: center; text-transform: uppercase;margin-bottom: 0;margin-top: 45px; position: relative;"><b>'.($layout->name).'</b></p>
<p style="text-align: center; text-transform: capitalize; margin-top: 5px; position: relative;"><b>'.($layout->position).'</b></p>

<span style="font-size: 0.7em; position: absolute;bottom: 65px; left: 60px;">Date/Time Printed:</span>
<span style="font-size: 0.9em; position: absolute;bottom: 48px; left: 75px;">'.(date('m/d/Y h:i:s A')).'</span>
</body>
</html>
';

// Instantiate Dompdf
$dompdf = new Dompdf($options);

// Load HTML content
$dompdf->loadHtml($html);

$dompdf->setPaper([0, 0, 828/2, 585], 'landscape');

// Render PDF (DOMPDF)
$dompdf->render();

// Output PDF to browser
$dompdf->stream('CERTIFICATE OF APPEARANCE.pdf', array('Attachment' => false));

exit(0);