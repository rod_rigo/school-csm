<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Background $background
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Background Form</h3>
            </div>
            <?= $this->Form->create($background,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                <img src="<?=$this->Url->assetUrl('/img/thumbnail.png')?>" class="img-thumbnail" id="logo-preview" alt="Logo Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 mt-4">

                                <div class="form-group">
                                    <?=$this->Form->label('logo_file', ucwords('Logo file'))?>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <?=$this->Form->file('logo_file',[
                                                'class' => 'custom-file-input rounded-0',
                                                'id' => 'logo-file',
                                                'data-target' => '#logo-preview',
                                                'accept' => 'image/*',
                                                'required' => true
                                            ])?>
                                            <?=$this->Form->label('logo_file', ucwords('Choose file'),[
                                                'class' => 'custom-file-label rounded-0'
                                            ])?>
                                        </div>
                                    </div>
                                    <small></small>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                <img src="<?=$this->Url->assetUrl('/img/thumbnail.png')?>" class="img-thumbnail" id="background-preview" alt="Background Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 mt-4">

                                <div class="form-group">
                                    <?=$this->Form->label('background_file', ucwords('background file'))?>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <?=$this->Form->file('background_file',[
                                                'class' => 'custom-file-input rounded-0',
                                                'id' => 'logo-file',
                                                'data-target' => '#background-preview',
                                                'accept' => 'image/*',
                                                'required' => true
                                            ])?>
                                            <?=$this->Form->label('background_file', ucwords('Choose file'),[
                                                'class' => 'custom-file-label rounded-0'
                                            ])?>
                                        </div>
                                    </div>
                                    <small></small>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                        <small></small>
                    </div>

                </div>

            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => @$auth['id']
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => 0,
                    'required' => true
                ]);?>
                <?= $this->Form->hidden('logo',[
                    'id' => 'logo',
                    'value' => uniqid(),
                    'required' => true
                ]);?>
                <?= $this->Form->hidden('background',[
                    'id' => 'is-active',
                    'value' => uniqid(),
                    'required' => true
                ]);?>

                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Questions', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/backgrounds/add')?>

