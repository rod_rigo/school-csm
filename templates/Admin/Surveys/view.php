<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-5 col-lg-5">
        <h4><?=$survey->ca_no?></h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <strong>Name:</strong> <?=strtoupper($survey->visitor->name)?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <strong>Email:</strong> <?=($survey->visitor->email)?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <strong>Sex:</strong> <?=ucwords($survey->visitor->gender->gender)?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-md-4 mt-lg-4">
                        <strong>Agency/Office:</strong> <?=ucwords($survey->agency_office)?>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <strong>Service:</strong> <?=ucwords($survey->service->service)?>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <dl>
                            <dt>Purpose</dt>
                            <dd><?=ucwords($survey->purpose)?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($survey->answers as $key => $answer):?>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$answer->question->question?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 d-sm-flex flex-sm-column justify-content-sm-start align-items-sm-start d-md-flex flex-md-row justify-content-md-around align-items-md-center d-lg-flex flex-lg-row justify-content-lg-around align-items-lg-center">
                            <?php foreach ($answer->question->options as $option):?>
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="<?=$key.'-'.$option->id?>" <?=(intval($option->choice_id) == intval($answer->choice_id))? 'checked': null;?> disabled>
                                    <label for="<?=$key.'-'.$option->id?>">
                                        <?=ucwords($option->choice->choice)?>
                                    </label>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <dl>
                    <dt>Remarks</dt>
                    <dd><?=$survey->remarks?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/surveys/view')?>
