<?php


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Add data to the first column dynamically
$data = ['Value 1', 'Value 2', 'Value 3', 'Value 4', 'Value 5']; // Example data
$column = 'A'; // Start with column A

$spreadsheet->getProperties()
    ->setCreator('asdasds')
    ->setTitle('asdsd')
    ->setSubject(ucwords('Survey Reports'));

//foreach ($data as $value) {
//    $spreadsheet->getActiveSheet()->setCellValue($column . '1', $value); // Set cell value
//    $column++; // Move to the next column
//}

// Set autofilter for the first row (assuming header row)
$lastColumn = $column; // Get the last column

// Save the spreadsheet
$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('sample.xlsx').'"');
$writer->save('php://output');
exit(0);