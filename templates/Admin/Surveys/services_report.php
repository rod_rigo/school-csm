<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

$count = 8;
$column = 'A';
$filename = $service->customer_type->customer_type.' '.$service->service;

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Client Type:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), ucwords($service->customer_type->customer_type))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('service:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), ucwords($service->service))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Total Number of Transactions:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), doubleval($total))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$count++;

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Age'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);

foreach ($spans as $span){
    $count++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($count), ucwords($span->span.':'))
        ->getStyle('A'.strval($count))
        ->getFont()
        ->setBold(true);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('B'.strval($count), doubleval($span->total))
        ->getStyle('B'.strval($count))
        ->getFont()
        ->setBold(false);
}

$count++;
$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Sex'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);

foreach ($genders as $gender){
    $count++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($count), ucwords($gender->gender.':'))
        ->getStyle('A'.strval($count))
        ->getFont()
        ->setBold(true);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('B'.strval($count), doubleval($gender->total))
        ->getStyle('B'.strval($count))
        ->getFont()
        ->setBold(false);
}

$isCharter = [
    ucwords('Service Quality Dimension'),
    ucwords('Citizen’s Charter Awareness'),
];

$type = [
    strtoupper('SQD'),
    strtoupper('CC'),
];

$choices = new \Cake\Collection\Collection($answers->toArray());

foreach ($collections as $key => $collection){
    $count++;
    $count++;;
    $checkCharter = $key;
    $firstMerge = 'A'.strval($count);

    $start = 0;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($count), ucwords($isCharter[intval($key)]))
        ->getStyle('A'.strval($count))
        ->getFont()
        ->setBold(true);

    $highest = [];
    foreach ($collection as $k => $v){
        $highest[] = [
            'max' => count($v['options'])
        ];
    }

    if(!boolval($checkCharter)){
        $count++;
        $max = (new \Cake\Collection\Collection($highest))->max('max');
        $mergeColumn = 'A';
        $header = 'B';
        for($i = 0; $i < intval(@$max['max']); $i++){
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue(strval($header).strval($count), intval($i + 1))
                ->getStyle(strval($header).strval($count))
                ->getFont()
                ->setBold(true);

            $spreadsheet->setActiveSheetIndex(0)
                ->getStyle(strval($header).strval($count))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $header++;
            $mergeColumn++;
        }

        $lastMerge = strval($mergeColumn).strval(intval($count-1));
        $spreadsheet->setActiveSheetIndex(0)
            ->mergeCells($firstMerge.':'.$lastMerge)
            ->getStyle($firstMerge)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    }else{

        $max = (new \Cake\Collection\Collection($highest))->max('max');
        $mergeColumn = 'A';
        $header = 'B';
        for($i = 0; $i < intval(@$max['max']); $i++){
            $header++;
            $mergeColumn++;
        }

        $lastMerge = strval($mergeColumn).strval(intval($count));
        $spreadsheet->setActiveSheetIndex(0)
            ->mergeCells($firstMerge.':'.$lastMerge)
            ->getStyle($firstMerge)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    }

    foreach ($collection as $k => $v){

        if(boolval($checkCharter)){
            $count++;
            $header = 'B';
            for($i = 0; $i < count($v['options']); $i++){
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue(strval($header).strval($count), intval($i + 1))
                    ->getStyle(strval($header).strval($count))
                    ->getFont()
                    ->setBold(true);

                $spreadsheet->setActiveSheetIndex(0)
                    ->getStyle(strval($header).strval($count))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $header++;
            }
        }

        $count++;
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.strval($count), ucwords($type[intval($v['is_charter'])]).intval($k + 1))
            ->getStyle('A'.strval($count))
            ->getFont()
            ->setBold(true);

        $column = 'A';
        foreach ($v['options'] as $key => $option){
            $column++;
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue(strval($column).strval($count), intval(@$choices->firstMatch(['question_id' => $option['question_id'], 'choice_id' => $option['choice_id']])['total']))
                ->getStyle(strval($column).strval($count))
                ->getFont()
                ->setBold(true);

        }
    }

}

$count++;
$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Certified true and correct:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), strtoupper('LEONIDA F. CULANG MPA'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), strtoupper('RICARDO Q. CABUGAO JR., MIT'))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('C'.strval($count), strtoupper('MA ANGELA L. PARAGAS'))
    ->getStyle('C'.strval($count))
    ->getFont()
    ->setBold(true);

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Administrative Officer V'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(false);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), ucwords('Information Technology Officer I'))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('C'.strval($count), ucwords('Administrative Officer IV'))
    ->getStyle('C'.strval($count))
    ->getFont()
    ->setBold(false);

//foreach ($collections as $key => $collection){
//    $count++;
//    $count++;
//    $start = 0;
//    $spreadsheet->setActiveSheetIndex(0)
//        ->setCellValue('A'.strval($count), ucwords($isCharter[intval($key)]))
//        ->getStyle('A'.strval($count))
//        ->getFont()
//        ->setBold(true);
//    foreach ($collection as $k => $v){
//        $count++;
//        $spreadsheet->setActiveSheetIndex(0)
//            ->setCellValue('A'.strval($count), strtoupper($type[intval($v['is_charter'])]).strval($start))
//            ->getStyle('A'.strval($count))
//            ->getFont()
//            ->setBold(true);
//        $spreadsheet->setActiveSheetIndex(0)
//            ->setCellValue('B'.strval($count), doubleval($v['total']))
//            ->getStyle('B'.strval($count))
//            ->getFont()
//            ->setBold(true);
//        $start++;
//    }
//}


// Set custom page size
$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4); // Change to desired paper size
$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE); // Set orientation if needed
$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1); // Fit to width
$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0); // Do not fit to height

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(250, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(250, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(250, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(250, 'pt');

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')
    ->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->filename = ucwords($filename);
$export->mime = strtolower('excel');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)) {
// Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
    $writer->save('php://output');
    exit(0);
}