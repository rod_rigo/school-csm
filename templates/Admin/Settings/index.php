<?php
/**
 * @var \App\View\AppView $this
 */
?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="row">
    <div class="col-sm-12 col-md-3 col-lg-2">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Visitors', 'action' => 'getNames'])?>" target="_blank" class="btn btn-secondary btn-block mb-3 rounded-0">
            Calibrate Names
        </a>
    </div>

    <div class="col-sm-12 col-md-9 col-lg-10">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-database-tab" data-toggle="pill" href="#custom-tabs-database" role="tab" aria-controls="custom-tabs-database" aria-selected="true">
                            Databases
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-activities-tab" data-toggle="pill" href="#custom-tabs-activities" role="tab" aria-controls="custom-tabs-activities" aria-selected="true">
                            Activities
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-exports-tab" data-toggle="pill" href="#custom-tabs-exports" role="tab" aria-controls="custom-tabs-exports" aria-selected="true">
                            Exports
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                    <div class="tab-pane fade active show" id="custom-tabs-database" role="tabpanel" aria-labelledby="custom-tabs-database-tab">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Database</th>
                                            <th>Modified By</th>
                                            <th>Created</th>
                                            <th>Options</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-activities" role="tabpanel" aria-labelledby="custom-tabs-activities-tab">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table id="activities-datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Model</th>
                                            <th>Action</th>
                                            <th>Original</th>
                                            <th>Data</th>
                                            <th>Modified By</th>
                                            <th>Created</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-exports" role="tabpanel" aria-labelledby="custom-tabs-exports-tab">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table id="exports-datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Filename</th>
                                            <th>Mime</th>
                                            <th>Modified By</th>
                                            <th>Created</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>

    <!-- /.col -->
</div>
<?=$this->Html->script('admin/settings/index')?>