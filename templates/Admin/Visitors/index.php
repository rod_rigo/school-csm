<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Visitor[]|\Cake\Collection\CollectionInterface $visitors
 */
?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <?=$this->Form->create(null,['class' => 'row mb-3', 'id' => 'form', 'type' => 'file'])?>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                    <?=$this->Form->date('start_date',[
                        'id' => 'start-date',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('Start Date'),
                        'required' => true,
                    ])?>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <?=$this->Form->label('end_date', ucwords('End Date'))?>
                    <?=$this->Form->date('end_date',[
                        'id' => 'end-date',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'),
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('End Date'),
                        'required' => true
                    ])?>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <?=$this->Form->label('records', ucwords('Records'))?>
                    <?=$this->Form->number('records',[
                        'id' => 'records',
                        'value' => 10000,
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('Records'),
                        'min' => 10000,
                        'max' => 50000,
                        'required' => true
                    ])?>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-end">
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-primary rounded-0',
                        'type' => 'submit'
                    ])?>
                </div>
                <?=$this->Form->end()?>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Transaction Code</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>Agency/Office</th>
                                    <th>Registered At</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/visitors/index')?>

