<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?=$this->Form->create(null,['id' => 'form', 'type' => 'file', 'class' => 'row mb-3'])?>
<div class="col-sm-12 col-md-5 col-lg-4 mt-2">
    <?=$this->Form->label('start_date', ucwords('start date'))?>
    <?=$this->Form->date('start_date',[
        'class' => 'form-control rounded-0',
        'id' => 'start-date',
        'required' => true,
        'title' => ucwords('please fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
    ])?>
</div>
<div class="col-sm-12 col-md-5 col-lg-4 mt-2">
    <?=$this->Form->label('end_date', ucwords('end date'))?>
    <?=$this->Form->date('end_date',[
        'class' => 'form-control rounded-0',
        'id' => 'end-date',
        'required' => true,
        'title' => ucwords('please fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'),
    ])?>
</div>
<div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end mt-2">
    <?=$this->Form->button('Submit',[
        'class' => 'btn btn-primary rounded-0',
        'type' => 'submit'
    ])?>
</div>
<?=$this->Form->end()?>

<div class="row">
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="far fa-file"></i>
                    </span>
            <div class="info-box-content">
                <span class="info-box-text">Today Survey</span>
                <span class="info-box-number" id="total-surveys-today">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="far fa-file"></i>
                    </span>
            <div class="info-box-content">
                <span class="info-box-text">Week Survey</span>
                <span class="info-box-number" id="total-surveys-week">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="far fa-file"></i>
                    </span>
            <div class="info-box-content">
                <span class="info-box-text">Month Survey</span>
                <span class="info-box-number" id="total-surveys-month">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="far fa-file"></i>
                    </span>
            <div class="info-box-content">
                <span class="info-box-text">Year Survey</span>
                <span class="info-box-number" id="total-surveys-year">0</span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 h-100">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"></h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-files-o"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#surveys-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#surveys-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#surveys-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#surveys-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <canvas id="surveys-chart" height="500" width="500" style="height: 600px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-4 col-lg-3 h-100">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"></h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-files-o"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#customer-types-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#customer-types-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#customer-types-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#customer-types-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <canvas id="customer-types-chart" height="500" width="500" style="height: 600px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-8 col-lg-9 h-100">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"></h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-files-o"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#genders-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#genders-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#genders-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#genders-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <canvas id="genders-chart" height="500" width="500" style="height: 600px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 h-100">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"></h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-files-o"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#spans-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#spans-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#spans-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#spans-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <canvas id="spans-chart" height="500" width="500" style="height: 600px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 h-100">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"></h5>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-files-o"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a href="javascript:void(0);" data-target="#services-chart" class="dropdown-item image">Image</a>
                            <a href="javascript:void(0);" data-target="#services-chart" class="dropdown-item print">Print</a>
                            <a href="javascript:void(0);" data-target="#services-chart" class="dropdown-item pdf">PDF</a>
                            <a href="javascript:void(0);" data-target="#services-chart" class="dropdown-item excel">XLSX</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <canvas id="services-chart" height="500" width="500" style="height: 600px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/dashboards/index')?>

