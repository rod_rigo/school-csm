<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question $question
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<style>
    #question {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
</style>

<script>
    var id = <?=$question->id?>;
</script>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'choice-form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('choice_id', ucwords('choice'))?>
                        <?=$this->Form->select('choice_id', $choices,[
                            'class' => 'form-control',
                            'id' => 'choice-id',
                            'required' => true,
                            'empty' => ucwords('please select a choice'),
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('trigger',[
                                'id' => 'trigger',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('trigger', ucwords('Trigger Charter'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?=$this->Form->hidden('question_id',[
                    'id' => 'question-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => $question->id
                ])?>
                <?=$this->Form->hidden('subject_id',[
                    'id' => 'subject-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => $question->subject_id
                ])?>
                <?=$this->Form->hidden('position',[
                    'id' => 'position',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$position->position)
                ])?>
                <?=$this->Form->hidden('trigger_charter',[
                    'id' => 'trigger-charter',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>

                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" title="Close" data-dismiss="modal">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Questions', 'action' => 'add'])?>" title="New Question" class="btn btn-primary rounded-0 mx-2">
            New Question
        </a>
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Option">
            New Option
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Question Form</h3>
            </div>
            <?= $this->Form->create($question,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('subject_id', ucwords('subject'))?>
                        <?=$this->Form->select('subject_id', $subjects,[
                            'class' => 'form-control',
                            'id' => 'subject-id',
                            'required' => true,
                            'empty' => ucwords('Please Select subject')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('charter',[
                                'id' => 'charter',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($question->is_charter)
                            ])?>
                            <?=$this->Form->label('charter', ucwords('Charter'))?>
                        </div>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($question->is_active)
                            ])?>
                            <?=$this->Form->label('active', ucwords('active'))?>
                        </div>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('question', ucwords('question'))?>
                        <?= $this->Form->textarea('question',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('question'),
                            'id' => 'question',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => @$auth['id']
                ]);?>
                <?= $this->Form->hidden('position',[
                    'id' => 'position',
                    'value' => $question->position,
                    'required' => true,
                ]);?>
                <?= $this->Form->hidden('is_charter',[
                    'id' => 'is-charter',
                    'value' => $question->is_charter,
                    'required' => true,
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => $question->is_active,
                    'required' => true,
                ]);?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Questions', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Choice</th>
                        <th>Trigger Charter</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/questions/edit')?>

