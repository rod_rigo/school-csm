<?php
/**
 * @var \App\View\AppView $this
 */
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="javascript:void(0);" class="brand-link">
        <img src="<?=$this->Url->assetUrl('img/deped.png')?>" alt="SDO Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">SCHOOL CSM</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
            <div class="info">
                <a href="javascript:void(0);" class="d-block">
                    <?=strtoupper(@$auth['name'])?>
                </a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">NAVIGATION</li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('dashboards'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('dashboards'))? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('dashboards') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Surveys</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'visitors'])?>" class="nav-link <?=(strtolower($controller) == strtolower('dashboards') && strtolower($action) == strtolower('visitors'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Visitors</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('users'))? 'active': null;?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'CustomerTypes', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('customertypes'))? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Customer Types
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Genders', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('genders'))? 'active': null;?>">
                        <i class="nav-icon fas fa-genderless"></i>
                        <p>
                            Genders
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Spans', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('spans'))? 'active': null;?>">
                        <i class="nav-icon fas fa-child"></i>
                        <p>
                            Spans
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Services', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('services'))? 'active': null;?>">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Services
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Subjects', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('subjects'))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Subjects
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Questions', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('questions'))? 'active': null;?>">
                        <i class="nav-icon fas fa-question-circle"></i>
                        <p>
                            Questions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Choices', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Choices'))? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            Choices
                        </p>
                    </a>
                </li>

                <li class="nav-item <?=(strtolower($controller) == strtolower('visitors'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('visitors'))? 'active': null;?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Visitors
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Visitors', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Visitors', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('today'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Visitors', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('week'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Visitors', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('month'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Visitors', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('year'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item <?=(strtolower($controller) == strtolower('surveys') && !in_array(strtolower($action),[strtolower('reports'), strtolower('services')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('surveys')  && !in_array(strtolower($action),[strtolower('reports'), strtolower('services')]) )? 'active': null;?>">
                        <i class="nav-icon fas fa-pencil-alt"></i>
                        <p>
                            Surveys
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('today'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('week'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('month'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('year'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('surveys') && in_array(strtolower($action),[strtolower('reports'), strtolower('services')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('surveys')  && in_array(strtolower($action),[strtolower('reports'), strtolower('services')]) )? 'active': null;?>">
                        <i class="nav-icon fa fa-file-excel"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'reports'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower('reports') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Questions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Surveys', 'action' => 'services'])?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower('services') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Services</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Backgrounds', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('backgrounds'))? 'active': null;?>">
                        <i class="nav-icon fas fa-image"></i>
                        <p>
                            Backgrounds
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Layouts', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('layouts'))? 'active': null;?>">
                        <i class="nav-icon fas fa-pen"></i>
                        <p>
                            Layouts
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Settings', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('settings') && strtolower('index') == strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            Settings
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($action) == strtolower('bin'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($action) == strtolower('bin'))? 'active': null;?>">
                        <i class="nav-icon fas fa-trash"></i>
                        <p>
                            Bin
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'CustomerTypes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('customertypes') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Customer Types</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Genders', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('genders') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Genders</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Spans', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('spans') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Spans</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Services', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('services') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Services</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Subjects', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('subjects') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Subjects</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Questions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('questions') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Questions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Choices', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('choices') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Choices</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Backgrounds', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('backgrounds') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Backgrounds</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Layouts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('layouts') && strtolower('bin') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Layouts</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
