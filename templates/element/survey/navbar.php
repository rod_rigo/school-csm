<?php
/**
 * @var \App\View\AppView $this
 */

$background = \Cake\ORM\TableRegistry::getTableLocator()->get('Backgrounds')
    ->find()
    ->where([
        'is_active =' => intval(1)
    ])
    ->first();

?>

<style>
    @media (max-width: 980px) {
        img[alt*="logos"]{
            /*display: none;*/
            height: 45px;
        }
    }
    @media (max-width: 700px) {
        img[alt*="logos"]{
            /*display: none;*/
            height: 45px;
        }
    }
</style>

<header class="fixed-top header">
    <!-- top header -->
    <div class="top-header py-2 bg-white">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-lg-4 text-center text-lg-left">
                    <a class="text-color mr-3" href="javascript:void(0);">
                        <strong><?=date('Y-m-d')?></strong>
                    </a>
                    <ul class="list-inline d-inline">
                        <li class="list-inline-item mx-0">
                            <a class="d-inline-block p-2 text-color" href="javascript:void(0);">
                                <i class="ti-calendar d-none"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-8 text-center text-lg-right">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a class="text-uppercase text-color p-sm-2 py-2 px-0 d-flex" href="javascript:void(0);">
                                <span id="time"><?=date('h:m:s A')?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- navbar -->
    <div class="navigation w-100 nav-bg">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light p-0">
                <a class="navbar-brand" href="javascript:void(0);">
                    <img src="<?=$this->Url->assetUrl('/img/deped-matatag.png')?>" alt="logos" style="object-fit: contain !important;" height="80" loading="lazy">
                    <img src="<?=$this->Url->assetUrl('/img/arta.png')?>" alt="logos" style="object-fit: contain !important;" height="80" loading="lazy">
                    <img src="<?=$this->Url->assetUrl('/img/'.(@$background->logo))?>" alt="logos" style="object-fit: contain !important;" height="80" loading="lazy">
                </a>
                <button class="navbar-toggler rounded-0" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navigation">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item <?=($this->Url->build() == $this->Url->build('/'))? 'active': null;?>">
                            <a class="nav-link" href="<?=$this->Url->build(['prefix' => null, 'controller' => 'Visitors', 'action' => 'index'])?>">
                                Register
                            </a>
                        </li>
                        <?php
                            $subjects = \Cake\ORM\TableRegistry::getTableLocator()->get('Subjects')->find()
                                ->where([
                                    'is_active =' => 1
                                ])
                                ->order([
                                    'subject' => 'ASC'
                                ],true);
                        ?>
                        <?php foreach ($subjects as $subject):?>
                            <li class="nav-item <?=($this->Url->build() == $this->Url->build(['prefix' => null, 'controller' => 'Surveys', 'action' => 'add', $subject->id]))? 'active': null;?>">
                                <a class="nav-link" href="<?=$this->Url->build(['prefix' => null, 'controller' => 'Surveys', 'action' => 'add', $subject->id])?>">
                                    <?=ucwords($subject->title)?>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
