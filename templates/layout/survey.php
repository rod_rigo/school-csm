<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">
    <?= $this->Html->meta('csrf-token',$this->request->getAttribute('csrfToken')) ?>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <?=$this->Html->css([
        '/plugins/fontawesome-free/css/all.min',
        '/jquery/css/jquery-ui',
        '/i-check/css/icheck-bootstrap',
        '/educenter/plugins/bootstrap/bootstrap.min',
        '/educenter/plugins/slick/slick',
        '/educenter/plugins/themify-icons/themify-icons',
        '/educenter/plugins/animate/animate',
        '/educenter/plugins/aos/aos',
        '/educenter/plugins/venobox/venobox',
        '/educenter/css/style'
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/chartjs/js/chart',
        '/ck-editor/js/ckeditor',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/school-csm/';
    </script>

</head>
<body>

<!-- header -->
<?=$this->element('survey/navbar')?>
<!-- /header -->

<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>

<!-- footer -->
<?=$this->element('survey/footer')?>
<!-- /footer -->


<?=$this->Html->script([
    '/educenter/plugins/bootstrap/bootstrap.min',
    '/educenter/plugins/slick/slick.min',
    '/educenter/plugins/aos/aos',
    '/educenter/plugins/venobox/venobox.min',
    '/educenter/plugins/mixitup/mixitup.min',
    '/educenter/js/script.js',
    'survey',
])?>

</body>
</html>
