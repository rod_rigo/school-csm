<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Visitor[]|\Cake\Collection\CollectionInterface $visitors
 */

$background = \Cake\ORM\TableRegistry::getTableLocator()->get('Backgrounds')
    ->find()
    ->where([
        'is_active =' => intval(1)
    ])
    ->first();

?>

<style>
    select.form-control{
        height: 60px !important;
    }
</style>

<!-- page title -->
<section class="page-title-section overlay" data-background="<?=$this->Url->assetUrl('/img/'.(@$background->background))?>" style="object-fit: contain; background-repeat: no-repeat; background-size: 100% 100%;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-inline custom-breadcrumb">
                    <li class="list-inline-item">
                        <a class="h2 text-primary font-secondary" href="javascript:void(0);">
                            Registration
                        </a>
                    </li>
                    <li class="list-inline-item text-white h3 font-secondary @@nasted"></li>
                </ul>
                <p class="text-lighten">
                </p>

                <p><span style="color:hsl(0,0%,100%);font-family:Arial, Helvetica, sans-serif;"><span style="-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;"><strong>Personal information shared will be kept confidential and you always have the option to not answer this form.</strong></span></span></p>
            </div>
        </div>
    </div>
</section>
<!-- /page title -->

<!-- contact -->
<section class="section bg-gray">
    <div class="container-fluid p-5">
        <?=$this->Form->create($entity,['type' => 'file', 'id' => 'form', 'class' => 'w-100'])?>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title">Visitor Information</h2>
            </div>
        </div>
        <div class="row d-flex justify-content-center align-items-start">
            <div class="col-sm-12 col-md-9 col-lg-10">
                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <strong>
                            <?=$this->Form->label('name', ucwords('name'))?>
                        </strong>
                        <?=$this->Form->text('name',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('enter your name (First Name, M.I., Last Name , Extension Name (If Applicable))'),
                            'required' => true,
                            'id' => 'name',
                            'autocomplete' => 'off'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <strong>
                            <?=$this->Form->label('email', ucwords('Email Address'))?>
                        </strong>
                        <?=$this->Form->email('email',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('enter your email (For DepEd Employees, Use Your DepEd Account)'),
                            'required' => true,
                            'id' => 'email',
                            'autocomplete' => 'off'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 mt-4">
                        <strong>
                            <?=$this->Form->label('age', ucwords('age'))?>
                        </strong>
                        <?=$this->Form->number('age',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('enter your age'),
                            'required' => true,
                            'id' => 'age',
                            'autocomplete' => 'off',
                            'min' => 18
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-8 mt-4">
                        <strong>
                            <?=$this->Form->label('gender_id', ucwords('gender'))?>
                        </strong>
                        <?=$this->Form->select('gender_id', $genders,[
                            'class' => 'form-control form-control-lg rounded-0',
                            'placeholder' => ucwords('gender'),
                            'required' => true,
                            'id' => 'gender-id',
                            'autocomplete' => 'off',
                            'empty' => ucwords('select your gender')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <strong>
                            <?=$this->Form->label('agency', ucwords('agency/Office'))?>
                        </strong>
                        <?=$this->Form->textarea('agency',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('enter your agency or Office you where from...'),
                            'required' => true,
                            'id' => 'agency',
                            'autocomplete' => 'off'
                        ])?>
                        <small></small>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-2" id="pending-transactions">

            </div>
        </div>

        <div class="row mt-4">
            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('is_answered',[
                    'id' => 'is-answered',
                    'required' => true,
                    'value' => intval(0)
                ])?>
                <?=$this->Form->hidden('transaction_code',[
                    'id' => 'transaction-code',
                    'required' => true,
                    'value' => strval($transaction_code)
                ])?>
                <?=$this->Form->button('Submit',[
                    'type' => 'submit',
                    'class' => 'btn btn-success'
                ])?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</section>
<!-- /contact -->

<?=$this->Html->script('visitors/index')?>