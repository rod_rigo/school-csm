<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="login-box">

    <div class="login-logo">
        <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle" height="100" width="140" loading="lazy" alt="Deped Logo" title="Depen Logo">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
            <div class="input-group mb-3">
                <?=$this->Form->email('email',[
                    'class' => 'form-control rounded-0',
                    'required' => true,
                    'id' => 'email',
                    'placeholder' => ucwords('Email'),
                ])?>
                <div class="input-group-append rounded-0">
                    <div class="input-group-text rounded-0">
                        <span class="fas fa-envelope rounded-0"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <?=$this->Form->password('password',[
                    'class' => 'form-control rounded-0',
                    'required' => true,
                    'id' => 'password',
                    'placeholder' => ucwords('Password'),
                ])?>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="icheck-primary">
                        <?=$this->Form->checkbox('remember_me',[
                            'id' => 'remember-me',
                            'hiddenField' => false,
                        ])?>
                        <?=$this->Form->label('remember_me',ucwords('Remember Me'))?>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <?=$this->Form->submit(ucwords('Submit'),[
                        'type' => 'submit',
                        'class' => 'btn btn-primary btn-block rounded-0'
                    ])?>
                </div>
                <!-- /.col -->
            </div>
            <?=$this->Form->end()?>
            <!-- /.social-auth-links -->
        </div>
        <!-- /.login-card-body -->
    </div>
</div>

<?=$this->Html->script('users/login')?>