/*
 Navicat Premium Data Transfer

 Source Server         : wampp
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : csm_school_version_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 25/02/2024 20:04:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `survey_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `choice_id` bigint UNSIGNED NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answers to surveys`(`survey_id` ASC) USING BTREE,
  INDEX `answers to questions`(`question_id` ASC) USING BTREE,
  INDEX `answers to choices`(`choice_id` ASC) USING BTREE,
  CONSTRAINT `answers to choices` FOREIGN KEY (`choice_id`) REFERENCES `choices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to surveys` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES (1, 1, 3, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (2, 1, 4, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (3, 1, 5, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (4, 1, 6, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (5, 1, 7, 6, 0, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (6, 1, 8, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (7, 1, 9, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (8, 1, 13, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (9, 1, 14, 7, 1, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (10, 1, 15, 13, 3, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (11, 1, 16, 18, 3, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (12, 2, 3, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (13, 2, 4, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (14, 2, 5, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (15, 2, 6, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (16, 2, 7, 5, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (17, 2, 8, 1, 5, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (18, 2, 9, 1, 5, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (19, 2, 13, 1, 5, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (20, 2, 14, 7, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (21, 2, 15, 11, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (22, 2, 16, 16, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (23, 3, 3, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (24, 3, 4, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (25, 3, 5, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (26, 3, 6, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (27, 3, 7, 6, 0, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (28, 3, 8, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (29, 3, 9, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (30, 3, 13, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (31, 3, 14, 7, 1, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (32, 3, 15, 13, 3, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (33, 3, 16, 18, 3, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (34, 4, 3, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (35, 4, 4, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (36, 4, 5, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (37, 4, 6, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (38, 4, 7, 6, 0, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (39, 4, 8, 1, 5, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (40, 4, 9, 1, 5, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (41, 4, 13, 1, 5, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (42, 4, 14, 7, 1, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (43, 4, 15, 13, 3, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (44, 4, 16, 18, 3, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (45, 5, 3, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (46, 5, 4, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (47, 5, 5, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (48, 5, 6, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (49, 5, 7, 6, 0, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (50, 5, 8, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (51, 5, 9, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (52, 5, 13, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (53, 5, 14, 7, 1, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (54, 5, 15, 13, 3, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (55, 5, 16, 18, 3, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (56, 6, 3, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (57, 6, 4, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (58, 6, 5, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (59, 6, 6, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (60, 6, 7, 6, 0, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (61, 6, 8, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (62, 6, 9, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (63, 6, 13, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (64, 6, 14, 7, 1, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (65, 6, 15, 13, 3, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (66, 6, 16, 18, 3, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (67, 7, 14, 10, 4, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (68, 7, 15, 15, 0, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (69, 7, 16, 15, 0, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (70, 7, 3, 3, 3, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (71, 7, 4, 3, 3, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (72, 7, 5, 2, 4, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (73, 7, 6, 4, 2, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (74, 7, 7, 3, 3, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (75, 7, 8, 2, 4, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (76, 7, 9, 3, 3, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (77, 7, 13, 4, 2, '2024-02-19 01:18:50', '2024-02-19 01:18:50', NULL);
INSERT INTO `answers` VALUES (78, 1, 14, 8, 2, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (79, 1, 15, 15, 0, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (80, 1, 16, 17, 2, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (81, 1, 3, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (82, 1, 4, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (83, 1, 5, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (84, 1, 6, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (85, 1, 7, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (86, 1, 8, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (87, 1, 9, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);
INSERT INTO `answers` VALUES (88, 1, 13, 3, 3, '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);

-- ----------------------------
-- Table structure for backgrounds
-- ----------------------------
DROP TABLE IF EXISTS `backgrounds`;
CREATE TABLE `backgrounds`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `background` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of backgrounds
-- ----------------------------
INSERT INTO `backgrounds` VALUES (1, 1, 'logo-img/65d0d0344092b377323874_2085746898448771_8555122959557130172_n.png', 'background-img/65d0d03441075427039964_1048259566254788_6512222671191062075_n.jpg', 1, '2024-02-18 06:43:05', '2024-02-26 00:15:57', NULL);

-- ----------------------------
-- Table structure for choices
-- ----------------------------
DROP TABLE IF EXISTS `choices`;
CREATE TABLE `choices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `choice` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `choices to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `choices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of choices
-- ----------------------------
INSERT INTO `choices` VALUES (1, 1, 'Strongly Agree (5)	', 5, '2024-02-14 17:45:26', '2024-02-14 23:47:19', NULL);
INSERT INTO `choices` VALUES (2, 1, 'Agree (4)', 4, '2024-02-14 17:45:32', '2024-02-14 23:47:24', NULL);
INSERT INTO `choices` VALUES (3, 1, 'Neither Agree Nor Disagree (3)', 3, '2024-02-14 23:45:16', '2024-02-14 23:47:28', NULL);
INSERT INTO `choices` VALUES (4, 1, 'Disagree (2)', 2, '2024-02-14 23:45:25', '2024-02-14 23:47:44', NULL);
INSERT INTO `choices` VALUES (5, 1, 'Strongly Disagree (1)', 1, '2024-02-14 23:47:57', '2024-02-14 23:47:57', NULL);
INSERT INTO `choices` VALUES (6, 1, 'Not Applicable', 0, '2024-02-14 23:48:13', '2024-02-14 23:48:13', NULL);
INSERT INTO `choices` VALUES (7, 2, 'I Know What A CC Is And I Saw This Office\'s CC.', 1, '2024-02-16 18:26:22', '2024-02-16 18:40:43', NULL);
INSERT INTO `choices` VALUES (8, 2, 'I Know What A CC Is But I Did NOT See This Office\'s CC.', 2, '2024-02-16 18:26:59', '2024-02-16 18:27:08', NULL);
INSERT INTO `choices` VALUES (9, 2, 'I Learned Of The CC Only When I Saw This Office\'s CC.', 3, '2024-02-16 18:27:41', '2024-02-16 18:27:41', NULL);
INSERT INTO `choices` VALUES (10, 2, 'I Do Not Know What A CC Is And I Did Not See One In This Office. (Answer \'N/A\' On CC2 And CC3)', 4, '2024-02-16 18:28:56', '2024-02-16 18:29:10', NULL);
INSERT INTO `choices` VALUES (11, 2, 'Easy To See', 1, '2024-02-16 18:29:31', '2024-02-16 18:29:31', NULL);
INSERT INTO `choices` VALUES (12, 2, 'Somewhat Easy To See', 2, '2024-02-16 18:29:47', '2024-02-16 18:29:47', NULL);
INSERT INTO `choices` VALUES (13, 2, 'Difficult To See', 3, '2024-02-16 18:30:05', '2024-02-16 18:30:05', NULL);
INSERT INTO `choices` VALUES (14, 2, 'Not Visible At All', 4, '2024-02-16 18:30:20', '2024-02-16 18:30:20', NULL);
INSERT INTO `choices` VALUES (15, 2, 'N/A', 0, '2024-02-16 18:30:31', '2024-02-16 18:30:31', NULL);
INSERT INTO `choices` VALUES (16, 2, 'Helped Very Much', 1, '2024-02-16 18:30:51', '2024-02-16 18:30:51', NULL);
INSERT INTO `choices` VALUES (17, 2, 'Somewhat Helpful', 2, '2024-02-16 18:31:05', '2024-02-16 18:31:05', NULL);
INSERT INTO `choices` VALUES (18, 2, 'Did Not Help', 3, '2024-02-16 18:31:26', '2024-02-16 18:31:26', NULL);

-- ----------------------------
-- Table structure for customer_types
-- ----------------------------
DROP TABLE IF EXISTS `customer_types`;
CREATE TABLE `customer_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `customer_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_types to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `customer_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of customer_types
-- ----------------------------
INSERT INTO `customer_types` VALUES (9, 2, 'Business (private School, Corporations, Etc.)', '2024-02-09 15:54:56', '2024-02-15 06:54:53', NULL);
INSERT INTO `customer_types` VALUES (10, 1, 'Citizen (general Public, Learners, Parents, Former DepEd Employees, Researchers, NGOs Etc.)', '2024-02-09 15:55:03', '2024-02-09 15:55:03', NULL);
INSERT INTO `customer_types` VALUES (11, 1, 'Government Government (current DepEd Employees Or Employees Of Other Government Agencies & LGUs)', '2024-02-09 15:55:11', '2024-02-25 22:23:45', NULL);

-- ----------------------------
-- Table structure for genders
-- ----------------------------
DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `genders to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `genders to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of genders
-- ----------------------------
INSERT INTO `genders` VALUES (1, 1, 'Male', '2024-02-17 21:12:44', '2024-02-17 21:12:44', NULL);
INSERT INTO `genders` VALUES (2, 1, 'Female', '2024-02-17 21:14:22', '2024-02-25 22:31:47', NULL);

-- ----------------------------
-- Table structure for layouts
-- ----------------------------
DROP TABLE IF EXISTS `layouts`;
CREATE TABLE `layouts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `division` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `office` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `signature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `layouts to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `layouts to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of layouts
-- ----------------------------
INSERT INTO `layouts` VALUES (1, 1, 'Leonida F. Culang, MPA', 'Region II - Cagayan Valley', 'SCHOOLS DIVISION OF SANTIAGO CITY', 'Administrative Officer V', 'baluarter', 'sadad', 'signature-img/65db266fbe36dsignature.png', 1, '2024-02-23 21:33:06', '2024-02-26 03:37:19', NULL);

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `choice_id` bigint UNSIGNED NOT NULL,
  `trigger_charter` tinyint NOT NULL DEFAULT 0,
  `position` int NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `options to questions`(`question_id` ASC) USING BTREE,
  INDEX `options to subjects`(`subject_id` ASC) USING BTREE,
  INDEX `options to choices`(`choice_id` ASC) USING BTREE,
  CONSTRAINT `options to choices` FOREIGN KEY (`choice_id`) REFERENCES `choices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `options to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `options to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 86 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES (6, 1, 3, 1, 2, 0, 2, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (7, 1, 3, 1, 4, 0, 4, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (8, 1, 3, 1, 3, 0, 3, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (9, 1, 3, 1, 6, 0, 6, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (10, 1, 3, 1, 1, 0, 1, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (11, 1, 3, 1, 5, 0, 5, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (12, 1, 4, 1, 2, 0, 2, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (13, 1, 4, 1, 4, 0, 4, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (14, 1, 4, 1, 3, 0, 3, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (15, 1, 4, 1, 6, 0, 6, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (16, 1, 4, 1, 1, 0, 1, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (17, 1, 4, 1, 5, 0, 5, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (18, 1, 5, 1, 2, 0, 2, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (19, 1, 5, 1, 4, 0, 4, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (20, 1, 5, 1, 3, 0, 3, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (21, 1, 5, 1, 6, 0, 6, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (22, 1, 5, 1, 1, 0, 1, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (23, 1, 5, 1, 5, 0, 5, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (24, 1, 6, 1, 2, 0, 2, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (25, 1, 6, 1, 4, 0, 3, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (26, 1, 6, 1, 3, 0, 4, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (27, 1, 6, 1, 6, 0, 6, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (28, 1, 6, 1, 1, 0, 1, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (29, 1, 6, 1, 5, 0, 5, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (30, 1, 7, 1, 2, 0, 5, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (31, 1, 7, 1, 4, 0, 3, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (32, 1, 7, 1, 3, 0, 4, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (33, 1, 7, 1, 6, 0, 1, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (34, 1, 7, 1, 1, 0, 6, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (35, 1, 7, 1, 5, 0, 2, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (36, 1, 8, 1, 2, 0, 2, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (37, 1, 8, 1, 4, 0, 4, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (38, 1, 8, 1, 3, 0, 3, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (39, 1, 8, 1, 6, 0, 6, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (40, 1, 8, 1, 1, 0, 1, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (41, 1, 8, 1, 5, 0, 5, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (42, 1, 9, 1, 2, 0, 2, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (43, 1, 9, 1, 4, 0, 4, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (44, 1, 9, 1, 3, 0, 3, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (45, 1, 9, 1, 6, 0, 6, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (46, 1, 9, 1, 1, 0, 1, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (47, 1, 9, 1, 5, 0, 5, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (65, 2, 13, 1, 2, 0, 2, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (66, 2, 13, 1, 4, 0, 4, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (67, 2, 13, 1, 3, 0, 3, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (68, 2, 13, 1, 6, 0, 6, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (69, 2, 13, 1, 1, 0, 1, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (70, 2, 13, 1, 5, 0, 5, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (71, 1, 14, 1, 10, 1, 4, '2024-02-16 18:39:30', '2024-02-19 00:27:09', NULL);
INSERT INTO `options` VALUES (72, 2, 14, 1, 7, 0, 1, '2024-02-16 18:39:30', '2024-02-16 18:39:30', NULL);
INSERT INTO `options` VALUES (73, 2, 14, 1, 8, 0, 2, '2024-02-16 18:39:30', '2024-02-16 18:39:30', NULL);
INSERT INTO `options` VALUES (74, 2, 14, 1, 9, 0, 3, '2024-02-16 18:39:30', '2024-02-16 18:39:30', NULL);
INSERT INTO `options` VALUES (75, 2, 15, 1, 13, 0, 0, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (76, 2, 15, 1, 11, 0, 1, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (77, 2, 15, 1, 15, 0, 2, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (78, 2, 15, 1, 14, 0, 3, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (79, 2, 15, 1, 12, 0, 4, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (80, 2, 16, 1, 18, 0, 0, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);
INSERT INTO `options` VALUES (81, 2, 16, 1, 16, 0, 1, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);
INSERT INTO `options` VALUES (82, 2, 16, 1, 15, 0, 2, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);
INSERT INTO `options` VALUES (83, 2, 16, 1, 17, 0, 3, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_charter` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `position` int NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `questions to users`(`user_id` ASC) USING BTREE,
  INDEX `questions to subjects`(`subject_id` ASC) USING BTREE,
  CONSTRAINT `questions to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `questions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (3, 1, 1, '<p><span style=\"color:hsl(0,0%,0%);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD1 - I spent an acceptable amount of time to complete my transaction (Responsiveness).</span></span></p>', 0, 1, 4, '2024-02-14 23:49:12', '2024-02-15 00:33:50', NULL);
INSERT INTO `questions` VALUES (4, 1, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD2 - The office accurately informed and followed the transaction\'s requirements and steps (Reliability)</span></span></p>', 0, 1, 5, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `questions` VALUES (5, 1, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD3 - My transaction (including steps and payment) was simple and convenient (Access and Facilities)</span></span></p>', 0, 1, 6, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `questions` VALUES (6, 2, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">S</span></span><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">DQ4 - I easily found information about my transaction from the office or its website (Communication)</span></span></p>', 0, 1, 7, '2024-02-15 00:37:57', '2024-02-15 05:33:46', NULL);
INSERT INTO `questions` VALUES (7, 2, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD5 - I paid an acceptable amount of fees for my transaction (Costs)</span></span></p>', 0, 1, 8, '2024-02-15 00:38:26', '2024-02-16 18:44:56', NULL);
INSERT INTO `questions` VALUES (8, 1, 1, '<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD6 - I am confident my transaction was secure (Integrity)</span></span></p>', 0, 1, 9, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `questions` VALUES (9, 1, 1, '<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD7 - The office\'s support was quick to respond (Assurance)</span></span></p>', 0, 1, 10, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `questions` VALUES (13, 2, 1, '<p>SQD8 - I got what I needed from the government office (Outcome)</p>', 0, 1, 11, '2024-02-16 18:37:26', '2024-02-16 18:38:14', NULL);
INSERT INTO `questions` VALUES (14, 1, 1, '<p>Which of the following best describes your awareness of a CC?</p>', 1, 1, 1, '2024-02-16 18:39:30', '2024-02-18 02:22:25', NULL);
INSERT INTO `questions` VALUES (15, 1, 1, '<p>I aware of CC (answered 1-3 in CC1), would you say that the CC of this office was …?</p>', 1, 1, 2, '2024-02-16 18:42:20', '2024-02-18 02:22:06', NULL);
INSERT INTO `questions` VALUES (16, 1, 1, '<p>If aware of CC (answered codes 1-3 in CC1), how much did the CC help you in your transaction?</p>', 1, 1, 3, '2024-02-16 18:43:49', '2024-02-25 23:42:41', NULL);

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013, 'CreateRememberMeTokens', '2024-02-09 02:36:32', '2024-02-09 02:36:32', 0);

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (12, '2024-02-11 04:49:09', '2024-02-11 17:10:44', 'Users', '1', '646d8cb303dc2a080669e2973db0232162087084', 'ab12a8d4a4785e1b1f69d9f29435c777252efd78', '2024-03-12 17:10:44');
INSERT INTO `remember_me_tokens` VALUES (13, '2024-02-13 03:03:43', '2024-02-13 03:03:43', 'Users', '1', '89f7ec7aa797f536be86e02d72e1bc897a459329', 'c4069ae8628e2d2dce395441e2e4fc8ad47fac59', '2024-03-14 03:03:43');
INSERT INTO `remember_me_tokens` VALUES (17, '2024-02-16 19:25:10', '2024-02-17 19:13:34', 'Users', '2', '4b0dd4e57084d240ad69a7c09c263c76acdb749c', '5200e634ef77e8f6e064aaf8fcbd802cebfae412', '2024-03-18 19:13:33');
INSERT INTO `remember_me_tokens` VALUES (20, '2024-02-24 23:04:25', '2024-02-24 23:04:25', 'Users', '1', 'cea3029f68c08a21bda716373566daa9f446aa47', '3170a5af57256dc2c73df315a18a116d64d5005a', '2024-03-25 23:04:25');
INSERT INTO `remember_me_tokens` VALUES (25, '2024-02-26 02:06:59', '2024-02-26 02:06:59', 'Users', '3', '43b8800c72b9570b4e25aab25ca23988f2230649', 'b0a80471ee2250d7e50e21c8b86926472c8693c7', '2024-03-27 02:06:59');

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `customer_type_id` bigint UNSIGNED NOT NULL,
  `service` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `services to offices`(`customer_type_id` ASC) USING BTREE,
  INDEX `services to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `services to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `services to customer_types` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES (1, 1, 11, 'Ll', 1, '2024-02-26 03:10:16', '2024-02-26 03:17:20', NULL);

-- ----------------------------
-- Table structure for spans
-- ----------------------------
DROP TABLE IF EXISTS `spans`;
CREATE TABLE `spans`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `span` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `start_age` int NOT NULL,
  `end_age` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `spans to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `spans to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of spans
-- ----------------------------
INSERT INTO `spans` VALUES (1, 1, '19 Or Lower', 1, 19, '2024-02-17 21:41:10', '2024-02-25 22:36:36', NULL);
INSERT INTO `spans` VALUES (2, 1, '20-34', 20, 34, '2024-02-17 21:42:23', '2024-02-17 21:42:23', NULL);
INSERT INTO `spans` VALUES (3, 1, '35-49', 35, 49, '2024-02-17 21:43:08', '2024-02-25 22:36:38', NULL);
INSERT INTO `spans` VALUES (4, 1, '50-64', 50, 64, '2024-02-17 21:43:23', '2024-02-25 22:36:40', NULL);
INSERT INTO `spans` VALUES (5, 1, '65 Or Higher', 65, 999, '2024-02-17 21:44:41', '2024-02-25 22:36:43', NULL);

-- ----------------------------
-- Table structure for subjects
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `subjects to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `subjects to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of subjects
-- ----------------------------
INSERT INTO `subjects` VALUES (1, 1, 'SDO-Santiago City Client Satisfaction Measurement (CSM)', 'CSM', '<p><span style=\"color:hsl(0,0%,100%);font-family:Arial, Helvetica, sans-serif;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>The Client Satisfaction (CSM) tracks the customer experience of government offices. Your feedback on your recently concluded transaction will help this office provide better service. Personal information shared will be kept confidential and you always have the option to not answer this form.</strong></span></span></p>', 1, '2024-02-14 17:36:23', '2024-02-18 07:55:31', NULL);

-- ----------------------------
-- Table structure for surveys
-- ----------------------------
DROP TABLE IF EXISTS `surveys`;
CREATE TABLE `surveys`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `ca_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `visitor_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `customer_type_id` bigint UNSIGNED NOT NULL,
  `service_id` bigint UNSIGNED NOT NULL,
  `score` double NOT NULL,
  `purpose` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `surveys to customer_types`(`customer_type_id` ASC) USING BTREE,
  INDEX `surveys to services`(`service_id` ASC) USING BTREE,
  INDEX `surveys to subjects`(`subject_id` ASC) USING BTREE,
  INDEX `surveys to visitors`(`visitor_id` ASC) USING BTREE,
  CONSTRAINT `surveys to customer_types` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to services` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to visitors` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of surveys
-- ----------------------------
INSERT INTO `surveys` VALUES (1, '2024-02-00001', '20240225-1', 1, 1, 11, 1, 28, 'Asdas', '<p>-</p>', '2024-02-26 03:35:02', '2024-02-26 03:35:02', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_admin` tinyint NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Rodrigo', 'cabotaje.rodrigo@gmail.com', '$2y$10$WPga6gYfiFHrAGlPrmFMvuaKDSa1v9JN3cQgCcux6eR37VlPNw1TG', 1, '65db26523aaee65db26523aaf3', 1, '2024-02-09 10:10:05', '2024-02-26 03:36:50', NULL);
INSERT INTO `users` VALUES (2, 'Marc Jefferson Calaunan', 'joppycalaunan@gmail.com', '$2y$10$UZT34/crFUs1Ych/95vyeuR6J04MzGXwo6kdyK68hnuQU3DgHbvkC', 1, '65daff28e5112', 1, '2024-02-15 05:20:51', '2024-02-26 00:49:46', NULL);
INSERT INTO `users` VALUES (3, 'Division', 'division@gmail.com', '$2y$10$jw/Ale5dK2Kxy7WwFlvawOUK90MKlyLEg/dB5jlvfqg92LGWlkEc6', 0, '65db11440fbd465db11440fbd9', 1, '2024-02-26 01:41:04', '2024-02-26 02:07:00', NULL);

-- ----------------------------
-- Table structure for visitors
-- ----------------------------
DROP TABLE IF EXISTS `visitors`;
CREATE TABLE `visitors`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` int NOT NULL,
  `gender_id` bigint UNSIGNED NOT NULL,
  `span_id` bigint UNSIGNED NOT NULL,
  `agency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `transaction_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_answered` tinyint UNSIGNED NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `visitors to genders`(`gender_id` ASC) USING BTREE,
  INDEX `visitors to spans`(`span_id` ASC) USING BTREE,
  CONSTRAINT `visitors to genders` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `visitors to spans` FOREIGN KEY (`span_id`) REFERENCES `spans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of visitors
-- ----------------------------
INSERT INTO `visitors` VALUES (1, 'RODRIGO', 'cabotaje.rodrigo@gmail.com', 24, 1, 2, 'Aaa', '20240225-1', 1, '2024-02-26 03:33:43', '2024-02-26 03:35:02', NULL);

SET FOREIGN_KEY_CHECKS = 1;
