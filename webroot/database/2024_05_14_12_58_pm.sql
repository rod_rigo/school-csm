-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: localhost	Database: csm_school_version_db
-- ------------------------------------------------------
-- Server version 	8.0.31
-- Date: Tue, 14 May 2024 12:58:18 +0800

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40101 SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `original` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `activities to users` (`user_id`) USING BTREE,
  CONSTRAINT `activities to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `activities` VALUES (1,1,'USERS','EDIT','{\"password\":\"$2y$10$\\/DZbMYjbPDgiXCzkZ2KRWOeznx1oD.8r4u0lem0Q7yTpYsVMVwkfO\",\"token\":\"6641d9272c6c96641d9272c6d0\",\"modified\":\"2024-05-13T17:11:03+08:00\",\"id\":1,\"name\":\"Rodrigo\",\"email\":\"cabotaje.rodrigo@gmail.com\",\"is_admin\":1,\"is_division\":0,\"is_office\":0,\"is_active\":1,\"created\":\"2024-02-09T02:10:05+08:00\",\"deleted\":null}','{\"id\":1,\"name\":\"Rodrigo\",\"email\":\"cabotaje.rodrigo@gmail.com\",\"is_admin\":1,\"is_division\":0,\"is_office\":0,\"is_active\":1,\"created\":\"2024-02-09T02:10:05+08:00\",\"modified\":\"2024-05-14T10:44:43+08:00\",\"deleted\":null}','2024-05-14 10:44:43','2024-05-14 10:44:43',NULL),(2,1,'USERS','EDIT','{\"password\":\"$2y$10$61t5MFzJuThGUtZKDlXTIeF4DQyHK.xuK9OTb9dvCTmUSw0N3EUBm\",\"token\":\"6642d01b2bb636642d01b2bb66\",\"modified\":\"2024-05-14T10:44:43+08:00\",\"id\":1,\"name\":\"Rodrigo\",\"email\":\"cabotaje.rodrigo@gmail.com\",\"is_admin\":1,\"is_division\":0,\"is_office\":0,\"is_active\":1,\"created\":\"2024-02-09T02:10:05+08:00\",\"deleted\":null}','{\"id\":1,\"name\":\"Rodrigo\",\"email\":\"cabotaje.rodrigo@gmail.com\",\"is_admin\":1,\"is_division\":0,\"is_office\":0,\"is_active\":1,\"created\":\"2024-02-09T02:10:05+08:00\",\"modified\":\"2024-05-14T11:52:23+08:00\",\"deleted\":null}','2024-05-14 11:52:23','2024-05-14 11:52:23',NULL);
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `activities` with 2 row(s)
--

--
-- Table structure for table `answers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `survey_id` bigint unsigned NOT NULL,
  `question_id` bigint unsigned NOT NULL,
  `choice_id` bigint unsigned NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `answers to surveys` (`survey_id`) USING BTREE,
  KEY `answers to questions` (`question_id`) USING BTREE,
  KEY `answers to choices` (`choice_id`) USING BTREE,
  CONSTRAINT `answers to choices` FOREIGN KEY (`choice_id`) REFERENCES `choices` (`id`),
  CONSTRAINT `answers to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `answers to surveys` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `answers` VALUES (1,1,14,7,1,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(2,1,15,11,1,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(3,1,16,18,3,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(4,1,3,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(5,1,4,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(6,1,5,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(7,1,6,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(8,1,7,6,0,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(9,1,8,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(10,1,9,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL),(11,1,13,1,5,'2024-03-06 16:28:16','2024-03-06 16:28:16',NULL);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `answers` with 11 row(s)
--

--
-- Table structure for table `backgrounds`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backgrounds` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `background` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backgrounds`
--

LOCK TABLES `backgrounds` WRITE;
/*!40000 ALTER TABLE `backgrounds` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `backgrounds` VALUES (1,3,'logo-img/65db52ff945e965db33cea417a42292967_1836148866463251_279911638679683072_n.png','background-img/65db52ff94c4365db33cea4cf6Untitled-1.jpg',1,'2024-02-17 22:43:05','2024-02-25 22:47:27',NULL);
/*!40000 ALTER TABLE `backgrounds` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `backgrounds` with 1 row(s)
--

--
-- Table structure for table `choices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `choices` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `choice` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `choices to users` (`user_id`) USING BTREE,
  CONSTRAINT `choices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `choices`
--

LOCK TABLES `choices` WRITE;
/*!40000 ALTER TABLE `choices` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `choices` VALUES (1,1,'Strongly Agree (5)	',5,'2024-02-14 09:45:26','2024-02-14 15:47:19',NULL),(2,1,'Agree (4)',4,'2024-02-14 09:45:32','2024-02-14 15:47:24',NULL),(3,1,'Neither Agree Nor Disagree (3)',3,'2024-02-14 15:45:16','2024-02-14 15:47:28',NULL),(4,1,'Disagree (2)',2,'2024-02-14 15:45:25','2024-02-14 15:47:44',NULL),(5,1,'Strongly Disagree (1)',1,'2024-02-14 15:47:57','2024-02-14 15:47:57',NULL),(6,1,'Not Applicable',0,'2024-02-14 15:48:13','2024-02-14 15:48:13',NULL),(7,2,'I Know What A CC Is And I Saw This Office\'s CC.',1,'2024-02-16 10:26:22','2024-02-16 10:40:43',NULL),(8,2,'I Know What A CC Is But I Did NOT See This Office\'s CC.',2,'2024-02-16 10:26:59','2024-02-16 10:27:08',NULL),(9,2,'I Learned Of The CC Only When I Saw This Office\'s CC.',3,'2024-02-16 10:27:41','2024-02-16 10:27:41',NULL),(10,2,'I Do Not Know What A CC Is And I Did Not See One In This Office. (Answer \'N/A\' On CC2 And CC3)',4,'2024-02-16 10:28:56','2024-02-16 10:29:10',NULL),(11,2,'Easy To See',1,'2024-02-16 10:29:31','2024-02-16 10:29:31',NULL),(12,2,'Somewhat Easy To See',2,'2024-02-16 10:29:47','2024-02-16 10:29:47',NULL),(13,2,'Difficult To See',3,'2024-02-16 10:30:05','2024-02-16 10:30:05',NULL),(14,2,'Not Visible At All',4,'2024-02-16 10:30:20','2024-02-16 10:30:20',NULL),(15,2,'N/A',0,'2024-02-16 10:30:31','2024-02-16 10:30:31',NULL),(16,2,'Helped Very Much',1,'2024-02-16 10:30:51','2024-02-16 10:30:51',NULL),(17,2,'Somewhat Helpful',2,'2024-02-16 10:31:05','2024-02-16 10:31:05',NULL),(18,2,'Did Not Help',3,'2024-02-16 10:31:26','2024-02-16 10:31:26',NULL);
/*!40000 ALTER TABLE `choices` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `choices` with 18 row(s)
--

--
-- Table structure for table `customer_types`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `customer_type` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer_types to users` (`user_id`) USING BTREE,
  CONSTRAINT `customer_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_types`
--

LOCK TABLES `customer_types` WRITE;
/*!40000 ALTER TABLE `customer_types` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `customer_types` VALUES (9,2,'Business (private School, Corporations, Etc.)','2024-02-09 07:54:56','2024-02-14 22:54:53',NULL),(10,1,'Citizen (general Public, Learners, Parents, Former DepEd Employees, Researchers, NGOs Etc.)','2024-02-09 07:55:03','2024-02-09 07:55:03',NULL),(11,1,'Government Government (current DepEd Employees Or Employees Of Other Government Agencies & LGUs)','2024-02-09 07:55:11','2024-02-25 14:23:45',NULL);
/*!40000 ALTER TABLE `customer_types` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `customer_types` with 3 row(s)
--

--
-- Table structure for table `databases`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databases` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `databases to users` (`user_id`) USING BTREE,
  CONSTRAINT `databases to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databases`
--

LOCK TABLES `databases` WRITE;
/*!40000 ALTER TABLE `databases` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `databases` VALUES (1,1,'database/2024_05_13_05_23_pm.sql','2024-05-13 17:23:39','2024-05-13 17:23:39','2024-05-14 12:58:16');
/*!40000 ALTER TABLE `databases` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `databases` with 1 row(s)
--

--
-- Table structure for table `exports`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exports` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exports to users` (`user_id`),
  CONSTRAINT `exports to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exports`
--

LOCK TABLES `exports` WRITE;
/*!40000 ALTER TABLE `exports` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `exports` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `exports` with 0 row(s)
--

--
-- Table structure for table `genders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `genders to users` (`user_id`) USING BTREE,
  CONSTRAINT `genders to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genders`
--

LOCK TABLES `genders` WRITE;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `genders` VALUES (1,1,'Male','2024-02-17 13:12:44','2024-02-17 13:12:44',NULL),(2,1,'Female','2024-02-17 13:14:22','2024-02-25 14:31:47',NULL);
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `genders` with 2 row(s)
--

--
-- Table structure for table `layouts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layouts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `region` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `division` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `office` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `layouts to users` (`user_id`) USING BTREE,
  CONSTRAINT `layouts to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layouts`
--

LOCK TABLES `layouts` WRITE;
/*!40000 ALTER TABLE `layouts` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `layouts` VALUES (1,3,'BERNALDO S. VICENTE','Region II - Cagayan Valley','SCHOOLS DIVISION OF SANTIAGO CITY','Head Teacher III','BALUARTE ELEMENTARY SCHOOL','Provincial Road, Baluarte, Santiago City','signature-img/65db52c7c1247BERNIE.png',1,'2024-02-23 13:33:06','2024-02-25 22:46:31',NULL);
/*!40000 ALTER TABLE `layouts` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `layouts` with 1 row(s)
--

--
-- Table structure for table `names`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `names` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `age` int NOT NULL,
  `gender_id` bigint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `names`
--

LOCK TABLES `names` WRITE;
/*!40000 ALTER TABLE `names` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `names` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `names` with 0 row(s)
--

--
-- Table structure for table `options`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `question_id` bigint unsigned NOT NULL,
  `subject_id` bigint unsigned NOT NULL,
  `choice_id` bigint unsigned NOT NULL,
  `trigger_charter` tinyint NOT NULL DEFAULT '0',
  `position` int NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `options to questions` (`question_id`) USING BTREE,
  KEY `options to subjects` (`subject_id`) USING BTREE,
  KEY `options to choices` (`choice_id`) USING BTREE,
  CONSTRAINT `options to choices` FOREIGN KEY (`choice_id`) REFERENCES `choices` (`id`),
  CONSTRAINT `options to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `options to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `options` VALUES (6,1,3,1,2,0,2,'2024-02-14 15:49:12','2024-02-14 15:49:12',NULL),(7,1,3,1,4,0,4,'2024-02-14 15:49:12','2024-02-14 15:49:12',NULL),(8,1,3,1,3,0,3,'2024-02-14 15:49:12','2024-02-14 15:49:12',NULL),(9,1,3,1,6,0,6,'2024-02-14 15:49:12','2024-02-14 15:49:12',NULL),(10,1,3,1,1,0,1,'2024-02-14 15:49:12','2024-02-14 15:49:12',NULL),(11,1,3,1,5,0,5,'2024-02-14 15:49:12','2024-02-14 15:49:12',NULL),(12,1,4,1,2,0,2,'2024-02-14 16:34:12','2024-02-14 16:34:12',NULL),(13,1,4,1,4,0,4,'2024-02-14 16:34:12','2024-02-14 16:34:12',NULL),(14,1,4,1,3,0,3,'2024-02-14 16:34:12','2024-02-14 16:34:12',NULL),(15,1,4,1,6,0,6,'2024-02-14 16:34:12','2024-02-14 16:34:12',NULL),(16,1,4,1,1,0,1,'2024-02-14 16:34:12','2024-02-14 16:34:12',NULL),(17,1,4,1,5,0,5,'2024-02-14 16:34:12','2024-02-14 16:34:12',NULL),(18,1,5,1,2,0,2,'2024-02-14 16:35:33','2024-02-14 16:35:33',NULL),(19,1,5,1,4,0,4,'2024-02-14 16:35:33','2024-02-14 16:35:33',NULL),(20,1,5,1,3,0,3,'2024-02-14 16:35:33','2024-02-14 16:35:33',NULL),(21,1,5,1,6,0,6,'2024-02-14 16:35:33','2024-02-14 16:35:33',NULL),(22,1,5,1,1,0,1,'2024-02-14 16:35:33','2024-02-14 16:35:33',NULL),(23,1,5,1,5,0,5,'2024-02-14 16:35:33','2024-02-14 16:35:33',NULL),(24,1,6,1,2,0,2,'2024-02-14 16:37:57','2024-02-14 16:37:57',NULL),(25,1,6,1,4,0,3,'2024-02-14 16:37:57','2024-02-14 16:37:57',NULL),(26,1,6,1,3,0,4,'2024-02-14 16:37:57','2024-02-14 16:37:57',NULL),(27,1,6,1,6,0,6,'2024-02-14 16:37:57','2024-02-14 16:37:57',NULL),(28,1,6,1,1,0,1,'2024-02-14 16:37:57','2024-02-14 16:37:57',NULL),(29,1,6,1,5,0,5,'2024-02-14 16:37:57','2024-02-14 16:37:57',NULL),(30,1,7,1,2,0,5,'2024-02-14 16:38:26','2024-02-14 16:38:26',NULL),(31,1,7,1,4,0,3,'2024-02-14 16:38:26','2024-02-14 16:38:26',NULL),(32,1,7,1,3,0,4,'2024-02-14 16:38:26','2024-02-14 16:38:26',NULL),(33,1,7,1,6,0,1,'2024-02-14 16:38:26','2024-02-14 16:38:26',NULL),(34,1,7,1,1,0,6,'2024-02-14 16:38:26','2024-02-14 16:38:26',NULL),(35,1,7,1,5,0,2,'2024-02-14 16:38:26','2024-02-14 16:38:26',NULL),(36,1,8,1,2,0,2,'2024-02-14 16:39:06','2024-02-14 16:39:06',NULL),(37,1,8,1,4,0,4,'2024-02-14 16:39:06','2024-02-14 16:39:06',NULL),(38,1,8,1,3,0,3,'2024-02-14 16:39:06','2024-02-14 16:39:06',NULL),(39,1,8,1,6,0,6,'2024-02-14 16:39:06','2024-02-14 16:39:06',NULL),(40,1,8,1,1,0,1,'2024-02-14 16:39:06','2024-02-14 16:39:06',NULL),(41,1,8,1,5,0,5,'2024-02-14 16:39:06','2024-02-14 16:39:06',NULL),(42,1,9,1,2,0,2,'2024-02-14 16:39:38','2024-02-14 16:39:38',NULL),(43,1,9,1,4,0,4,'2024-02-14 16:39:38','2024-02-14 16:39:38',NULL),(44,1,9,1,3,0,3,'2024-02-14 16:39:38','2024-02-14 16:39:38',NULL),(45,1,9,1,6,0,6,'2024-02-14 16:39:38','2024-02-14 16:39:38',NULL),(46,1,9,1,1,0,1,'2024-02-14 16:39:38','2024-02-14 16:39:38',NULL),(47,1,9,1,5,0,5,'2024-02-14 16:39:38','2024-02-14 16:39:38',NULL),(65,2,13,1,2,0,2,'2024-02-16 10:37:26','2024-02-16 10:37:26',NULL),(66,2,13,1,4,0,4,'2024-02-16 10:37:26','2024-02-16 10:37:26',NULL),(67,2,13,1,3,0,3,'2024-02-16 10:37:26','2024-02-16 10:37:26',NULL),(68,2,13,1,6,0,6,'2024-02-16 10:37:26','2024-02-16 10:37:26',NULL),(69,2,13,1,1,0,1,'2024-02-16 10:37:26','2024-02-16 10:37:26',NULL),(70,2,13,1,5,0,5,'2024-02-16 10:37:26','2024-02-16 10:37:26',NULL),(71,1,14,1,10,1,4,'2024-02-16 10:39:30','2024-02-18 16:27:09',NULL),(72,2,14,1,7,0,1,'2024-02-16 10:39:30','2024-02-16 10:39:30',NULL),(73,2,14,1,8,0,2,'2024-02-16 10:39:30','2024-02-16 10:39:30',NULL),(74,2,14,1,9,0,3,'2024-02-16 10:39:30','2024-02-16 10:39:30',NULL),(75,2,15,1,13,0,3,'2024-02-16 10:42:22','2024-02-16 10:42:22',NULL),(76,2,15,1,11,0,1,'2024-02-16 10:42:22','2024-02-16 10:42:22',NULL),(77,2,15,1,15,0,5,'2024-02-16 10:42:22','2024-02-16 10:42:22',NULL),(78,2,15,1,14,0,4,'2024-02-16 10:42:22','2024-02-16 10:42:22',NULL),(79,2,15,1,12,0,2,'2024-02-16 10:42:22','2024-02-16 10:42:22',NULL),(80,2,16,1,18,0,0,'2024-02-16 10:43:52','2024-02-16 10:43:52',NULL),(81,2,16,1,16,0,1,'2024-02-16 10:43:52','2024-02-16 10:43:52',NULL),(82,2,16,1,15,0,2,'2024-02-16 10:43:52','2024-02-16 10:43:52',NULL),(83,2,16,1,17,0,3,'2024-02-16 10:43:52','2024-02-16 10:43:52',NULL);
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `options` with 61 row(s)
--

--
-- Table structure for table `questions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `subject_id` bigint unsigned NOT NULL,
  `question` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `is_charter` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '1',
  `position` int NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `questions to users` (`user_id`) USING BTREE,
  KEY `questions to subjects` (`subject_id`) USING BTREE,
  CONSTRAINT `questions to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
  CONSTRAINT `questions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `questions` VALUES (3,3,1,'<p><span style=\"color:hsl(0,0%,0%);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>SQD1 - I spent an acceptable amount of time to complete my transaction (Responsiveness).</strong></span></span></p><p><span style=\"background-color:rgb(248,248,248);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The willingness to help, assist, and provide prompt service to citizens/clients.</span></span></p>',0,1,4,'2024-02-14 15:49:12','2024-02-25 22:40:57',NULL),(4,3,1,'<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>SQD2 - The office accurately informed and followed the transaction\'s requirements and steps (Reliability).</strong></span></span></p><p><span style=\"background-color:rgb(248,249,250);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The provision of what is needed and what was promised, following the policy and standards, with zero to a minimal error rate.</span></span></p>',0,1,5,'2024-02-14 16:34:12','2024-02-25 22:41:28',NULL),(5,3,1,'<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>SQD3 - My transaction (including steps and payment) was simple and convenient (Access and Facilities).</strong></span></span></p><p><span style=\"background-color:rgb(248,249,250);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The convenience of location, ample amenities for comfortable transactions, use of clear signages and modes of technology.</span></span></p>',0,1,6,'2024-02-14 16:35:33','2024-02-25 22:41:57',NULL),(6,3,1,'<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>S</strong></span></span><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>DQ4 - I easily found information about my transaction from the office or its website (Communication).</strong></span></span></p><p><span style=\"background-color:rgb(248,248,248);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The act of keeping citizens and clients informed in a language they can easily understand, as well as listening to their feedback.</span></span></p>',0,1,7,'2024-02-14 16:37:57','2024-02-25 22:42:21',NULL),(7,3,1,'<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>SQD5 - I paid an acceptable amount of fees for my transaction (Costs).</strong></span></span></p><p><span style=\"background-color:rgb(248,249,250);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The satisfaction with timeliness of the billing, billing process/es, preferred methods of payment, reasonable payment period, value for money, the acceptable range of costs, and qualitative information on the cost of each service.</span></span></p>',0,1,8,'2024-02-14 16:38:26','2024-02-25 22:42:59',NULL),(8,3,1,'<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>SQD6 - I am confident my transaction was secure (Integrity).</strong></span></span></p><p><span style=\"background-color:rgb(248,248,248);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The assurance that there is honesty, justice, fairness, and trust in each service while dealing with the citizens/clients.</span></span></p>',0,1,9,'2024-02-14 16:39:06','2024-02-25 22:43:24',NULL),(9,3,1,'<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>SQD7 - The office\'s support was quick to respond (Assurance).</strong></span></span></p><p><span style=\"background-color:rgb(248,248,248);color:rgb(77,77,230);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">The capability of frontline staff to perform their duties, product and service knowledge, understand citizen/client needs, helpfulness, and good work relationships.</span></span></p>',0,1,10,'2024-02-14 16:39:38','2024-02-25 22:43:52',NULL),(13,3,1,'<p><strong>SQD8 - I got what I needed from the government office (Outcome).</strong></p><p><span style=\"background-color:rgb(248,248,248);color:rgb(77,77,230);font-family:Poppins, sans-serif;font-size:15px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:normal;widows:2;word-spacing:0px;\">The extent of achieving outcomes or realizing the intended benefits of government services.</span></span></p>',0,1,11,'2024-02-16 10:37:26','2024-02-25 22:44:19',NULL),(14,1,1,'<p>Which of the following best describes your awareness of a CC?</p>',1,1,1,'2024-02-16 10:39:30','2024-02-17 18:22:25',NULL),(15,3,1,'<p>If aware of CC (answered 1-3 in CC1), would you say that the CC of this office was …?</p>',1,1,2,'2024-02-16 10:42:20','2024-02-26 08:18:27',NULL),(16,1,1,'<p>If aware of CC (answered codes 1-3 in CC1), how much did the CC help you in your transaction?</p>',1,1,3,'2024-02-16 10:43:49','2024-02-25 15:42:41',NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `questions` with 11 row(s)
--

--
-- Table structure for table `remember_me_phinxlog`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_me_phinxlog` (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_me_phinxlog`
--

LOCK TABLES `remember_me_phinxlog` WRITE;
/*!40000 ALTER TABLE `remember_me_phinxlog` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013,'CreateRememberMeTokens','2024-02-08 18:36:32','2024-02-08 18:36:32',0);
/*!40000 ALTER TABLE `remember_me_phinxlog` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `remember_me_phinxlog` with 1 row(s)
--

--
-- Table structure for table `remember_me_tokens`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_me_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `U_token_identifier` (`model`,`foreign_id`,`series`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_me_tokens`
--

LOCK TABLES `remember_me_tokens` WRITE;
/*!40000 ALTER TABLE `remember_me_tokens` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `remember_me_tokens` VALUES (36,'2024-05-14 12:42:09','2024-05-14 12:42:09','Users','1','254ac2c1d50c90cc82883a7d12092093bad56d37','72b5ee24ddb99647a444629741a7b016bb47b5be','2024-06-13 12:42:09');
/*!40000 ALTER TABLE `remember_me_tokens` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `remember_me_tokens` with 1 row(s)
--

--
-- Table structure for table `services`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `customer_type_id` bigint unsigned NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `services to offices` (`customer_type_id`) USING BTREE,
  KEY `services to users` (`user_id`) USING BTREE,
  CONSTRAINT `services to customer_types` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`),
  CONSTRAINT `services to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `services` VALUES (1,2,9,'Certified True Copy (CTC) Of Documents (Business)',1,'2024-02-25 20:18:48','2024-02-25 20:26:15',NULL),(2,2,9,'Clearance (Business)',1,'2024-02-25 20:18:58','2024-02-25 20:26:03',NULL),(3,2,9,'School Permanent Records (Business)',1,'2024-02-25 20:19:07','2024-02-25 20:25:52',NULL),(4,2,11,'Public Assistance (feedback/complaints)',1,'2024-02-25 20:19:22','2024-02-25 20:19:22','2024-02-25 20:19:38'),(5,2,9,'Receiving/releasing Of Documents (Business)',1,'2024-02-25 20:19:34','2024-02-25 20:25:36',NULL),(6,2,9,'Public Assistance (feedback/complaints) - Business',1,'2024-02-25 20:19:56','2024-02-25 20:25:20',NULL),(7,2,9,'Use/rental Of School Facilities (gym, Etc) - Business',1,'2024-02-25 20:20:10','2024-02-25 20:25:12',NULL),(8,2,9,'Other Requests/inquiries (Business)',1,'2024-02-25 20:20:26','2024-02-25 20:20:26',NULL),(9,2,10,'Enrollment (Citizen)',1,'2024-02-25 20:20:41','2024-02-25 20:27:32',NULL),(10,2,10,'Teacher 1 Application (Citizen)',1,'2024-02-25 20:20:50','2024-02-25 20:27:48',NULL),(11,2,10,'Certified True Copy (CTC) Of Documents (Citizen)',1,'2024-02-25 20:21:01','2024-02-25 20:23:36',NULL),(12,2,10,'Personnel Records (COE, Service Record, Etc.) - Citizen',1,'2024-02-25 20:21:27','2024-02-25 20:28:00',NULL),(13,2,10,'Distribution Of Modules (Citizen)',1,'2024-02-25 20:21:38','2024-02-25 20:28:10',NULL),(14,2,10,'Borrowing Of Books/learning Materials (Citizen)',1,'2024-02-25 20:21:48','2024-02-25 20:28:22',NULL),(15,2,10,'Public Assistance (feedback/complaints) - Citizen',1,'2024-02-25 20:22:02','2024-02-25 20:24:08',NULL),(16,2,10,'Receiving/releasing Of Documents (Citizen)',1,'2024-02-25 20:22:12','2024-02-25 20:24:20',NULL),(17,2,10,'Clearance (Citizen)',1,'2024-02-25 20:22:24','2024-02-25 20:24:33',NULL),(18,2,10,'School Permanent Records (Citizen)',1,'2024-02-25 20:22:34','2024-02-25 20:24:44',NULL),(19,2,10,'Service Credits/Certification Of Compensatory Time Credits (Citizen)',1,'2024-02-25 20:22:43','2024-02-25 20:28:41',NULL),(20,2,10,'Use/rental Of School Facilities (gym, Etc) - Citizen',1,'2024-02-25 20:22:52','2024-02-25 20:24:57',NULL),(21,2,10,'Other Requests/inquiries (Citizen)',1,'2024-02-25 20:23:16','2024-02-25 20:23:16',NULL),(22,2,11,'Enrollment (Government)',1,'2024-02-25 20:27:19','2024-02-25 20:27:19',NULL),(23,2,11,'Teacher 1 Application (Government)',1,'2024-02-25 20:29:04','2024-02-25 20:29:04',NULL),(24,2,11,'Certified True Copy (CTC) Of Documents (Government)',1,'2024-02-25 20:29:20','2024-02-25 20:29:20',NULL),(25,2,11,'Personnel Records (COE, Service Record, Etc.) - Government',1,'2024-02-25 20:29:35','2024-02-25 20:29:35',NULL),(26,2,11,'Distribution Of Modules (Government)',1,'2024-02-25 20:29:53','2024-02-25 20:29:53',NULL),(27,2,11,'Borrowing Of Books/learning Materials (Government)',1,'2024-02-25 20:30:09','2024-02-25 20:30:09',NULL),(28,2,11,'Public Assistance (feedback/complaints) - Government',1,'2024-02-25 20:30:26','2024-02-25 20:30:26',NULL),(29,2,11,'Receiving/releasing Of Documents (Government)',1,'2024-02-25 20:30:41','2024-02-25 20:30:41',NULL),(30,2,11,'Clearance (Government)',1,'2024-02-25 20:30:53','2024-02-25 20:30:53',NULL),(31,2,11,'School Permanent Records (Government)',1,'2024-02-25 20:31:07','2024-02-25 20:31:07',NULL),(32,2,11,'Service Credits/Certification Of Compensatory Time Credits (Government)',1,'2024-02-25 20:31:22','2024-02-25 20:31:22',NULL),(33,2,11,'Use/rental Of School Facilities (gym, Etc) - Government',1,'2024-02-25 20:31:39','2024-02-25 20:31:39',NULL),(34,2,11,'Other Requests/inquiries (Government)',1,'2024-02-25 20:31:52','2024-02-25 20:31:52',NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `services` with 34 row(s)
--

--
-- Table structure for table `spans`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `span` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `start_age` int NOT NULL,
  `end_age` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `spans to users` (`user_id`) USING BTREE,
  CONSTRAINT `spans to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spans`
--

LOCK TABLES `spans` WRITE;
/*!40000 ALTER TABLE `spans` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `spans` VALUES (1,1,'19 Or Lower',1,19,'2024-02-17 13:41:10','2024-02-25 14:36:36',NULL),(2,1,'20-34',20,34,'2024-02-17 13:42:23','2024-02-17 13:42:23',NULL),(3,1,'35-49',35,49,'2024-02-17 13:43:08','2024-02-25 14:36:38',NULL),(4,1,'50-64',50,64,'2024-02-17 13:43:23','2024-02-25 14:36:40',NULL),(5,1,'65 Or Higher',65,999,'2024-02-17 13:44:41','2024-02-25 14:36:43',NULL);
/*!40000 ALTER TABLE `spans` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `spans` with 5 row(s)
--

--
-- Table structure for table `subjects`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `subjects to users` (`user_id`) USING BTREE,
  CONSTRAINT `subjects to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `subjects` VALUES (1,3,'Baluarte Elementary School Client Satisfaction Measurement (CSM)','SURVEY','<p><span style=\"color:hsl(0,0%,100%);font-family:Arial, Helvetica, sans-serif;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>The Client Satisfaction (CSM) tracks the customer experience of government offices. Your feedback on your recently concluded transaction will help this office provide better service. Personal information shared will be kept confidential and you always have the option to not answer this form.</strong></span></span></p>',1,'2024-02-14 09:36:23','2024-02-26 08:14:19',NULL);
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `subjects` with 1 row(s)
--

--
-- Table structure for table `surveys`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `ca_no` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `visitor_id` bigint unsigned NOT NULL,
  `subject_id` bigint unsigned NOT NULL,
  `customer_type_id` bigint unsigned NOT NULL,
  `service_id` bigint unsigned NOT NULL,
  `score` double NOT NULL,
  `purpose` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `surveys to customer_types` (`customer_type_id`) USING BTREE,
  KEY `surveys to services` (`service_id`) USING BTREE,
  KEY `surveys to subjects` (`subject_id`) USING BTREE,
  KEY `surveys to visitors` (`visitor_id`) USING BTREE,
  CONSTRAINT `surveys to customer_types` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`),
  CONSTRAINT `surveys to services` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  CONSTRAINT `surveys to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
  CONSTRAINT `surveys to visitors` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `surveys` VALUES (1,'2024-03-00001','20240306-1',1,1,10,10,40,'Lk\'bhliunijlkip','<p>-</p>','2024-03-06 16:28:16','2024-03-06 16:28:16',NULL);
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `surveys` with 1 row(s)
--

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_admin` tinyint NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `users` VALUES (1,'Rodrigo C. Cabotaje Jr','cabotaje.rodrigo@gmail.com','$2y$10$bxM4WpTrcOsp10nyMeSbguqNk3xOQZJ7joavE21t6Azs/bL5htcry',1,'6642eba1c3acc6642eba1c3ad0',1,'2024-02-09 02:10:05','2024-05-14 12:42:09',NULL),(2,'Marc Jefferson Calaunan','joppycalaunan@gmail.com','$2y$10$I5WXpTukIdgfViSEok/RWu32PfxvBIAJ0N48W8IwzutTc6VM29nke',1,'65dc18e2c219665dc18e2c2198',1,'2024-02-14 21:20:51','2024-02-26 12:51:46',NULL),(3,'Bernaldo S. Vicente','school@gmail.com','$2y$10$B/ArJ9LmSBGWvB.JP.FMbeOUYOGQvYAZi.abvbmc1KGhevkcxgAJq',0,'65dc47ba8652765dc47ba8652a',1,'2024-02-25 17:41:04','2024-02-26 16:11:38',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `users` with 3 row(s)
--

--
-- Table structure for table `visitors`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitors` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `age` int NOT NULL,
  `gender_id` bigint unsigned NOT NULL,
  `span_id` bigint unsigned NOT NULL,
  `agency` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_answered` tinyint unsigned NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `visitors to genders` (`gender_id`) USING BTREE,
  KEY `visitors to spans` (`span_id`) USING BTREE,
  CONSTRAINT `visitors to genders` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `visitors to spans` FOREIGN KEY (`span_id`) REFERENCES `spans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors`
--

LOCK TABLES `visitors` WRITE;
/*!40000 ALTER TABLE `visitors` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `visitors` VALUES (1,'KLJOIUIGHIU','jkkj@mail.com',34,1,2,',mhlbknujop;','20240306-1',1,'2024-03-06 16:26:58','2024-03-06 16:28:16',NULL);
/*!40000 ALTER TABLE `visitors` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `visitors` with 1 row(s)
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET AUTOCOMMIT=@OLD_AUTOCOMMIT */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Tue, 14 May 2024 12:58:18 +0800
