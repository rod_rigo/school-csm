/*
 Navicat Premium Data Transfer

 Source Server         : wampp
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : csm_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 18/02/2024 16:27:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `survey_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `choice_id` bigint UNSIGNED NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answers to surveys`(`survey_id` ASC) USING BTREE,
  INDEX `answers to questions`(`question_id` ASC) USING BTREE,
  INDEX `answers to choices`(`choice_id` ASC) USING BTREE,
  CONSTRAINT `answers to choices` FOREIGN KEY (`choice_id`) REFERENCES `choices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to surveys` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES (1, 1, 3, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (2, 1, 4, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (3, 1, 5, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (4, 1, 6, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (5, 1, 7, 6, 0, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (6, 1, 8, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (7, 1, 9, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (8, 1, 13, 1, 5, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (9, 1, 14, 7, 1, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (10, 1, 15, 13, 3, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (11, 1, 16, 18, 3, '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `answers` VALUES (12, 2, 3, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (13, 2, 4, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (14, 2, 5, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (15, 2, 6, 2, 4, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (16, 2, 7, 5, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (17, 2, 8, 1, 5, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (18, 2, 9, 1, 5, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (19, 2, 13, 1, 5, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (20, 2, 14, 7, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (21, 2, 15, 11, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (22, 2, 16, 16, 1, '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `answers` VALUES (23, 3, 3, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (24, 3, 4, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (25, 3, 5, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (26, 3, 6, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (27, 3, 7, 6, 0, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (28, 3, 8, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (29, 3, 9, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (30, 3, 13, 1, 5, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (31, 3, 14, 7, 1, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (32, 3, 15, 13, 3, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (33, 3, 16, 18, 3, '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `answers` VALUES (34, 4, 3, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (35, 4, 4, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (36, 4, 5, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (37, 4, 6, 1, 5, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (38, 4, 7, 6, 0, '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `answers` VALUES (39, 4, 8, 1, 5, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (40, 4, 9, 1, 5, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (41, 4, 13, 1, 5, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (42, 4, 14, 7, 1, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (43, 4, 15, 13, 3, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (44, 4, 16, 18, 3, '2024-02-18 23:31:33', '2024-02-18 23:31:33', NULL);
INSERT INTO `answers` VALUES (45, 5, 3, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (46, 5, 4, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (47, 5, 5, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (48, 5, 6, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (49, 5, 7, 6, 0, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (50, 5, 8, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (51, 5, 9, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (52, 5, 13, 1, 5, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (53, 5, 14, 7, 1, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (54, 5, 15, 13, 3, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (55, 5, 16, 18, 3, '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `answers` VALUES (56, 6, 3, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (57, 6, 4, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (58, 6, 5, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (59, 6, 6, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (60, 6, 7, 6, 0, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (61, 6, 8, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (62, 6, 9, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (63, 6, 13, 1, 5, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (64, 6, 14, 7, 1, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (65, 6, 15, 13, 3, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);
INSERT INTO `answers` VALUES (66, 6, 16, 18, 3, '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);

-- ----------------------------
-- Table structure for backgrounds
-- ----------------------------
DROP TABLE IF EXISTS `backgrounds`;
CREATE TABLE `backgrounds`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `background` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of backgrounds
-- ----------------------------
INSERT INTO `backgrounds` VALUES (1, 1, 'logo-img/65d0d0344092b377323874_2085746898448771_8555122959557130172_n.png', 'background-img/65d0d03441075427039964_1048259566254788_6512222671191062075_n.jpg', 1, '2024-02-18 06:43:05', '2024-02-18 07:26:44', NULL);

-- ----------------------------
-- Table structure for choices
-- ----------------------------
DROP TABLE IF EXISTS `choices`;
CREATE TABLE `choices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `choice` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `choices to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `choices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of choices
-- ----------------------------
INSERT INTO `choices` VALUES (1, 1, 'Strongly Agree (5)	', 5, '2024-02-14 17:45:26', '2024-02-14 23:47:19', NULL);
INSERT INTO `choices` VALUES (2, 1, 'Agree (4)', 4, '2024-02-14 17:45:32', '2024-02-14 23:47:24', NULL);
INSERT INTO `choices` VALUES (3, 1, 'Neither Agree Nor Disagree (3)', 3, '2024-02-14 23:45:16', '2024-02-14 23:47:28', NULL);
INSERT INTO `choices` VALUES (4, 1, 'Disagree (2)', 2, '2024-02-14 23:45:25', '2024-02-14 23:47:44', NULL);
INSERT INTO `choices` VALUES (5, 1, 'Strongly Disagree (1)', 1, '2024-02-14 23:47:57', '2024-02-14 23:47:57', NULL);
INSERT INTO `choices` VALUES (6, 1, 'Not Applicable', 0, '2024-02-14 23:48:13', '2024-02-14 23:48:13', NULL);
INSERT INTO `choices` VALUES (7, 2, 'I Know What A CC Is And I Saw This Office\'s CC.', 1, '2024-02-16 18:26:22', '2024-02-16 18:40:43', NULL);
INSERT INTO `choices` VALUES (8, 2, 'I Know What A CC Is But I Did NOT See This Office\'s CC.', 2, '2024-02-16 18:26:59', '2024-02-16 18:27:08', NULL);
INSERT INTO `choices` VALUES (9, 2, 'I Learned Of The CC Only When I Saw This Office\'s CC.', 3, '2024-02-16 18:27:41', '2024-02-16 18:27:41', NULL);
INSERT INTO `choices` VALUES (10, 2, 'I Do Not Know What A CC Is And I Did Not See One In This Office. (Answer \'N/A\' On CC2 And CC3)', 4, '2024-02-16 18:28:56', '2024-02-16 18:29:10', NULL);
INSERT INTO `choices` VALUES (11, 2, 'Easy To See', 1, '2024-02-16 18:29:31', '2024-02-16 18:29:31', NULL);
INSERT INTO `choices` VALUES (12, 2, 'Somewhat Easy To See', 2, '2024-02-16 18:29:47', '2024-02-16 18:29:47', NULL);
INSERT INTO `choices` VALUES (13, 2, 'Difficult To See', 3, '2024-02-16 18:30:05', '2024-02-16 18:30:05', NULL);
INSERT INTO `choices` VALUES (14, 2, 'Not Visible At All', 4, '2024-02-16 18:30:20', '2024-02-16 18:30:20', NULL);
INSERT INTO `choices` VALUES (15, 2, 'N/A', 5, '2024-02-16 18:30:31', '2024-02-16 18:30:31', NULL);
INSERT INTO `choices` VALUES (16, 2, 'Helped Very Much', 1, '2024-02-16 18:30:51', '2024-02-16 18:30:51', NULL);
INSERT INTO `choices` VALUES (17, 2, 'Somewhat Helpful', 2, '2024-02-16 18:31:05', '2024-02-16 18:31:05', NULL);
INSERT INTO `choices` VALUES (18, 2, 'Did Not Help', 3, '2024-02-16 18:31:26', '2024-02-16 18:31:26', NULL);

-- ----------------------------
-- Table structure for customer_types
-- ----------------------------
DROP TABLE IF EXISTS `customer_types`;
CREATE TABLE `customer_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `customer_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_types to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `customer_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of customer_types
-- ----------------------------
INSERT INTO `customer_types` VALUES (1, 1, 'Test', '2024-02-09 11:29:00', '2024-02-09 11:32:11', '2024-02-09 03:32:21');
INSERT INTO `customer_types` VALUES (7, 1, 'Aaa', '2024-02-09 11:31:54', '2024-02-09 11:31:54', '2024-02-09 03:32:03');
INSERT INTO `customer_types` VALUES (8, 1, 'Testsdsdsd', '2024-02-09 11:32:19', '2024-02-09 11:32:19', '2024-02-09 03:32:24');
INSERT INTO `customer_types` VALUES (9, 2, 'Business (private School, Corporations, Etc.)', '2024-02-09 15:54:56', '2024-02-15 06:54:53', NULL);
INSERT INTO `customer_types` VALUES (10, 1, 'Citizen (general Public, Learners, Parents, Former DepEd Employees, Researchers, NGOs Etc.)', '2024-02-09 15:55:03', '2024-02-09 15:55:03', NULL);
INSERT INTO `customer_types` VALUES (11, 1, 'Government Government (current DepEd Employees Or Employees Of Other Government Agencies & LGUs)', '2024-02-09 15:55:11', '2024-02-09 15:55:11', NULL);

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `departments to users`(`user_id` ASC) USING BTREE,
  INDEX `departments to offices`(`office_id` ASC) USING BTREE,
  CONSTRAINT `departments to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `departments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES (1, 1, 2, 'Accounting Unit', '2024-02-12 04:35:42', '2024-02-12 04:40:25', '2024-02-12 20:42:43');
INSERT INTO `departments` VALUES (2, 1, 4, 'N/A', '2024-02-12 04:42:52', '2024-02-12 04:42:52', '2024-02-12 20:42:46');
INSERT INTO `departments` VALUES (3, 1, 5, 'Cash', '2024-02-13 04:43:20', '2024-02-13 04:43:20', NULL);
INSERT INTO `departments` VALUES (4, 1, 5, 'Personnel', '2024-02-13 04:43:28', '2024-02-13 04:43:28', NULL);
INSERT INTO `departments` VALUES (5, 1, 5, 'Records', '2024-02-13 04:43:35', '2024-02-13 04:43:35', NULL);
INSERT INTO `departments` VALUES (6, 1, 5, 'Property And Supply', '2024-02-13 04:43:45', '2024-02-13 04:43:45', NULL);
INSERT INTO `departments` VALUES (7, 1, 5, 'General Services', '2024-02-13 04:43:54', '2024-02-13 04:43:54', NULL);
INSERT INTO `departments` VALUES (8, 1, 5, 'Procurement', '2024-02-13 04:44:06', '2024-02-13 04:44:06', NULL);
INSERT INTO `departments` VALUES (9, 1, 2, 'N/A', '2024-02-13 04:45:00', '2024-02-13 04:45:00', NULL);
INSERT INTO `departments` VALUES (10, 1, 3, 'N/A', '2024-02-13 04:45:21', '2024-02-13 04:45:21', NULL);
INSERT INTO `departments` VALUES (11, 1, 6, 'LRMS - Learning Resource Management Section', '2024-02-13 04:48:25', '2024-02-13 04:48:25', NULL);
INSERT INTO `departments` VALUES (12, 1, 6, 'Instructional Management Section', '2024-02-13 04:48:39', '2024-02-13 04:48:39', NULL);
INSERT INTO `departments` VALUES (13, 1, 6, 'Public School District Supervisor', '2024-02-13 04:48:57', '2024-02-13 04:48:57', NULL);
INSERT INTO `departments` VALUES (14, 1, 7, 'Accounting', '2024-02-13 04:49:24', '2024-02-13 04:49:24', NULL);
INSERT INTO `departments` VALUES (15, 1, 7, 'Budget', '2024-02-13 04:49:33', '2024-02-13 04:49:33', NULL);
INSERT INTO `departments` VALUES (16, 1, 9, 'Education Facilities', '2024-02-13 04:50:29', '2024-02-13 04:50:29', NULL);
INSERT INTO `departments` VALUES (17, 1, 9, 'HRD - Human Resource Development', '2024-02-13 04:50:49', '2024-02-13 04:50:49', NULL);
INSERT INTO `departments` VALUES (18, 1, 9, 'Planning & Research', '2024-02-13 04:51:03', '2024-02-13 04:51:03', NULL);
INSERT INTO `departments` VALUES (19, 1, 9, 'School Health', '2024-02-13 04:51:14', '2024-02-13 04:51:14', NULL);
INSERT INTO `departments` VALUES (20, 1, 9, 'SMM&E - School Management Monitoring And Evaluation Section', '2024-02-13 04:51:47', '2024-02-13 04:51:47', NULL);
INSERT INTO `departments` VALUES (21, 1, 9, 'SocMob - Social Mobilization And Networking', '2024-02-13 04:52:10', '2024-02-13 04:52:10', NULL);
INSERT INTO `departments` VALUES (22, 1, 4, 'N/A', '2024-02-13 05:52:05', '2024-02-13 05:52:05', NULL);
INSERT INTO `departments` VALUES (23, 1, 6, 'N/A', '2024-02-13 05:52:39', '2024-02-13 05:52:39', '2024-02-16 10:15:49');
INSERT INTO `departments` VALUES (24, 1, 8, 'N/A', '2024-02-13 05:52:49', '2024-02-13 05:52:49', NULL);
INSERT INTO `departments` VALUES (25, 2, 9, 'SGOD (Private School-related)', '2024-02-16 16:03:06', '2024-02-16 16:03:06', NULL);

-- ----------------------------
-- Table structure for genders
-- ----------------------------
DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `genders to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `genders to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of genders
-- ----------------------------
INSERT INTO `genders` VALUES (1, 1, 'Male', '2024-02-17 21:12:44', '2024-02-17 21:12:44', NULL);
INSERT INTO `genders` VALUES (2, 1, 'Female', '2024-02-17 21:14:22', '2024-02-17 21:14:22', NULL);

-- ----------------------------
-- Table structure for offices
-- ----------------------------
DROP TABLE IF EXISTS `offices`;
CREATE TABLE `offices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `office` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `offices to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `offices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of offices
-- ----------------------------
INSERT INTO `offices` VALUES (2, 1, 'ASDS - Assistant Schools Division Superintendent', 1, '2024-02-09 12:17:26', '2024-02-09 15:55:51', NULL);
INSERT INTO `offices` VALUES (3, 1, 'SDS - Schools Division Superintendent', 1, '2024-02-09 15:55:38', '2024-02-13 04:40:06', NULL);
INSERT INTO `offices` VALUES (4, 1, 'ICT', 1, '2024-02-09 15:55:58', '2024-02-09 15:55:58', NULL);
INSERT INTO `offices` VALUES (5, 1, 'Admin (Cash, Personnel, Records, Supply, General Services, Procurement)', 1, '2024-02-13 04:40:54', '2024-02-13 04:40:54', NULL);
INSERT INTO `offices` VALUES (6, 1, 'CID - Curriculum Implementation Division (LRMS, Instructional Management, PSDS)', 1, '2024-02-13 04:41:32', '2024-02-13 04:41:32', NULL);
INSERT INTO `offices` VALUES (7, 1, 'Finance (Accounting, Budget)', 1, '2024-02-13 04:41:47', '2024-02-13 04:41:47', NULL);
INSERT INTO `offices` VALUES (8, 1, 'Legal', 1, '2024-02-13 04:41:55', '2024-02-13 04:41:55', NULL);
INSERT INTO `offices` VALUES (9, 1, 'SGOD - School Governance And Operations Division (M&E, SocMob, Planning & Research, HRD, Facilities, School Health)', 1, '2024-02-13 04:42:31', '2024-02-13 04:42:31', NULL);

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `choice_id` bigint UNSIGNED NOT NULL,
  `trigger_charter` tinyint NOT NULL DEFAULT 0,
  `position` int NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `options to questions`(`question_id` ASC) USING BTREE,
  INDEX `options to subjects`(`subject_id` ASC) USING BTREE,
  INDEX `options to choices`(`choice_id` ASC) USING BTREE,
  CONSTRAINT `options to choices` FOREIGN KEY (`choice_id`) REFERENCES `choices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `options to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `options to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 84 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES (1, 1, 1, 1, 1, 0, 0, '2024-02-14 21:13:59', '2024-02-14 23:35:57', NULL);
INSERT INTO `options` VALUES (2, 1, 2, 1, 1, 0, 0, '2024-02-14 21:48:54', '2024-02-14 21:48:54', NULL);
INSERT INTO `options` VALUES (3, 1, 2, 1, 2, 0, 0, '2024-02-14 21:48:54', '2024-02-14 21:48:54', NULL);
INSERT INTO `options` VALUES (4, 1, 1, 1, 2, 0, 0, '2024-02-14 23:36:09', '2024-02-14 23:42:14', NULL);
INSERT INTO `options` VALUES (6, 1, 3, 1, 2, 0, 2, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (7, 1, 3, 1, 4, 0, 4, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (8, 1, 3, 1, 3, 0, 3, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (9, 1, 3, 1, 6, 0, 6, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (10, 1, 3, 1, 1, 0, 1, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (11, 1, 3, 1, 5, 0, 5, '2024-02-14 23:49:12', '2024-02-14 23:49:12', NULL);
INSERT INTO `options` VALUES (12, 1, 4, 1, 2, 0, 2, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (13, 1, 4, 1, 4, 0, 4, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (14, 1, 4, 1, 3, 0, 3, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (15, 1, 4, 1, 6, 0, 6, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (16, 1, 4, 1, 1, 0, 1, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (17, 1, 4, 1, 5, 0, 5, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `options` VALUES (18, 1, 5, 1, 2, 0, 2, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (19, 1, 5, 1, 4, 0, 4, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (20, 1, 5, 1, 3, 0, 3, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (21, 1, 5, 1, 6, 0, 6, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (22, 1, 5, 1, 1, 0, 1, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (23, 1, 5, 1, 5, 0, 5, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `options` VALUES (24, 1, 6, 1, 2, 0, 2, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (25, 1, 6, 1, 4, 0, 3, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (26, 1, 6, 1, 3, 0, 4, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (27, 1, 6, 1, 6, 0, 6, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (28, 1, 6, 1, 1, 0, 1, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (29, 1, 6, 1, 5, 0, 5, '2024-02-15 00:37:57', '2024-02-15 00:37:57', NULL);
INSERT INTO `options` VALUES (30, 1, 7, 1, 2, 0, 5, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (31, 1, 7, 1, 4, 0, 3, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (32, 1, 7, 1, 3, 0, 4, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (33, 1, 7, 1, 6, 0, 1, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (34, 1, 7, 1, 1, 0, 6, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (35, 1, 7, 1, 5, 0, 2, '2024-02-15 00:38:26', '2024-02-15 00:38:26', NULL);
INSERT INTO `options` VALUES (36, 1, 8, 1, 2, 0, 2, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (37, 1, 8, 1, 4, 0, 4, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (38, 1, 8, 1, 3, 0, 3, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (39, 1, 8, 1, 6, 0, 6, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (40, 1, 8, 1, 1, 0, 1, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (41, 1, 8, 1, 5, 0, 5, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `options` VALUES (42, 1, 9, 1, 2, 0, 2, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (43, 1, 9, 1, 4, 0, 4, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (44, 1, 9, 1, 3, 0, 3, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (45, 1, 9, 1, 6, 0, 6, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (46, 1, 9, 1, 1, 0, 1, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (47, 1, 9, 1, 5, 0, 5, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `options` VALUES (48, 1, 10, 1, 2, 0, 2, '2024-02-15 00:40:04', '2024-02-15 00:40:04', NULL);
INSERT INTO `options` VALUES (49, 1, 10, 1, 4, 0, 4, '2024-02-15 00:40:04', '2024-02-15 00:40:04', NULL);
INSERT INTO `options` VALUES (50, 1, 10, 1, 3, 0, 3, '2024-02-15 00:40:04', '2024-02-15 00:40:04', NULL);
INSERT INTO `options` VALUES (51, 1, 10, 1, 6, 0, 6, '2024-02-15 00:40:04', '2024-02-15 00:40:04', NULL);
INSERT INTO `options` VALUES (52, 1, 10, 1, 1, 0, 1, '2024-02-15 00:40:04', '2024-02-15 00:40:04', NULL);
INSERT INTO `options` VALUES (53, 1, 10, 1, 5, 0, 5, '2024-02-15 00:40:04', '2024-02-15 00:40:04', NULL);
INSERT INTO `options` VALUES (59, 2, 11, 1, 2, 0, 1, '2024-02-16 18:35:13', '2024-02-16 18:35:13', NULL);
INSERT INTO `options` VALUES (60, 2, 12, 1, 2, 0, 1, '2024-02-16 18:36:08', '2024-02-16 18:36:08', NULL);
INSERT INTO `options` VALUES (61, 2, 12, 1, 4, 0, 3, '2024-02-16 18:36:08', '2024-02-16 18:36:08', NULL);
INSERT INTO `options` VALUES (62, 2, 12, 1, 6, 0, 5, '2024-02-16 18:36:08', '2024-02-16 18:36:08', NULL);
INSERT INTO `options` VALUES (63, 2, 12, 1, 1, 0, 2, '2024-02-16 18:36:08', '2024-02-16 18:36:08', NULL);
INSERT INTO `options` VALUES (64, 2, 12, 1, 5, 0, 4, '2024-02-16 18:36:08', '2024-02-16 18:36:08', NULL);
INSERT INTO `options` VALUES (65, 2, 13, 1, 2, 0, 2, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (66, 2, 13, 1, 4, 0, 4, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (67, 2, 13, 1, 3, 0, 3, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (68, 2, 13, 1, 6, 0, 6, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (69, 2, 13, 1, 1, 0, 1, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (70, 2, 13, 1, 5, 0, 5, '2024-02-16 18:37:26', '2024-02-16 18:37:26', NULL);
INSERT INTO `options` VALUES (71, 1, 14, 1, 10, 1, 4, '2024-02-16 18:39:30', '2024-02-19 00:27:09', NULL);
INSERT INTO `options` VALUES (72, 2, 14, 1, 7, 0, 1, '2024-02-16 18:39:30', '2024-02-16 18:39:30', NULL);
INSERT INTO `options` VALUES (73, 2, 14, 1, 8, 0, 2, '2024-02-16 18:39:30', '2024-02-16 18:39:30', NULL);
INSERT INTO `options` VALUES (74, 2, 14, 1, 9, 0, 3, '2024-02-16 18:39:30', '2024-02-16 18:39:30', NULL);
INSERT INTO `options` VALUES (75, 2, 15, 1, 13, 0, 0, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (76, 2, 15, 1, 11, 0, 1, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (77, 2, 15, 1, 15, 0, 2, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (78, 2, 15, 1, 14, 0, 3, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (79, 2, 15, 1, 12, 0, 4, '2024-02-16 18:42:22', '2024-02-16 18:42:22', NULL);
INSERT INTO `options` VALUES (80, 2, 16, 1, 18, 0, 0, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);
INSERT INTO `options` VALUES (81, 2, 16, 1, 16, 0, 1, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);
INSERT INTO `options` VALUES (82, 2, 16, 1, 15, 0, 2, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);
INSERT INTO `options` VALUES (83, 2, 16, 1, 17, 0, 3, '2024-02-16 18:43:52', '2024-02-16 18:43:52', NULL);

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_charter` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `position` int NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `questions to users`(`user_id` ASC) USING BTREE,
  INDEX `questions to subjects`(`subject_id` ASC) USING BTREE,
  CONSTRAINT `questions to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `questions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, 1, 1, '<p>asdasda</p>', 0, 1, 2, '2024-02-14 21:13:59', '2024-02-14 21:13:59', '2024-02-14 15:48:24');
INSERT INTO `questions` VALUES (2, 1, 1, '<p>asda</p>', 0, 1, 1, '2024-02-14 21:48:54', '2024-02-14 21:48:54', '2024-02-14 15:48:22');
INSERT INTO `questions` VALUES (3, 1, 1, '<p><span style=\"color:hsl(0,0%,0%);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD1 - I spent an acceptable amount of time to complete my transaction (Responsiveness).</span></span></p>', 0, 1, 1, '2024-02-14 23:49:12', '2024-02-15 00:33:50', NULL);
INSERT INTO `questions` VALUES (4, 1, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD2 - The office accurately informed and followed the transaction\'s requirements and steps (Reliability)</span></span></p>', 0, 1, 2, '2024-02-15 00:34:12', '2024-02-15 00:34:12', NULL);
INSERT INTO `questions` VALUES (5, 1, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD3 - My transaction (including steps and payment) was simple and convenient (Access and Facilities)</span></span></p>', 0, 1, 3, '2024-02-15 00:35:33', '2024-02-15 00:35:33', NULL);
INSERT INTO `questions` VALUES (6, 2, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">S</span></span><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">DQ4 - I easily found information about my transaction from the office or its website (Communication)</span></span></p>', 0, 1, 4, '2024-02-15 00:37:57', '2024-02-15 05:33:46', NULL);
INSERT INTO `questions` VALUES (7, 2, 1, '<p><span style=\"background-color:rgb(248,249,250);color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD5 - I paid an acceptable amount of fees for my transaction (Costs)</span></span></p>', 0, 1, 5, '2024-02-15 00:38:26', '2024-02-16 18:44:56', NULL);
INSERT INTO `questions` VALUES (8, 1, 1, '<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD6 - I am confident my transaction was secure (Integrity)</span></span></p>', 0, 1, 6, '2024-02-15 00:39:06', '2024-02-15 00:39:06', NULL);
INSERT INTO `questions` VALUES (9, 1, 1, '<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD7 - The office\'s support was quick to respond (Assurance)</span></span></p>', 0, 1, 7, '2024-02-15 00:39:38', '2024-02-15 00:39:38', NULL);
INSERT INTO `questions` VALUES (10, 1, 1, '<p><span style=\"color:rgb(32,33,36);font-family:Roboto, Arial, sans-serif;font-size:14.6667px;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\">SQD8 - I got what I needed from the government office (Outcome)</span></span></p>', 0, 1, 8, '2024-02-15 00:40:04', '2024-02-15 00:40:04', '2024-02-16 10:32:44');
INSERT INTO `questions` VALUES (11, 2, 1, '<p>SQD8 - I got what I needed from the government office (Outcome)</p>', 0, 1, 8, '2024-02-16 18:34:17', '2024-02-16 18:34:17', '2024-02-16 10:35:43');
INSERT INTO `questions` VALUES (12, 2, 1, '<p>SQD8 - I got what I needed from the government office (Outcome)</p>', 0, 1, 8, '2024-02-16 18:36:07', '2024-02-16 18:36:07', '2024-02-16 10:37:07');
INSERT INTO `questions` VALUES (13, 2, 1, '<p>SQD8 - I got what I needed from the government office (Outcome)</p>', 0, 1, 8, '2024-02-16 18:37:26', '2024-02-16 18:38:14', NULL);
INSERT INTO `questions` VALUES (14, 1, 1, '<p>Which of the following best describes your awareness of a CC?</p>', 1, 1, 9, '2024-02-16 18:39:30', '2024-02-18 02:22:25', NULL);
INSERT INTO `questions` VALUES (15, 1, 1, '<p>I aware of CC (answered 1-3 in CC1), would you say that the CC of this office was …?</p>', 1, 1, 10, '2024-02-16 18:42:20', '2024-02-18 02:22:06', NULL);
INSERT INTO `questions` VALUES (16, 1, 1, '<p>If aware of CC (answered codes 1-3 in CC1), how much did the CC help you in your transaction?</p>', 1, 1, 11, '2024-02-16 18:43:49', '2024-02-18 02:21:54', NULL);

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013, 'CreateRememberMeTokens', '2024-02-09 02:36:32', '2024-02-09 02:36:32', 0);

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (12, '2024-02-11 04:49:09', '2024-02-11 17:10:44', 'Users', '1', '646d8cb303dc2a080669e2973db0232162087084', 'ab12a8d4a4785e1b1f69d9f29435c777252efd78', '2024-03-12 17:10:44');
INSERT INTO `remember_me_tokens` VALUES (13, '2024-02-13 03:03:43', '2024-02-13 03:03:43', 'Users', '1', '89f7ec7aa797f536be86e02d72e1bc897a459329', 'c4069ae8628e2d2dce395441e2e4fc8ad47fac59', '2024-03-14 03:03:43');
INSERT INTO `remember_me_tokens` VALUES (17, '2024-02-16 19:25:10', '2024-02-17 19:13:34', 'Users', '2', '4b0dd4e57084d240ad69a7c09c263c76acdb749c', '5200e634ef77e8f6e064aaf8fcbd802cebfae412', '2024-03-18 19:13:33');
INSERT INTO `remember_me_tokens` VALUES (19, '2024-02-19 00:06:41', '2024-02-19 00:06:41', 'Users', '1', '3574c85395e259465a13f11718b369692cd4076b', 'a6e7c3cc237bbe82b75037bf3f29eb9ad5419886', '2024-03-20 00:06:41');

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL DEFAULT 1,
  `service` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `services to offices`(`office_id` ASC) USING BTREE,
  INDEX `services to users`(`user_id` ASC) USING BTREE,
  INDEX `services to departments`(`department_id` ASC) USING BTREE,
  CONSTRAINT `services to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `services to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `services to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES (1, 1, 2, 23, 'Ictsss', 1, '2024-02-09 12:42:06', '2024-02-09 12:42:13', '2024-02-09 04:45:54');
INSERT INTO `services` VALUES (2, 1, 3, 10, 'Travel Authority', 1, '2024-02-09 15:56:34', '2024-02-13 06:06:51', '2024-02-16 08:03:38');
INSERT INTO `services` VALUES (3, 1, 3, 10, 'Other Requests/inquiries', 1, '2024-02-09 15:56:42', '2024-02-13 06:06:46', '2024-02-16 08:03:42');
INSERT INTO `services` VALUES (4, 1, 3, 10, 'Feedback/Complaint', 1, '2024-02-09 15:56:50', '2024-02-13 06:06:41', '2024-02-16 08:03:45');
INSERT INTO `services` VALUES (5, 1, 2, 9, 'BAC', 1, '2024-02-09 15:57:11', '2024-02-13 06:06:37', '2024-02-16 08:03:26');
INSERT INTO `services` VALUES (6, 1, 2, 9, 'Other Requests/inquiries', 1, '2024-02-09 15:57:15', '2024-02-13 06:06:32', '2024-02-16 08:03:30');
INSERT INTO `services` VALUES (7, 1, 2, 9, 'Feedback/Complaint', 1, '2024-02-09 15:57:24', '2024-02-13 06:06:27', '2024-02-16 08:03:34');
INSERT INTO `services` VALUES (8, 1, 4, 22, 'Create/delete/rename/reset User Accounts', 1, '2024-02-09 15:57:50', '2024-02-13 06:06:21', '2024-02-16 08:03:50');
INSERT INTO `services` VALUES (9, 1, 4, 22, 'Troubleshooting Of ICT Equipment', 1, '2024-02-09 15:57:57', '2024-02-13 06:06:16', '2024-02-16 08:03:52');
INSERT INTO `services` VALUES (10, 1, 4, 22, 'Uploading Of Publications', 1, '2024-02-09 15:58:04', '2024-02-13 06:06:08', '2024-02-16 08:03:54');
INSERT INTO `services` VALUES (11, 1, 4, 22, 'Other Requests/inquiries', 1, '2024-02-09 15:58:11', '2024-02-13 06:05:53', '2024-02-16 08:03:58');
INSERT INTO `services` VALUES (12, 2, 3, 10, 'Travel Authority', 1, '2024-02-16 16:04:23', '2024-02-16 16:04:23', NULL);
INSERT INTO `services` VALUES (13, 2, 3, 10, 'Other Requests/inquiries', 1, '2024-02-16 16:04:35', '2024-02-16 16:04:35', NULL);
INSERT INTO `services` VALUES (14, 2, 3, 10, 'Feedback/Complaint', 1, '2024-02-16 16:04:47', '2024-02-16 16:04:47', NULL);
INSERT INTO `services` VALUES (15, 2, 2, 9, 'BAC', 1, '2024-02-16 16:04:59', '2024-02-16 16:04:59', NULL);
INSERT INTO `services` VALUES (16, 2, 2, 9, 'Other Requests/inquiries', 1, '2024-02-16 16:05:07', '2024-02-16 16:05:07', NULL);
INSERT INTO `services` VALUES (17, 2, 2, 9, 'Feedback/Complaint', 1, '2024-02-16 16:05:14', '2024-02-16 16:05:14', NULL);
INSERT INTO `services` VALUES (18, 2, 5, 3, 'Cash Advance', 1, '2024-02-16 17:35:02', '2024-02-16 17:35:02', NULL);
INSERT INTO `services` VALUES (19, 2, 5, 3, 'General Services-related', 1, '2024-02-16 17:35:16', '2024-02-16 17:35:16', NULL);
INSERT INTO `services` VALUES (20, 2, 5, 3, 'Procurement-related', 1, '2024-02-16 17:35:26', '2024-02-16 17:35:26', NULL);
INSERT INTO `services` VALUES (21, 2, 5, 3, 'Other Requests/inquiries', 1, '2024-02-16 17:35:38', '2024-02-16 17:35:38', NULL);
INSERT INTO `services` VALUES (22, 2, 5, 7, 'Cash Advance', 1, '2024-02-16 17:35:52', '2024-02-16 17:35:52', '2024-02-16 09:36:19');
INSERT INTO `services` VALUES (23, 2, 5, 4, 'Application - Teaching Position', 1, '2024-02-16 17:58:32', '2024-02-16 17:58:32', NULL);
INSERT INTO `services` VALUES (24, 2, 5, 4, 'Application - Non-teaching/Teaching-related', 1, '2024-02-16 17:58:43', '2024-02-16 17:58:43', NULL);
INSERT INTO `services` VALUES (25, 2, 5, 4, 'Appointment (new, Promotion, Transfer, Etc.)', 1, '2024-02-16 18:09:16', '2024-02-16 18:09:16', NULL);
INSERT INTO `services` VALUES (26, 2, 5, 4, 'COE-Certificate Of Employment', 1, '2024-02-16 18:09:28', '2024-02-16 18:09:28', NULL);
INSERT INTO `services` VALUES (27, 2, 5, 4, 'Correction Of Name/Change Of Status', 1, '2024-02-16 18:09:37', '2024-02-16 18:09:37', NULL);
INSERT INTO `services` VALUES (28, 2, 5, 4, 'ERF-Equivalent Record Form', 1, '2024-02-16 18:09:47', '2024-02-16 18:09:47', NULL);
INSERT INTO `services` VALUES (29, 2, 5, 4, 'Leave Application', 1, '2024-02-16 18:09:58', '2024-02-16 18:09:58', NULL);
INSERT INTO `services` VALUES (30, 2, 5, 4, 'Loan Approval And Verification', 1, '2024-02-16 18:10:23', '2024-02-16 18:10:23', NULL);
INSERT INTO `services` VALUES (31, 2, 5, 4, 'Retirement', 1, '2024-02-16 18:10:43', '2024-02-16 18:10:43', NULL);
INSERT INTO `services` VALUES (32, 2, 5, 4, 'Service Record', 1, '2024-02-16 18:10:58', '2024-02-16 18:10:58', NULL);
INSERT INTO `services` VALUES (33, 2, 5, 4, 'Terminal Leave', 1, '2024-02-16 18:11:07', '2024-02-16 18:11:07', NULL);
INSERT INTO `services` VALUES (34, 2, 5, 4, 'Other Requests/inquiries', 1, '2024-02-16 18:11:17', '2024-02-16 18:11:17', NULL);
INSERT INTO `services` VALUES (35, 2, 5, 6, 'Inspection/Acceptance/Distribution Of LRs, Supplies, Equipment', 1, '2024-02-16 18:11:33', '2024-02-16 18:11:33', NULL);
INSERT INTO `services` VALUES (36, 2, 5, 6, 'Property And Equipment Clearance', 1, '2024-02-16 18:11:45', '2024-02-16 18:11:45', NULL);
INSERT INTO `services` VALUES (37, 2, 5, 6, 'Request/Issuance Of Supplies', 1, '2024-02-16 18:11:54', '2024-02-16 18:11:54', NULL);
INSERT INTO `services` VALUES (38, 2, 5, 6, 'Other Requests/inquiries', 1, '2024-02-16 18:12:06', '2024-02-16 18:12:06', NULL);
INSERT INTO `services` VALUES (39, 2, 5, 5, 'CAV-Certification, Authentication, Verification', 1, '2024-02-16 18:12:17', '2024-02-16 18:12:17', NULL);
INSERT INTO `services` VALUES (40, 2, 5, 5, 'Certified True Copy (CTC)/Photocopy Of Documents', 1, '2024-02-16 18:12:28', '2024-02-16 18:12:28', NULL);
INSERT INTO `services` VALUES (41, 2, 5, 5, 'Non-Certified True Copy Documents', 1, '2024-02-16 18:12:37', '2024-02-16 18:12:37', NULL);
INSERT INTO `services` VALUES (42, 2, 5, 5, 'Receiving & Releasing Of Documents', 1, '2024-02-16 18:12:48', '2024-02-16 18:12:48', NULL);
INSERT INTO `services` VALUES (43, 2, 5, 5, 'Other Requests/inquiries', 1, '2024-02-16 18:13:26', '2024-02-16 18:13:26', NULL);
INSERT INTO `services` VALUES (44, 2, 6, 11, 'ALS Enrollment', 1, '2024-02-16 18:16:13', '2024-02-16 18:16:13', NULL);
INSERT INTO `services` VALUES (45, 2, 6, 11, 'Access To LR Portal', 1, '2024-02-16 18:16:27', '2024-02-16 18:16:27', NULL);
INSERT INTO `services` VALUES (46, 2, 6, 11, 'Borrowing Of Books/Learning Materials', 1, '2024-02-16 18:16:37', '2024-02-16 18:16:37', NULL);
INSERT INTO `services` VALUES (47, 2, 6, 11, 'Contextualized Learning Resources', 1, '2024-02-16 18:16:46', '2024-02-16 18:16:46', NULL);
INSERT INTO `services` VALUES (48, 2, 6, 11, 'Quality Assurance Of Supplementary Learning Resources', 1, '2024-02-16 18:16:57', '2024-02-16 18:16:57', NULL);
INSERT INTO `services` VALUES (49, 2, 6, 11, 'Instructional Supervision', 1, '2024-02-16 18:17:06', '2024-02-16 18:17:06', NULL);
INSERT INTO `services` VALUES (50, 2, 6, 11, 'Technical Assistance', 1, '2024-02-16 18:17:15', '2024-02-16 18:17:15', NULL);
INSERT INTO `services` VALUES (51, 2, 6, 11, 'Other Requests/inquiries', 1, '2024-02-16 18:17:28', '2024-02-16 18:17:28', NULL);
INSERT INTO `services` VALUES (52, 2, 7, 14, 'Accounting-related', 1, '2024-02-16 18:17:54', '2024-02-16 18:17:54', NULL);
INSERT INTO `services` VALUES (53, 2, 7, 14, 'ORS-Obligation Request And Status', 1, '2024-02-16 18:18:03', '2024-02-16 18:18:03', NULL);
INSERT INTO `services` VALUES (54, 2, 7, 14, 'Posting/Updating Of Disbursement', 1, '2024-02-16 18:18:13', '2024-02-16 18:18:13', NULL);
INSERT INTO `services` VALUES (55, 2, 7, 14, 'Other Requests/inquiries', 1, '2024-02-16 18:18:26', '2024-02-16 18:18:26', NULL);
INSERT INTO `services` VALUES (56, 2, 4, 22, 'Create/delete/rename/reset User Accounts', 1, '2024-02-16 18:18:42', '2024-02-16 18:18:42', NULL);
INSERT INTO `services` VALUES (57, 2, 4, 22, 'Troubleshooting Of ICT Equipment', 1, '2024-02-16 18:18:49', '2024-02-16 18:18:49', NULL);
INSERT INTO `services` VALUES (58, 2, 4, 22, 'Uploading Of Publications', 1, '2024-02-16 18:18:55', '2024-02-16 18:18:55', NULL);
INSERT INTO `services` VALUES (59, 2, 4, 22, 'Other Requests/inquiries', 1, '2024-02-16 18:19:02', '2024-02-16 18:19:02', NULL);
INSERT INTO `services` VALUES (60, 2, 8, 24, 'Certificate Of No Pending Case', 1, '2024-02-16 18:19:15', '2024-02-16 18:19:15', NULL);
INSERT INTO `services` VALUES (61, 2, 8, 24, 'Correction Of Entries In School Record', 1, '2024-02-16 18:19:23', '2024-02-16 18:19:23', NULL);
INSERT INTO `services` VALUES (62, 2, 8, 24, 'Feedback/Complaints', 1, '2024-02-16 18:19:30', '2024-02-16 18:19:30', NULL);
INSERT INTO `services` VALUES (63, 2, 8, 24, 'Legal Advice/opinion', 1, '2024-02-16 18:19:37', '2024-02-16 18:19:37', NULL);
INSERT INTO `services` VALUES (64, 2, 8, 24, 'Sites Titling', 1, '2024-02-16 18:19:45', '2024-02-16 18:19:45', NULL);
INSERT INTO `services` VALUES (65, 2, 9, 16, 'Basic Education Data', 1, '2024-02-16 18:20:45', '2024-02-16 18:20:45', NULL);
INSERT INTO `services` VALUES (66, 2, 9, 16, 'EBEIS/LIS/NAT Data And Performance Indicators', 1, '2024-02-16 18:21:03', '2024-02-16 18:21:03', NULL);
INSERT INTO `services` VALUES (67, 2, 9, 16, 'Other Requests/inquiries', 1, '2024-02-16 18:21:20', '2024-02-16 18:21:20', NULL);
INSERT INTO `services` VALUES (68, 2, 9, 25, 'Additional SHS Track For Private Schools', 1, '2024-02-16 18:21:43', '2024-02-16 18:21:43', NULL);
INSERT INTO `services` VALUES (69, 2, 9, 25, 'Increase In Tuition/other School Fees (TOSF)', 1, '2024-02-16 18:21:53', '2024-02-16 18:21:53', NULL);
INSERT INTO `services` VALUES (70, 2, 9, 25, 'No Increase In Tuition/other School Fees', 1, '2024-02-16 18:22:03', '2024-02-16 18:22:03', NULL);
INSERT INTO `services` VALUES (71, 2, 9, 25, 'Private Schools Permit/recognition/renewal', 1, '2024-02-16 18:22:16', '2024-02-16 18:22:16', NULL);
INSERT INTO `services` VALUES (72, 2, 9, 25, 'Special Orders-graduation Of Private Schools Learners', 1, '2024-02-16 18:22:25', '2024-02-16 18:22:25', NULL);
INSERT INTO `services` VALUES (73, 2, 9, 25, 'Summer Permit For Private Schools', 1, '2024-02-16 18:22:34', '2024-02-16 18:22:34', NULL);
INSERT INTO `services` VALUES (74, 2, 9, 25, 'Other Private School Concerns', 1, '2024-02-16 18:22:43', '2024-02-16 18:22:43', NULL);

-- ----------------------------
-- Table structure for spans
-- ----------------------------
DROP TABLE IF EXISTS `spans`;
CREATE TABLE `spans`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `span` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `start_age` int NOT NULL,
  `end_age` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `spans to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `spans to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spans
-- ----------------------------
INSERT INTO `spans` VALUES (1, 1, '19 Or Lower', 1, 19, '2024-02-17 21:41:10', '2024-02-17 21:41:10', NULL);
INSERT INTO `spans` VALUES (2, 1, '20-34', 20, 34, '2024-02-17 21:42:23', '2024-02-17 21:42:23', NULL);
INSERT INTO `spans` VALUES (3, 1, '35-49', 35, 49, '2024-02-17 21:43:08', '2024-02-17 21:43:08', NULL);
INSERT INTO `spans` VALUES (4, 1, '50-64', 50, 64, '2024-02-17 21:43:23', '2024-02-17 21:43:23', NULL);
INSERT INTO `spans` VALUES (5, 1, '65 Or Higher', 65, 999, '2024-02-17 21:44:41', '2024-02-17 21:44:41', NULL);

-- ----------------------------
-- Table structure for subjects
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `subjects to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `subjects to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of subjects
-- ----------------------------
INSERT INTO `subjects` VALUES (1, 1, 'SDO-Santiago City Client Satisfaction Measurement (CSM)', 'CSM', '<p><span style=\"color:hsl(0,0%,100%);font-family:Arial, Helvetica, sans-serif;\"><span style=\"-webkit-text-stroke-width:0px;display:inline !important;float:none;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:pre-wrap;widows:2;word-spacing:0px;\"><strong>The Client Satisfaction (CSM) tracks the customer experience of government offices. Your feedback on your recently concluded transaction will help this office provide better service. Personal information shared will be kept confidential and you always have the option to not answer this form.</strong></span></span></p>', 1, '2024-02-14 17:36:23', '2024-02-18 07:55:31', NULL);

-- ----------------------------
-- Table structure for surveys
-- ----------------------------
DROP TABLE IF EXISTS `surveys`;
CREATE TABLE `surveys`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `ca_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `visitor_id` bigint UNSIGNED NOT NULL,
  `subject_id` bigint UNSIGNED NOT NULL,
  `customer_type_id` bigint UNSIGNED NOT NULL,
  `office_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NULL DEFAULT NULL,
  `service_id` bigint UNSIGNED NOT NULL,
  `score` double NOT NULL,
  `purpose` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `surveys to customer_types`(`customer_type_id` ASC) USING BTREE,
  INDEX `surveys to offices`(`office_id` ASC) USING BTREE,
  INDEX `surveys to services`(`service_id` ASC) USING BTREE,
  INDEX `surveys to subjects`(`subject_id` ASC) USING BTREE,
  INDEX `surveys to departments`(`department_id` ASC) USING BTREE,
  INDEX `surveys to visitors`(`visitor_id` ASC) USING BTREE,
  CONSTRAINT `surveys to customer_types` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to services` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `surveys to visitors` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of surveys
-- ----------------------------
INSERT INTO `surveys` VALUES (1, '2024-02-00001', '20240217-0002', 2, 1, 10, 8, 24, 60, 42, 'Sdsd', '<p> service</p>', '2024-02-18 02:56:14', '2024-02-18 02:56:14', NULL);
INSERT INTO `surveys` VALUES (2, '2024-02-00002', '20240217-0003', 3, 1, 10, 3, 10, 13, 35, 'Dsdsds', '<p>good service</p>', '2024-02-18 03:49:24', '2024-02-18 03:49:24', NULL);
INSERT INTO `surveys` VALUES (3, '2024-02-00003', '20240218-4', 4, 1, 10, 3, 10, 12, 42, 'Asdasd', '<p>-</p>', '2024-02-18 23:30:19', '2024-02-18 23:30:19', NULL);
INSERT INTO `surveys` VALUES (4, '2024-02-00004', '20240218-4', 4, 1, 9, 5, 5, 19, 42, 'Sss', '<p>-</p>', '2024-02-18 23:31:32', '2024-02-18 23:31:32', NULL);
INSERT INTO `surveys` VALUES (5, '2024-02-00005', '20240217-0003', 3, 1, 9, 8, 24, 62, 42, 'Ssss', '<p>-</p>', '2024-02-18 23:35:46', '2024-02-18 23:35:46', NULL);
INSERT INTO `surveys` VALUES (6, '2024-02-00006', '20240218-4', 4, 1, 9, 8, 24, 63, 42, 'Sdsdsds', '<p>-</p>', '2024-02-18 23:38:41', '2024-02-18 23:38:41', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Rodrigo', 'cabotaje.rodrigo@gmail.com', '$2y$10$MexEmm6MdpBsKuCBxm67Hu.rsq3HCQGI.A/tvUu51VXFann.XZTqW', '65d1ba91bb08065d1ba91bb084', 1, '2024-02-09 10:10:05', '2024-02-19 00:06:41', NULL);
INSERT INTO `users` VALUES (2, 'Marc Jefferson Calaunan', 'joppycalaunan@gmail.com', '$2y$10$UZT34/crFUs1Ych/95vyeuR6J04MzGXwo6kdyK68hnuQU3DgHbvkC', '65d0245e8dfd665d0245e8dfd9', 1, '2024-02-15 05:20:51', '2024-02-17 19:13:34', NULL);

-- ----------------------------
-- Table structure for visitors
-- ----------------------------
DROP TABLE IF EXISTS `visitors`;
CREATE TABLE `visitors`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `age` int NOT NULL,
  `gender_id` bigint UNSIGNED NOT NULL,
  `span_id` bigint UNSIGNED NOT NULL,
  `agency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_answered` tinyint UNSIGNED NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `visitors to genders`(`gender_id` ASC) USING BTREE,
  INDEX `visitors to spans`(`span_id` ASC) USING BTREE,
  CONSTRAINT `visitors to genders` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `visitors to spans` FOREIGN KEY (`span_id`) REFERENCES `spans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of visitors
-- ----------------------------
INSERT INTO `visitors` VALUES (1, 'RODRIGO', 'cabotaje.rodrigo@gmail.com', 20, 1, 2, '111', '20240217-0001', 1, '2024-02-17 23:51:49', '2024-02-18 00:43:35', NULL);
INSERT INTO `visitors` VALUES (2, 'ASDASD ASDASD ASDASDASD', 'cabotaje.rodrigo@gmail.com', 20, 2, 2, 'Asdasdas', '20240217-0002', 1, '2024-02-18 02:55:48', '2024-02-18 02:56:14', NULL);
INSERT INTO `visitors` VALUES (3, 'ASDASD ASDASD ASDASDASD', 'cabotaje.rodrigo@gmail.com', 20, 2, 2, 'S', '20240217-0003', 1, '2024-02-18 03:48:44', '2024-02-18 23:35:46', NULL);
INSERT INTO `visitors` VALUES (4, 'JOANA DOE', 'joana@gmail.com', 20, 2, 2, 'Css', '20240218-4', 1, '2024-02-18 23:29:49', '2024-02-18 23:38:41', NULL);

SET FOREIGN_KEY_CHECKS = 1;
