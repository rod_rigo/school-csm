'use strict';
$(document).ready(function (e) {

    setInterval(function () {
        var time = moment().format('hh:mm:ss A');
        $('#time').text(time);
    }, 1000);

});