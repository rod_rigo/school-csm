Turbolinks.start();
$('a[link]').click(function (e) {
    var href = $(this).attr('href');
    Turbolinks.visit(href,{action:'replace'});
});
$('input[type="number"]').keypress(function (e) {
    var key = e.key;
    var regex = /^([0-9]){1,}$/;
    if(!key.match(regex)){
        e.preventDefault();
    }
});

function exports(filename, mime) {
    var bool = false;
    $.ajax({
        url: mainurl+'exports/add',
        type: 'POST',
        method: 'POST',
        data: JSON.stringify({'user_id': parseInt(JSON.parse(auth).id),'filename':filename, 'mime': mime}),
        contentType: 'application/json',
        dataType: 'JSON',
        global:false,
        async:false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (e) {

        },
    }).done(function (data, status, xhr) {
        bool = true;
    }).fail(function (data, status, xhr) {
        var errors = data.responseJSON.errors;
        var message = data.responseJSON;
    });
    return bool;
}