'use strict';
$(document).ready(function () {

    const baseurl = mainurl+'surveys/';
    var url = '';
    var excel = '';

    var title = function (e) {
        var customerType = $('#customer-type').val();
        var services = $('#services').val();
        var year = $('#year').val();
        var month = $('#month').val();
        return 'Services_'+customerType+'_'+services+'_'+year+'_'+month;
    };

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getSurveyServices',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        buttons: [
            {
                extend: 'print',
                title: 'Print',
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: '',
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed && exports(title(), 'excel')) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: '',
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed && exports(title(), 'pdf')) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.tableBodyEven.fontSize = 7;
                    doc.styles.tableBodyOdd.fontSize = 7;
                    doc.styles.tableFooter.fontSize = 7;
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [10],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
        ],
        columns: [
            { data: 'id'},
            { data: 'ca_no'},
            { data: 'visitor.name'},
            { data: 'visitor.email'},
            { data: 'visitor.age'},
            { data: 'visitor.gender.gender'},
            { data: 'visitor.agency'},
            { data: 'purpose'},
            { data: 'customer_type.customer_type'},
            { data: 'service.service'},
            { data: 'modified'},
        ]
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var offices = $('#offices').val();
        var departments = $('#departments').val();
        var services = $('#services').val();
        var year = $('#year').val();
        var month = $('#month').val();
        var url = 'getSurveyServices/'+(services)+'?year='+(year)+'&month='+(month);
        $('input.form-control, button, select.form-control').prop('disabled', true);
        table.ajax.url(baseurl + url).load(function () {
            window.open(baseurl + 'servicesReport/' + (services)+'?year='+(year)+'&month='+(month));
            $('input.form-control, button, select.form-control').prop('disabled', false);
        }, false);
    });

    $('#customer-type').change(function (e) {
        var value = $(this).val();
        if(value){
            getServicesList(value);
        }
    });

    function getServicesList(customerTypeId) {
        $.ajax({
            url: mainurl+'services/getServicesList/'+(customerTypeId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#services').empty().prop('disabled', true).append('<option value="">Please Wait</option>');
            },
        }).done(function (data, status, xhr) {
            if((Object.keys(data)).length){
                $('#services').empty().append('<option value="">Please Select Service</option>');
                $.map(data, function (data, key) {
                    $('#services').append('<option value="'+(key)+'">'+(data)+'</option>');
                });
            }

            $('#services').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
        });
    }

});