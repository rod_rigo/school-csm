'use strict';
$(document).ready(function (e) {

    $('input[type="radio"]').change(function (e) {
        var checked = $(this).prop('checked');
        $(this).prop('checked', !checked);
    });

});