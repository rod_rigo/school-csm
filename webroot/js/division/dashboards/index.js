'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    $('#form').submit(function (e) {
        e.preventDefault();
        survey_chart.destroy();
        getSurveysChart();
        customer_types_chart.destroy();
        getSurveyCustomerTypesChart();
        genders_chart.destroy();
        getSurveyGendersChart();
        spans_chart.destroy();
        getSurveySpansChart();
        services_chart.destroy();
        getSurveyServicesChart();
    });

    var survey_canvas = document.querySelector('#surveys-chart').getContext('2d');
    var survey_chart;

    function getSurveys() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getSurveys?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getSurveysChart() {
        survey_chart =  new Chart(survey_canvas, {
            type: 'bar',
            data: {
                labels: getSurveys().label,
                datasets: [
                    {
                        label:'Total',
                        data: getSurveys().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: true,
                        text: 'Survey By Month',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var customer_types_canvas = document.querySelector('#customer-types-chart').getContext('2d');
    var customer_types_chart;

    function getSurveyCustomerTypes() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getSurveyCustomerTypes?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.customer_type).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getSurveyCustomerTypesChart() {
        customer_types_chart =  new Chart(customer_types_canvas, {
            type: 'polarArea',
            data: {
                labels: getSurveyCustomerTypes().label,
                datasets: [
                    {
                        label:'Total',
                        data: getSurveyCustomerTypes().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 2
                    },
                    title: {
                        display: true,
                        text: 'Customer Type',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var genders_canvas = document.querySelector('#genders-chart').getContext('2d');
    var genders_chart;

    function getSurveyGenders() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getSurveyGenders?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.gender).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getSurveyGendersChart() {
        genders_chart =  new Chart(genders_canvas, {
            type: 'bar',
            data: {
                labels: getSurveyGenders().label,
                datasets: [
                    {
                        label:'Total',
                        data: getSurveyGenders().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 3
                    },
                    title: {
                        display: true,
                        text: 'Gender',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var spans_canvas = document.querySelector('#spans-chart').getContext('2d');
    var spans_chart;

    function getSurveySpans() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getSurveySpans?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.span).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getSurveySpansChart() {
        spans_chart =  new Chart(spans_canvas, {
            type: 'pie',
            data: {
                labels: getSurveySpans().label,
                datasets: [
                    {
                        label:'Total',
                        data: getSurveySpans().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 4
                    },
                    title: {
                        display: true,
                        text: 'Age Span',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var services_canvas = document.querySelector('#services-chart').getContext('2d');
    var services_chart;

    function getSurveyServices() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getSurveyServices?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.service).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getSurveyServicesChart() {
        services_chart =  new Chart(services_canvas, {
            type: 'doughnut',
            data: {
                labels: getSurveyServices().label,
                datasets: [
                    {
                        label:'Total',
                        data: getSurveyServices().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Service',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    function getSurveysCount() {
        $.ajax({
            url: baseurl + 'getSurveysCount',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#total-surveys-today, #total-surveys-week, #total-surveys-month, #total-surveys-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-surveys-today').text(data.today);
            $('#total-surveys-week').text(data.week);
            $('#total-surveys-month').text(data.month);
            $('#total-surveys-year').text(data.year);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    getSurveysChart();
    getSurveyCustomerTypesChart();
    getSurveyGendersChart();
    getSurveySpansChart();
    getSurveyServicesChart();
    getSurveysCount();

});
$(document).ready(function (e) {
    $('.image').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        Swal.fire({
            title: 'Export To Image',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            input: 'text',
            inputValidator: function (value) {
                if (!value) {
                    return 'Please Enter A Filename';
                }
            },
            inputAttributes: {
                placeholder: 'Enter A Filename',
                id: 'filename'
            }
        }).then(function (result) {
            if (result.isConfirmed) {
                document.querySelector(dataTarget).toBlob(function(blob) {
                    saveAs(blob, ($('#filename').val())+'.png');
                });
            }
        });
    });

    $('.print').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        var chart =  document.querySelector(dataTarget);
        Swal.fire({
            title: 'Print Canvas',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                printJS({
                    printable: chart.toDataURL('image/png',1.0),
                    type: 'image',
                    imageStyle: 'width:100%;height:90vh;margin-bottom:20px;',
                    style: '@page { size: Letter landscape; }'
                });
            }
        });
    });

    $('.pdf').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        var canvas = document.querySelector(dataTarget);
        var chart = Chart.getChart(canvas);
        Swal.fire({
            title: 'Export To PDF',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                var  row = [
                    [
                        {text:'Title'},
                        {text:'Total'}
                    ]
                ];

                $.map(chart.config.data.labels,function (value, key) {
                    row.push([
                        { text : chart.config.data.labels[(key)], colspan : 1 },
                        { text : chart.config.data.datasets[0].data[(key)], colspan : 1 }
                    ]);
                });

                var docDefinition = {
                    pageOrientation: 'landscape',
                    pageMargins: [ 10, 10, 10, 10 ],
                    content: [
                        {
                            text: null,
                            fontSize: 15,
                            width:100
                        },
                        {
                            columns:[
                                {
                                    image: canvas.toDataURL('image/png',1.0),
                                    width:500,
                                    height: 500
                                },
                                {
                                    width: 297.6377952755905,
                                    margin:[10,50],
                                    table: {
                                        headerRows: 2,
                                        widths: [ 100, 100 ],
                                        heights: [
                                            20
                                        ],
                                        body: row
                                    }
                                }
                            ]
                        }
                    ]
                };

                pdfMake.createPdf(docDefinition).open();
            }
        });
    });

    $('.excel').click(function (e) {
        var dataTarget = $(this).attr('data-target');
        var canvas = document.querySelector(dataTarget);
        var chart = Chart.getChart(canvas);
        Swal.fire({
            title: 'Export To Excel',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            input: 'text',
            inputValidator: function (value) {
                if (!value) {
                    return 'Please Enter A Filename';
                }
            },
            inputAttributes: {
                placeholder: 'Enter A Filename',
                id: 'filename'
            }
        }).then(function (result) {
            if (result.isConfirmed) {
                const workbook = new ExcelJS.Workbook();

                workbook.creator = '';
                workbook.lastModifiedBy = '';
                workbook.created = new Date();
                workbook.modified = new Date();
                workbook.lastPrinted = new Date();

                const worksheet = workbook.addWorksheet('New Sheet');
                worksheet.columns = [
                    { header: 'Title', key: 'value' },
                    { header: 'Total', key: 'data' }
                ];

                // add image to workbook by base64
                const image = canvas.toDataURL('image/png',1.0);
                const imageId2 = workbook.addImage({
                    base64: image,
                    extension: 'png'
                });
                worksheet.addImage(imageId2, {
                    tl: { col: 3, row: 0 },
                    ext: { width: 1000, height: 1000 }
                });

                var rows = [];

                for(var i = 0; i < chart.config.data.labels.length; i++){
                    rows.push({
                        value : chart.config.data.labels[i],
                        data : chart.config.data.datasets[0].data[i]
                    });
                }

                worksheet.addRows(rows);

                workbook.xlsx.writeBuffer().then(function (data) {
                    const blob = new Blob([data], {
                        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
                    });
                    saveAs(blob, ($('#filename').val())+'.xlsx');
                });
            }
        });
    });
});