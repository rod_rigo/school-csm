'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'visitors/add';
    var attemps = 0;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            swal('success', data.transaction_code, data.message);
            attemps++;
            setTimeout(function () {
                if(attemps == 3){
                    window.location.reload();
                }
            }, 1000);
            getVisitors();
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            const questions = data.responseJSON.questions;
            const feedbacks = data.responseJSON.feedbacks;
            const sector = data.responseJSON.sector;
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);

                });
            });

            $.map(sector, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="sector['+(name)+']"]').addClass('is-invalid');
                    $('[name="sector['+(name)+']"]').next('small').text(value);

                });
            });

            $.map(questions, function (value, key) {

                var name = Object.keys(value)[0];
                $.map(value, function (value, key) {
                    var message = value;
                    var input = $('input[class*="answers-'+(name.replace(/_/g, '-'))+'"]');
                    input.each(function (e) {
                        var value = $(this).val();
                        if(!value){
                            $(this).next('small').text(Object.values(message)[0]);
                        }
                    });
                });

            });

            $.map(feedbacks, function (value, key) {

                var name = Object.keys(value)[0];
                $.map(value, function (value, key) {
                    var message = value;
                    var input = $('input[class*="feedbacks-'+(name.replace(/_/g, '-'))+'"]');
                    input.each(function (e) {
                        var value = $(this).val();
                        if(!value){
                            $(this).next('small').text(Object.values(message)[0]);
                        }
                    });
                });

            });

        });
    });

    $('input[type="number"]').keypress(function (e) {
        var key = e.key;
        var pattern = /^([0-9]){1,}$/;
        if(!key.match(pattern)){
            e.preventDefault();
        }
    });

    $('#name').on('input', function (e) {
        setTimeout(function (e) {
            getNames();
        }, 1500);
        $('#name').autocomplete('search', $(this).val());
    });

    function getVisitors() {
        $.ajax({
            url: mainurl+'visitors/getVisitors',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#pending-transactions').html('<h6>Pending Transactions</h6>');
            },
        }).done(function (data, status, xhr) {
            if((Object.keys(data)).length){
                $('#pending-transactions').fadeIn(100);
                $.map(data, function (data, key) {
                    $('#pending-transactions').append('<div class="card rounded-0 border-bottom border-primary border-top-0 border-left-0 border-right-0 hover-shadow"> ' +
                        '<div class="card-body"> ' +
                        '<a href="javascript:void(0);"> ' +
                        '<h6 class="card-title mb-0"><small>'+(data.transaction_code)+'</small></h6> </a>' +
                        ' <p class="card-text">'+(data.name)+'</p> ' +
                        '</div> </div>');
                });
            }else{
                $('#pending-transactions').fadeOut(100);
            }
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getNames() {
        $.ajax({
            url:mainurl+'visitors/getNames?name='+($('#name').val()),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend:function (e) {

            },
        }).done(function (data, status, xhr) {
            $('#name').autocomplete({
                delay: 100,
                minLength: 2,
                autoFocus: true,
                source: function (request, response) {
                    response($.map(data, function (value, key) {
                        var name = value.name.toUpperCase();
                        if (name.indexOf(request.term.toUpperCase()) !== -1) {
                            return {
                                label: value.name,
                                age: value.age,
                                email: value.email,
                                gender_id: value.gender_id,
                                id: value.id,
                            }
                        } else {
                            return null;
                        }
                    }));
                },
                create: function (event, ui) {
                    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                        var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                        return $('<li></li>')
                            .data('item.autocomplete', item)
                            .append('<li>'+(label)+'</li>')
                            .appendTo(ul);
                    };
                },
                focus: function(e, ui) {
                    e.preventDefault();
                },
                select: function(e, ui) {
                    $('#name').val(ui.item.label);
                    $('#age').val(ui.item.age);
                    $('#email').val(ui.item.email);
                    $('#gender-id').val(ui.item.gender_id);
                },
                change: function(e, ui ) {
                    e.preventDefault();
                },
            });

            $('#name').autocomplete('search',$('#name').val());
        }).fail(function (data, status, xhr) {

        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 60000,
            timerProgressBar:true,
        })
    }

    getVisitors();

    setTimeout(function () {
        getVisitors();
    }, (1000 * 60 * 15));

});