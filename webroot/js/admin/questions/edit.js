'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;
    var editor;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    CKEDITOR.ClassicEditor.create(document.getElementById('question'), {
        // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
        toolbar: {
            items: [
                'exportPDF','exportWord', '|',
                'findAndReplace', 'selectAll', '|',
                'heading', '|',
                'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                'bulletedList', 'numberedList', 'todoList', '|',
                'outdent', 'indent', '|',
                'undo', 'redo',
                '-',
                'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                'alignment', '|',
                'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                'specialCharacters', 'horizontalLine', 'pageBreak',
            ],
            shouldNotGroupWhenFull: true
        },
        // Changing the language of the interface requires loading the language file using the <script> tag.
        // language: 'es',
        list: {
            properties: {
                styles: true,
                startIndex: true,
                reversed: true
            }
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
            ]
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
        placeholder: 'Enter A Question Here...',
        // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
        fontFamily: {
            options: [
                'default',
                'Arial, Helvetica, sans-serif',
                'Courier New, Courier, monospace',
                'Georgia, serif',
                'Lucida Sans Unicode, Lucida Grande, sans-serif',
                'Tahoma, Geneva, sans-serif',
                'Times New Roman, Times, serif',
                'Trebuchet MS, Helvetica, sans-serif',
                'Verdana, Geneva, sans-serif'
            ],
            supportAllValues: true
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
        fontSize: {
            options: [ 10, 12, 14, 'default', 18, 20, 22 ],
            supportAllValues: true
        },
        // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
        // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
        htmlSupport: {
            allow: [
                {
                    name: /.*/,
                    attributes: true,
                    classes: true,
                    styles: true
                }
            ]
        },
        // Be careful with enabling previews
        // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
        htmlEmbed: {
            showPreviews: true
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
        link: {
            decorators: {
                addTargetToExternalLinks: true,
                defaultProtocol: 'https://',
                toggleDownloadable: {
                    mode: 'manual',
                    label: 'Downloadable',
                    attributes: {
                        download: 'file'
                    }
                }
            }
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
        mention: {
            feeds: [
                {
                    marker: '@',
                    feed: [
                        '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                        '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                        '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                        '@sugar', '@sweet', '@topping', '@wafer'
                    ],
                    minimumCharacters: 1
                }
            ]
        },
        // The "super-build" contains more premium features that require additional configuration, disable them below.
        // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
        removePlugins: [
            // These two are commercial, but you can try them out without registering to a trial.
            // 'ExportPdf',
            // 'ExportWord',
            'CKBox',
            'CKFinder',
            'EasyImage',
            // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
            // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
            // Storing images as Base64 is usually a very bad idea.
            // Replace it on production website with other solutions:
            // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
            // 'Base64UploadAdapter',
            'RealTimeCollaborativeComments',
            'RealTimeCollaborativeTrackChanges',
            'RealTimeCollaborativeRevisionHistory',
            'PresenceList',
            'Comments',
            'TrackChanges',
            'TrackChangesData',
            'RevisionHistory',
            'Pagination',
            'WProofreader',
            // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
            // from a local file system (file://) - load this site via HTTP server if you enable MathType
            'MathType'
        ]
    }).then( function (data) {
        editor = data;
    }).catch(function (error) {
        window.location.reload();
    });

    setInterval(function(){
        editor.updateSourceElement();
    }, 1000);

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});
$(document).ready(function (e) {
    const baseurl = mainurl+'options/';
    var url = '';
    var triggerCharter = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getOptionsList/'+(id ),
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(3)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1+'<input type="hidden" class="positions" value="'+(parseInt(row+1))+'"  data-id="'+(parseInt(full.id))+'" readonly required/>';
                }
            },
            {
                targets: [2],
                data: null,
                render: function(data,type,row,meta){
                    return triggerCharter[parseInt(row.trigger_charter)];
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [5],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'choice.choice'},
            { data: 'trigger_charter'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.edit',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'edit/'+(parseInt(dataId));
        $.ajax({
            url:href,
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                url = 'edit/'+(parseInt(dataId));
                $('button[type="reset"]').fadeOut(100);
            },
        }).done(function (data, status, xhr) {
            $('#choice-id').val(data.choice_id);
            $('#trigger').prop('checked', data.trigger_charter);
            $('#trigger-charter').val(data.trigger_charter);
            $('#points').val(data.points);
            $('#modal').modal('toggle');
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });

    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#choice-form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl + url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('#modal').modal('toggle');
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#toggle-modal').click(function (e) {
        url = 'add';
        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#choice-form')[0].reset();
        $('small').empty();
        $('.form-control').removeClass('is-invalid');
        $('button[type="reset"]').fadeIn(100);
        $('button[type="reset"], button[type="submit"]').prop('disabled', false);
    });

    $(document).find('#datatable tbody').sortable({
        revert: true,
        handle: 'td:nth-child(1)',
        start:function( event, ui ){

        },
        update: function( event, ui ) {
            console.log(table.data().toArray());
            positions();
        },
    });

    function positions() {
        var data = [];
        var counter = 0;
        $('.positions').each(function () {
            var dataId = $(this).attr('data-id');
            counter++;
            data.push({
                id : parseInt(dataId),
                position : parseInt(counter)
            });
        });
        $.ajax({
            url: baseurl+'positions',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json', // Set content type to JSON
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            table.ajax.reload(null, false);
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });
    }

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

    $('#charter').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-charter').val(Number(checked));
    });

    $('#trigger').change(function (e) {
        var checked = $(this).prop('checked');
        $('#trigger-charter').val(Number(checked));
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});