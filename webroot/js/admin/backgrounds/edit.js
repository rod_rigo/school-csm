'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('input[type="file"]').change(function (e) {
        var file = e.target.files[0];
        var mimes = ['image/png', 'image/jpeg', 'image/jpg', 'image/jfif', 'image/webp'];
        var dataTarget = $(this).attr('data-target');
        var reader = new FileReader();

        if(!mimes.includes(file.type)){
            $(this).addClass('is-invalid').parent().parent().next('small').text('Invalid Image Format');
            return true;
        }

        reader.addEventListener('load', function (e) {
            $(dataTarget).attr('src', e.target.result);
        });

        $(this).removeClass('is-invalid').parent().parent().next('small').empty();
        $(this).next('label.custom-file-label').text(file.name);
        reader.readAsDataURL(file);
    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});