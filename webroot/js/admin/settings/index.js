'use strict';
$(document).ready(function (e) {
    const baseurl = mainurl+'databases/';
    var url = '';

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getDatabases',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        buttons: [
            {
                text: 'Download',
                action: function () {
                    Swal.fire({
                        title:'Download',
                        text:'Are You Sure?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            download();
                        }
                    })
                },
                attr:{
                    class:'btn btn-info rounded-0',
                    id:'download'
                },
            }
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [4],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white download" title="Download">Download</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'filename'},
            { data: 'user.name'},
            { data: 'created'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.download',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'download/'+(dataId);
        Swal.fire({
            title: 'Download Database',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    function download() {
        $.ajax({
            url: baseurl+'add',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify({'filename':(new Date()).getMilliseconds()}),
            contentType: 'application/json', // Set content type to JSON
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
            },
        }).done(function (data, status, xhr) {
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }
});
$(document).ready(function (e) {
    const baseurl = mainurl+'activities/';
    var url = '';

    var datatable = $('#activities-datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getActivities',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        buttons: [
            {
                text: 'Truncate',
                action: function () {
                    Swal.fire({
                        title:'Truncate',
                        text:'Are You Sure?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            Swal.fire({
                                icon:'info',
                                title:'Please Enter You Password',
                                input:'password',
                                inputLabel:'Password',
                                inputPlaceholder:'Enter Your Password',
                                allowOutsideClick:false,
                                showCancelButton:true,
                                confirmButtonText:'Submit',
                                inputAttributes: {
                                    autocapitalize:'off',
                                    autocorrect:'off',
                                    id:'password',
                                    title:'Please Fill Out This Field',
                                },
                                inputValidator: function (value) {
                                    if (!value) {
                                        return 'Please Fill Out This Field';
                                    }else{
                                        truncate();
                                    }
                                }
                            });
                        }
                    })
                },
                attr:{
                    class:'btn btn-warning rounded-0',
                    id:'truncate'
                },
            }
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    var object = JSON.parse(row.original);
                    var ul = '<ul>';
                    $.map(object, function (value, key) {
                        ul+='<li>'+(key)+' - '+(value)+'</li>';
                    });
                    ul += '</ul>';
                    return ul;
                }
            },
            {
                targets: [4],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    var object = JSON.parse(row.data);
                    var ul = '<ul>';
                    $.map(object, function (value, key) {
                        ul+='<li>'+(key)+' - '+(value)+'</li>';
                    });
                    ul += '</ul>';
                    return ul;
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
        ],
        columns: [
            { data: 'id'},
            { data: 'model'},
            { data: 'action'},
            { data: 'original'},
            { data: 'data'},
            { data: 'user'},
            { data: 'created'},
        ]
    });

    function truncate() {
        $.ajax({
            url: baseurl+'truncate',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify({'password':($('#password').val())}),
            contentType: 'application/json', // Set content type to JSON
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
            },
        }).done(function (data, status, xhr) {
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }
});
$(document).ready(function (e) {
    const baseurl = mainurl+'exports/';
    var url = '';

    var datatable = $('#exports-datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getExports',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        buttons: [
            {
                text: 'Truncate',
                action: function () {
                    Swal.fire({
                        title:'Truncate',
                        text:'Are You Sure?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            Swal.fire({
                                icon:'info',
                                title:'Please Enter You Password',
                                input:'password',
                                inputLabel:'Password',
                                inputPlaceholder:'Enter Your Password',
                                allowOutsideClick:false,
                                showCancelButton:true,
                                confirmButtonText:'Submit',
                                inputAttributes: {
                                    autocapitalize:'off',
                                    autocorrect:'off',
                                    id:'password',
                                    title:'Please Fill Out This Field',
                                },
                                inputValidator: function (value) {
                                    if (!value) {
                                        return 'Please Fill Out This Field';
                                    }else{
                                        truncate();
                                    }
                                }
                            });
                        }
                    })
                },
                attr:{
                    class:'btn btn-warning rounded-0',
                    id:'truncate'
                },
            }
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
        ],
        columns: [
            { data: 'id'},
            { data: 'filename'},
            { data: 'mime'},
            { data: 'user.name'},
            { data: 'created'},
        ]
    });

    function truncate() {
        $.ajax({
            url: baseurl+'truncate',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify({'password':($('#password').val())}),
            contentType: 'application/json', // Set content type to JSON
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
            },
        }).done(function (data, status, xhr) {
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }
});