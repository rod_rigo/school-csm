'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'spans/';
    var url = '';

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getSpans',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {

        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [2],
                data: null,
                render: function(data,type,row,meta){
                    return row.start_age+' - '+row.end_age;
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [5],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'span'},
            { data: 'start_age'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.edit',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'edit/'+(parseInt(dataId));
        $.ajax({
            url:href,
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                url = 'edit/'+(parseInt(dataId));
                $('button[type="reset"]').fadeOut(100);
            },
        }).done(function (data, status, xhr) {
            $('#gender').val(data.gender);
            $('#span').val(data.span);
            $('#start-age').val(data.start_age);
            $('#end-age').val(data.end_age);
            $('#modal').modal('toggle');
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });

    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl + url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('#modal').modal('toggle');
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#toggle-modal').click(function (e) {
        url = 'add';
        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#form')[0].reset();
        $('small').empty();
        $('.form-control').removeClass('is-invalid');
        $('button[type="reset"]').fadeIn(100);
        $('button[type="reset"], button[type="submit"]').prop('disabled', false);
    });

    $('#gender, #start-age, #end-age').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('input[type="number"]').keypress(function (e) {
        var key = e.key;
        var pattern = /^([0-9]){1,}$/;
        if(!key.match(pattern)){
            e.preventDefault();
        }
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});