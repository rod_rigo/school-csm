'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;
    var editor;
    var attemps = 0;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            editor.setData('-');
            window.open(data.redirect);
            swal('success', null, data.message);
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            setTimeout(function () {
                if(attemps == 3){
                    window.location.reload();
                }
                window.scrollTo({ top: 0, behavior: 'smooth' });
                window.location.replace(mainurl+'visitors');
            }, 1000);
            getVisitors();
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            const questions = data.responseJSON.questions;
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);

                });
            });

            $.map(questions, function (value, key) {

                var name = Object.keys(value)[0];
                $.map(value, function (value, key) {
                    var message = value;
                    var input = $('input[class*="answers-'+(name.replace(/_/g, '-'))+'"]');
                    input.each(function (e) {
                        var value = $(this).val();
                        if(!value){
                            $(this).next('small').text(Object.values(message)[0]);
                        }
                    });
                });

            });

        });
    });

    $('.sex').change(function (e) {
        var value = $(this).val();
        $(this).prop('checked', true);
        $('.sex:checked').not(this).prop('checked', false);
        $('#sex').val(value);
    });

    $('.choices').change(function (e) {
        var dataId = $(this).attr('data-id');
        var value = $(this).val();
        $(this).prop('checked', true);
        $('.choices[data-id="'+(dataId)+'"]').not(this).prop('checked', false).prop('required', false);
        $('#answers-'+(dataId)+'-choice-id').val(value);
        var triggerCharter = $(this).attr('trigger-charter');
        if(triggerCharter == 1){
            $('.charters').not(this).prop('checked', false);
            $('.charters[points="0"]').each(function () {
                var value = $(this).val();
                var dataId = $(this).attr('data-id');
                $(this).prop('checked', true);
                $('#answers-'+(dataId)+'-choice-id').val(value);
            });
        }
    });

    $('#customer-type-id').change(function (e) {
        var value = $(this).val();
        if(value){
            getServicesList(value);
        }
    });

    CKEDITOR.ClassicEditor.create(document.getElementById('remarks'), {
        // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
        toolbar: {
            items: [
//                    'exportPDF','exportWord', '|',
//                    'findAndReplace', 'selectAll', '|',
//                    'heading', '|',
//                    'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
//                    'bulletedList', 'numberedList', 'todoList', '|',
//                    'outdent', 'indent', '|',
//                    'undo', 'redo',
//                    '-',
//                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
//                    'alignment', '|',
//                    'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
//                    'specialCharacters', 'horizontalLine', 'pageBreak',
            ],
            shouldNotGroupWhenFull: true
        },
        // Changing the language of the interface requires loading the language file using the <script> tag.
        // language: 'es',
        list: {
            properties: {
                styles: true,
                startIndex: true,
                reversed: true
            }
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
            ]
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
        placeholder: 'Enter Your Remarks...',
        // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
        fontFamily: {
            options: [
                'default',
                'Arial, Helvetica, sans-serif',
                'Courier New, Courier, monospace',
                'Georgia, serif',
                'Lucida Sans Unicode, Lucida Grande, sans-serif',
                'Tahoma, Geneva, sans-serif',
                'Times New Roman, Times, serif',
                'Trebuchet MS, Helvetica, sans-serif',
                'Verdana, Geneva, sans-serif'
            ],
            supportAllValues: true
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
        fontSize: {
            options: [ 10, 12, 14, 'default', 18, 20, 22 ],
            supportAllValues: true
        },
        // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
        // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
        htmlSupport: {
            allow: [
                {
                    name: /.*/,
                    attributes: true,
                    classes: true,
                    styles: true
                }
            ]
        },
        // Be careful with enabling previews
        // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
        htmlEmbed: {
            showPreviews: true
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
        link: {
            decorators: {
                addTargetToExternalLinks: true,
                defaultProtocol: 'https://',
                toggleDownloadable: {
                    mode: 'manual',
                    label: 'Downloadable',
                    attributes: {
                        download: 'file'
                    }
                }
            }
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
        mention: {
            feeds: [
                {
                    marker: '@',
                    feed: [
                        '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                        '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                        '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                        '@sugar', '@sweet', '@topping', '@wafer'
                    ],
                    minimumCharacters: 1
                }
            ]
        },
        // The "super-build" contains more premium features that require additional configuration, disable them below.
        // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
        removePlugins: [
            // These two are commercial, but you can try them out without registering to a trial.
            // 'ExportPdf',
            // 'ExportWord',
            'CKBox',
            'CKFinder',
            'EasyImage',
            // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
            // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
            // Storing images as Base64 is usually a very bad idea.
            // Replace it on production website with other solutions:
            // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
            // 'Base64UploadAdapter',
            'RealTimeCollaborativeComments',
            'RealTimeCollaborativeTrackChanges',
            'RealTimeCollaborativeRevisionHistory',
            'PresenceList',
            'Comments',
            'TrackChanges',
            'TrackChangesData',
            'RevisionHistory',
            'Pagination',
            'WProofreader',
            // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
            // from a local file system (file://) - load this site via HTTP server if you enable MathType
            'MathType',
        ]
    }).then( function (data) {
        editor = data;
    }).catch(function (error) {
        window.location.reload();
    });

    setInterval(function(){
        editor.updateSourceElement();
    }, 1000);

    function getServicesList(customerTypeId) {
        $.ajax({
            url: mainurl+'services/getServicesList/'+(customerTypeId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#service-id').empty().prop('disabled', true).append('<option value="">Please Wait</option>');
            },
        }).done(function (data, status, xhr) {
            if((Object.keys(data)).length){
                $('#service-id').empty().append('<option value="">Please Select Service</option>');
                $.map(data, function (data, key) {
                    $('#service-id').append('<option value="'+(key)+'">'+(data)+'</option>');
                });
            }

            $('#service-id').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
        });
    }

    function getVisitors() {
        $.ajax({
            url: mainurl+'visitors/getVisitors',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#pending-transactions').html('<h6>Pending Transactions</h6>');
            },
        }).done(function (data, status, xhr) {
            if((Object.keys(data)).length){
                $('#pending-transactions').fadeIn(100);
                $.map(data, function (data, key) {
                    $('#pending-transactions').append(' <div class="card rounded-0 border-bottom border-primary border-top-0 border-left-0 border-right-0 hover-shadow"> ' +
                        '<div class="card-body"> ' +
                        '<a href="javascript:void(0);"> ' +
                        '<h6 class="card-title mb-0"><small>'+(data.transaction_code)+'</small></h6> </a>' +
                        ' <p class="card-text">'+(data.name)+'</p> ' +
                        '</div> </div>');
                });
            }else{
                $('#pending-transactions').fadeOut(100);
            }
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

    getVisitors();

    setTimeout(function () {
        getVisitors();
    }, (1000 * 60 * 15));

});